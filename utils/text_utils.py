import re
from googletrans import Translator
from googletrans.models import Detected
from langdetect import DetectorFactory, detect_langs

__use_googleAPI = False
translator = Translator()


def lang_of(text):
    global translator
    global __use_googleAPI

    if __use_googleAPI:
        return translator.detect(text)
    else:
        DetectorFactory.seed = 0

        for result in detect_langs(text):
            return Detected(result.lang, result.prob)

        raise ValueError("Unknown language.")


def lang_detect(text: str):

    lang_results = dict()
    tokens = re.findall("[\\w_']+", text)

    for token in tokens:
        try:
            detection = lang_of(token)
        except Exception as err:
            continue

        lang = detection.lang
        confidence = detection.confidence

        if lang not in lang_results:
            lang_results[lang] = 0

        lang_results[lang] = lang_results[lang] + confidence

    try:
        detection = lang_of(text)

        lang = detection.lang
        confidence = detection.confidence

        if lang not in lang_results:
            lang_results[lang] = 0

        lang_results[lang] = lang_results[lang] + (confidence * 2)
    except Exception as err:
        ignored = True

    return lang_results


def prominent_lang_detect(text):
    lang_results = lang_detect(text)

    text_language = None
    max = 0

    for lang in lang_results.keys():
        confidence = lang_results[lang]

        if confidence >= max:
            max = confidence
            text_language = lang

    return text_language


def is_english(text):
    return prominent_lang_detect(text) == "en"


def is_fully_english(text):
    lang_results = lang_detect(text)

    if len(lang_results) == 0:
        return False
    elif len(lang_results) == 1:
        return "en" in lang_results.keys()
    else:
        langs = {k: v for k, v in sorted(lang_results.items(), key=lambda item: item[1], reverse=True)}

        dominant_lang = list(langs.keys())[0]
        dominant_conf = langs[dominant_lang]

        if not dominant_lang == "en":
            return False

        secondary_lang = list(langs.keys())[1]
        secondary_conf = langs[secondary_lang]

        if secondary_conf < (dominant_conf/2.0):
            return True

    return False


def is_latin_script(text):
    return re.match("[A-Za-z0-9_\\-']+", text) is not None


def is_blank(text):
    if text is None:
        return True

    if isinstance(text, str):
        return len(text) < 1
    else:
        raise ValueError("Expected a String.")
