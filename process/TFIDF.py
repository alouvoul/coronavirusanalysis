# import math
# from nltk import word_tokenize
# from nltk.corpus import stopwords
# from sklearn.feature_extraction.text import TfidfVectorizer
# import pandas as pd
#
#
# def tf_idf(documents, remove_stopwords=True):
#     if not isinstance(documents, list):
#         raise ValueError("Expected [String].")
#
#     if len(documents) == 0:
#         return []
#
#     first = documents[0]
#
#     if not isinstance(first, str):
#         raise ValueError("Expected [String].")
#
#     docs_n = len(documents)
#     vocabulary = set()
#
#     docs_word_counts = []
#     tf_list = []
#
#     for doc in documents:
#         tokens = word_tokenize(doc)
#
#         stopwords_english = set(stopwords.words('english'))
#         # remove punctuation
#         tokens = list(filter(lambda t: t.isalpha(), tokens))
#
#         vocabulary = vocabulary.union(set(tokens))
#
#         if remove_stopwords:
#             tokens = [token for token in tokens if token not in stopwords_english]
#
#         bag_of_words = set(tokens)
#         words_counts = dict.fromkeys(bag_of_words, 0)
#         docs_word_counts.append(words_counts)
#
#         for word in tokens:
#             words_counts[word] += 1
#
#         tf = find_tf(words_counts)
#         tf_list.append(tf)
#
#     idf = find_idf(docs_word_counts, vocabulary)
#     return find_tf_idf(tf_list, idf)
#
#
# def find_tf(words_counts):
#     words_n = float(sum(map(lambda kv_pair: kv_pair[1], words_counts.items())))
#     return {word: (float(c)/words_n) for word, c in words_counts.items()}
#
#
# def find_idf(docs_word_counts: list, vocabulary: list):
#     docs_n = float(len(docs_word_counts))
#     idf = dict.fromkeys(vocabulary, 0)
#
#     for word_counts in docs_word_counts:
#         for word, count in word_counts.items():
#             if count > 0:
#                 idf[word] += 1
#
#     for word, value in idf.items():
#         if value == 0:
#             idf[word] = 0
#         else:
#             v = docs_n / float(value)
#             idf[word] = math.log(v, 10)
#
#     return idf
#
#
# def find_tf_idf(tfs, idf):
#     tf_idfs = []
#
#     for document_tf in tfs:
#         _tf_idf = dict.fromkeys(idf.keys(), 0)
#
#         for word, term_freq in document_tf.items():
#             word_idf = idf[word]
#             _tf_idf[word] = term_freq * word_idf
#
#         tf_idfs.append(_tf_idf)
#
#     return tf_idfs
#
#
# def sk_tf_idf(documents):
#     vectorizer = TfidfVectorizer()
#     vectors = vectorizer.fit_transform(documents)
#     feature_names = vectorizer.get_feature_names()
#     dense = vectors.todense()
#     denselist = dense.tolist()
#
#     tfidf = []
#
#     for doc_tfidf in denselist:
#         _tfidf = dict.fromkeys(feature_names, 0)
#
#         for i in range(0, len(feature_names)):
#             word = feature_names[i]
#             value = doc_tfidf[i]
#
#             _tfidf[word] = value
#
#         tfidf.append(_tfidf)
#     return tfidf
#
