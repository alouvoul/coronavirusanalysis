from process.process_data import process_data

#This function starts the Data processing workflow

def start_data_processing():
    print("Started Data Processing...")
    process_data()