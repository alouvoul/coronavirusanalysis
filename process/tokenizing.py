import re
import string

import emoji
import nltk
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer

from data.mongo import mongo
from process.dictionaries import punctuation_normalizations

# Initialize nltk packages. If exist the file then
try:
    file = open("nltk_packages.txt")
except IOError:
    nltk.download("punkt")
    nltk.download('wordnet')
    nltk.download('stopwords')
    nltk.download('averaged_perceptron_tagger')
    nltk.download("names")
    file = open("nltk_packages.txt", "w")
    file.close()


def stop_words():
    return set(stopwords.words("english")).union(set(stopwords.words("spanish")))


def preprocess_text(text, discard_limited_tokens=True):
    """
    This function applies all the preprocessing fuctions to each row of data
    :param discard_limited_tokens: flag to determine whether or not to retain small size posts
    :param text: the post text
    :return:
    """

    # Remove technicalities (numbers, mentions, urls) - 1st pass
    text = remove_technical([text])[0]

    # normalize text punctuation
    text = normalize_text(text)

    # Tokenize text of tweets
    tokens = tokenize_text(text)

    # Remove stopwords
    tokens = remove_stopwords(tokens)

    # Detect Emoji
    tokens = clear_emoji(tokens)

    # Remove technicalities (numbers, mentions, urls) - 2nd pass
    tokens = remove_technical(tokens)

    # Remove punctuations
    tokens = remove_punctuations(tokens)

    # Lemmatize tokens
    tokens = lemmatize(tokens)

    tokens = demoji(tokens)
    tokens = clean(tokens)

    if not discard_limited_tokens:
        return tokens

    if len(tokens) < 5:
        return None

    return tokens

def preprocess_like(text, discard_limited_tokens=True):
    """
    This function performs the preprocessing for the like prediction task
    :param discard_limited_tokens: flag to determine whether or not to retain small size posts
    :param text:
    :return:
    """

    # Remove technicalities (numbers, mentions, urls) - 1st pass
    text = remove_technical([text])[0]

    # normalize text punctuation
    text = normalize_text(text)

    # Tokenize text of tweets
    tokens = tokenize_text(text)

    # Remove stopwords
    tokens = remove_stopwords(tokens)

    # Detect Emoji
    tokens = clear_emoji(tokens)

    # Remove technicalities (numbers, mentions, urls) - 2nd pass
    tokens = remove_technical(tokens)

    # Remove punctuations
    tokens = remove_punctuations(tokens)

    # Lemmatize tokens
    tokens = lemmatize(tokens)

    tokens = demoji(tokens)
    tokens = clean(tokens)

    return tokens


def clean(tokens):
    """

    :param tokens: The data to be cleaned
    :return:
    """
    cleaned = []

    for token in tokens:

        if " " in token:
            parts = clean(token.split(" "))

            for part in parts:
                part = part.strip()

                if part is None or len(part) == 0:
                    continue

                if part == "SEMANTICNEGATION":
                    part = "SEMANTIC_NEGATION"

                cleaned.append(part)

            continue

        cleaned_token = token.strip()

        if cleaned_token is None or len(cleaned_token) == 0:
            continue

        if cleaned_token == "SEMANTICNEGATION":
            cleaned_token = "SEMANTIC_NEGATION"

        cleaned.append(cleaned_token)

    return cleaned


def normalize_text(text):
    """
    This function normalises text,in the sense that it removes spaces, tags, hashtags
    :param text:
    :return:
    """
    # for contraction in contraction_expansions.keys():
    #     expansion = contraction_expansions[contraction]
    #     expression = "(\\b)("+contraction+")(\\b)"
    #     text = re.sub(expression, "\\1"+expansion+"\\3", text)

    here = 0

    # normalization of double and single quotes from unicode to ascii characters
    for key in punctuation_normalizations:
        replacement = key
        for target in punctuation_normalizations[key]:
            text = text.replace(target, replacement)

    # fix missing space after punkt
    text = re.sub(r"(\b)?(\w+)([!?:;,.])(\w+)(\b|[!?:;,.])", " \\2\\3 \\4\\5 ", text, 0, re.MULTILINE)

    # fix space before punkt
    text = re.sub(r"(\s)([!?:;,.])(\s)?", "\\2\\3", text, 0, re.MULTILINE)

    # separate hashtags with spaces
    text = re.sub(r"((\s|\b|[\"\'><.,;:?!*+\-=$#{}\[\]\\/])(#[\w]+))", "\\2 \\3 ", text, 0, re.MULTILINE)

    # separate tags with spaces
    text = re.sub(r"((\s|\b|[\"\'><.,;:?!*+\-=$#{}\[\]\\/])(@[\w]+))", "\\2 \\3 ", text, 0, re.MULTILINE)

    return text


def tokenize_text(text=""):
    """
    Function that splits text in separate tokens
    :param text: text to be tokenized
    :return:
    """
    if not text:
        return []
    return [token.lower() for token in text.split(" ")]


def clear_emoji(tokens):
    """
    This function clear emojies from text and keeps them for further usage
    :param tokens: separated tokens
    :return:
    """
    split = []
    for token in tokens:
        splitted_emojies = emoji.get_emoji_regexp().split(token)
        split = split + splitted_emojies
    return split


def demoji(tokens):
    """
    This function describes each emoji with a text that can be later used for vectorization and ML predictions
    :param tokens:
    :return:
    """
    emoji_description = []
    for token in tokens:
        detect = emoji.demojize(token)
        emoji_description.append(detect)
    return emoji_description


def remove_punctuations(tokens):
    """
    This function removes punctuation from a list of tokens
    :param tokens:
    :return:
    """
    cleaned_tokens = []

    punctuations = [x for x in string.punctuation]
    punctuations.remove('?')
    punctuations.remove('!')
    punctuations.remove('.')

    punctuations.append("'")
    punctuations.append('...')
    punctuations.append('#')
    punctuations.append('-')
    punctuations.append('$')
    punctuations.append('|')

    for token in tokens:
        # Sanitize string for punctuation chars
        for punkt in punctuations:
            replacement = ""

            if punkt == "'" and re.search(r"[a-zA-Z]'[a-zA-Z]", token) is not None:
                replacement = " "
            elif punkt in ["(", "[", "{", ")", "]", ")"]:
                replacement = " "

            token = token.replace(punkt, replacement)

        token = token.strip()

        if token and len(token) > 0:
            cleaned_tokens.append(token)
    return cleaned_tokens


def remove_technical(tokens):
    """
    Removes technicalities (numbers,mentions,urls) from tokens
    :param tokens:
    :return:
    """
    technical = []

    for token in tokens:
        token = re.sub(r'\d+', '', token)  # remove numbers
        token = re.sub(r"(\b(http(s)?)(:)([\w\-.&=?\\\/]+)\b)", 'url',
                       token)  # remove urls
        token = re.sub("(@[A-Za-z0-9_]+)", "", token)  # Remove mentions
        technical.append(token)
    return technical


def remove_stopwords(tokens):
    """
    Removes stopwords (common words such as "the","a" etc.) from tokens
    :param tokens:
    :return:
    """
    negations = ["not", "no"]
    stopwords_english = set(stopwords.words('english'))

    tokens_no_stopwords = []

    for token in tokens:

        if token not in stopwords_english:
            tokens_no_stopwords.append(token)
            continue

        replacement = ""

        for stopword in stopwords_english:

            if token != stopword:
                continue

            if stopword in negations or re.search(r"[a-zA-Z]+n\'t", stopword) is not None:
                replacement = "SEMANTIC_NEGATION"

            token = token.replace(stopword, replacement).strip()
            break

        if len(token) == 0:
            continue

        tokens_no_stopwords.append(token)

    return tokens_no_stopwords
    # return [token for token in tokens if token not in stopwords_english]


def lemmatize(tokens):
    """
    Function that performs lemmatization to tokens
    :param tokens:
    :return:
    """
    lemmatizer = WordNetLemmatizer()
    cleaned_data = []
    for token in tokens:
        lemma = lemmatizer.lemmatize(token)
        cleaned_data.append(lemma)
    return cleaned_data
# EOF
