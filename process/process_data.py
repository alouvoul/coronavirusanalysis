import gc

from data.entities.converters import twitter_post_make, instagram_post_make, twitter_user_make, instagram_user_make
from data.mongo import mongo
from prediction.LDA import lda_topics
from scrap.InstaAPI import IGUser, IGPost
from scrap.TwitterAPI import TWTweet, TWUser

NO_TOPIC = "no_topic"
topics = []


def resolve_topics_of(tokens: list):
    """

    :param tokens: a list of tokens extracted from the text of a post or tweet
    :return: a list of matching topics
    """
    global topics
    topic_matches = []

    for topic in topics:
        topic_similarity = 0.0

        for term_weight in topic["terms"]:
            term = term_weight["term"]
            weight = term_weight["weight"]

            if term in tokens:
                topic_similarity += 1.0 * weight

        if topic_similarity < 0.5:
            continue

        topic_match = dict()
        topic_match["topic"] = topic["topic_id"]
        topic_match["similarity"] = topic_similarity

        topic_matches.append(topic_match)

    return topic_matches


def process_data():
    """
    This method processes the scrapped Posts/Tweets and Users and then
    converts them to a common representation under the CA (Coronavirus Analysis)
    namespace --- i.e. any object that starts with CA.

    The conversion pivot is the User object.
    :return:
    """
    # Find LDA topics from DB
    global topics
    topics = []

    print("Loading LDA topics...")

    for topic in mongo().ca_db.find_all_topics():
        topics.append(topic)

    # If no topics were found, create topics for ALL documents (twitter & instagram)
    if topics is None or len(topics) < 1:
        topics = lda_topics()

        for topic in topics:
            mongo().ca_db.save_topic(topic)

    page_size = 50

    twitter_users_page_count = mongo().twitter.count_user_pages(page_size)
    instagram_users_page_count = mongo().instagram.count_user_pages(page_size)

    last_processed_twitter_user_page = 286
    twitter_limit = 300
    last_processed_twitter_user_page = 485
    twitter_limit = 500

    last_processed_instagram_user_page = 0

    for page in range(last_processed_twitter_user_page + 1, twitter_users_page_count + 1):

        if page > twitter_limit:
            break

        process_twitter(twitter_users_page_count, page, page_size)
        # memory cleanup
        gc.collect()

    for page in range(last_processed_instagram_user_page + 1, instagram_users_page_count + 1):
        process_instagram(instagram_users_page_count, page, page_size)
        # memory cleanup
        gc.collect()

    return None


def process_twitter(total_pages: int, page: int, size: int):
    print("\n")
    print("|---- Processing twitter users (" + str(page) + "/" + str(total_pages) + ")....")

    # Process Twitter
    all_twitter_users = [user_dict for user_dict in mongo().twitter.get_users_paginated(page, size)]

    for user_dict in all_twitter_users:
        # Convert user_dict from mongo to TWUser
        user = TWUser(None).from_dict(user_dict)

        print("|------ Processing twitter user:", user.name)

        user_ca_posts = []
        tweets = [tweet_dict for tweet_dict in mongo().twitter.get_user_tweets(user.user_id)]

        for tweet_dict in tweets:
            # Convert dict from mongo to TWTweet
            tweet = TWTweet(None).from_dict(tweet_dict)

            print("|            Processing tweet:", tweet.tweet_id)

            # Build CAPost
            ca_post = twitter_post_make(tweet, user, resolve_topics_of)

            if ca_post is None:
                continue

            user_ca_posts.append(ca_post)

        # Build CAUser and User Profile
        ca_user = twitter_user_make(user, user_ca_posts)

        # Save ca_user
        mongo().ca_db.save_processed_user(ca_user)

    print("|---- Processed twitter users (" + str(page) + "/" + str(total_pages) + ").")
    print("\n")


def process_instagram(total_pages: int, page: int, size: int):
    print("\n")
    print("|---- Processing instagram users (" + str(page) + "/" + str(total_pages) + ")....")

    # Process Instagram
    all_instagram_users = [user_dict for user_dict in mongo().instagram.get_users_paginated(page, size)]

    for user_dict in all_instagram_users:
        # Convert user_dict from mongo to IGUser
        user = IGUser(None).from_dict(user_dict)

        print("|------ Processing instagram user:", user.username)

        user_ca_posts = []
        posts = [post_dict for post_dict in mongo().instagram.get_user_posts(user.user_id)]

        for post_dict in posts:
            # Convert dict from mongo to IGPost
            post = IGPost(None).from_dict(post_dict)

            print("|            Processing instagram post:", post.shortcode)

            # Build CAPost
            ca_post = instagram_post_make(post, user, resolve_topics_of)

            if ca_post is None:
                continue

            user_ca_posts.append(ca_post)

        # Build CAUser and User Profile
        ca_user = instagram_user_make(user, user_ca_posts)

        # Save ca_user
        mongo().ca_db.save_processed_user(ca_user)

    print("|---- Processed instagram users (" + str(page) + "/" + str(total_pages) + ").")
    print("\n")
