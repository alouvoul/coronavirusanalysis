import random

from flask import render_template
import flask
from app import app
from data.entities.CAUser import CAUser
from data.entities.ca_constants import SOURCE_TWITTER, SOURCE_INSTAGRAM
from data.mongo import mongo
from process.tokenizing import stop_words

twitter_data = None
insta_data = None
expl_data = None


def count(cursor):
    counts = list(cursor)

    if len(counts) == 0:
        return 0

    return counts[0]["count"]


def value(cursor, field):
    elements = list(cursor)

    if len(elements) == 0:
        return 0

    return elements[0][field]


def as_locations(cursor, limit: int = 1000):
    elements = list(cursor)

    if elements is None or len(elements) == 0:
        return []

    clean_locations = []

    for id_count_pair in elements:
        lonlat = id_count_pair["_id"]

        if lonlat is None or len(lonlat) == 0:
            continue

        info = [0, 0, 0]
        info[0] = lonlat[1]
        info[1] = lonlat[0]
        info[2] = id_count_pair["count"]

        clean_locations.append(info)

        if len(clean_locations) >= limit:
            break

    return clean_locations


def as_tokens(cursor, limit: int = 1000):
    elements = list(cursor)

    if elements is None or len(elements) == 0:
        return []

    clean_tokens = []

    for id_count_pair in elements:
        token_info = dict()
        token_info["token"] = id_count_pair["_id"]
        token_info["count"] = id_count_pair["count"]

        if len(token_info["token"]) < 2 or token_info["token"] in stop_words():
            continue

        clean_tokens.append(token_info)

        if len(clean_tokens) >= limit:
            break

    return clean_tokens


def as_hashtags(cursor, limit: int = 1000):
    elements = list(cursor)

    if elements is None or len(elements) == 0:
        return []

    clean_hashtags = []

    for id_count_pair in elements:
        hashtag_info = dict()
        hashtag_info["hashtag"] = id_count_pair["_id"]
        hashtag_info["count"] = id_count_pair["count"]

        if len(hashtag_info["hashtag"]) < 2 or hashtag_info["hashtag"] in stop_words():
            continue

        hashtag_info["hashtag"] = "#" + hashtag_info["hashtag"]

        clean_hashtags.append(hashtag_info)

        if len(clean_hashtags) >= limit:
            break

    return clean_hashtags


def as_named_entities(cursor, limit: int = 1000):
    elements = list(cursor)

    if elements is None or len(elements) == 0:
        return []

    discard_tokens = ["SEMANTIC_NEGATION", "url", "amp"]
    clean_entities = []

    for id_count_pair in elements:
        info = dict()
        info["entity"] = id_count_pair["_id"]
        info["count"] = id_count_pair["count"]

        if len(info["entity"]["text"]) < 2 or info["entity"]["text"] in stop_words() or info["entity"]["text"] in discard_tokens:
            continue

        if info["entity"]["type"] not in ["PERSON", "GPE", "ORG", "LOC"]:
            continue

        clean_entities.append(info)

        if len(clean_entities) >= limit:
            break

    return clean_entities


def to_percent(count_value, total, round_digit=1):
    if total == 0:
        return 0

    return round((count_value / total) * 100, round_digit)

def as_user(cursor):
    elemenets = list(cursor)

    if elemenets is None or len(elemenets) == 0:
        return None

    user = CAUser().from_dict(elemenets[0])
    posts = user.posts

    if len(posts) > 1:
        posts = [random.choice(posts)]
        user.posts = posts

    return user


def init_data():
    global twitter_data
    global insta_data
    global expl_data

    if twitter_data is None:
        print("Creating twitter view model...")
        twitter_data = dict()

        # Header Data
        twitter_data["posts_count"] = count(mongo().ca_db.count_source_posts(SOURCE_TWITTER))
        twitter_data["users_count"] = count(mongo().ca_db.count_source_users(SOURCE_TWITTER))
        twitter_data["avg_likes"] = int(value(mongo().ca_db.avg_like_count(SOURCE_TWITTER), "avg_likes"))

        # Sentiment Data
        sent_pos_count = count(mongo().ca_db.count_positive_posts(SOURCE_TWITTER))
        sent_neu_count = count(mongo().ca_db.count_neutral_posts(SOURCE_TWITTER))
        sent_neg_count = count(mongo().ca_db.count_negative_posts(SOURCE_TWITTER))

        sent_total = sent_pos_count + sent_neu_count + sent_neg_count

        twitter_data["sent_pos_count"] = sent_pos_count
        twitter_data["sent_pos_percent"] = to_percent(sent_pos_count, sent_total)

        twitter_data["sent_neu_count"] = sent_neu_count
        twitter_data["sent_neu_percent"] = to_percent(sent_neu_count, sent_total)

        twitter_data["sent_neg_count"] = sent_neg_count
        twitter_data["sent_neg_percent"] = to_percent(sent_neg_count, sent_total)

        # Emotion Data
        emo_anger = count(mongo().ca_db.count_emo_anger(SOURCE_TWITTER))
        emo_disgust = count(mongo().ca_db.count_emo_disgust(SOURCE_TWITTER))
        emo_fear = count(mongo().ca_db.count_emo_fear(SOURCE_TWITTER))
        emo_joy = count(mongo().ca_db.count_emo_joy(SOURCE_TWITTER))
        emo_sadness = count(mongo().ca_db.count_emo_sadness(SOURCE_TWITTER))
        emo_surprise = count(mongo().ca_db.count_emo_surprise(SOURCE_TWITTER))

        emo_total = emo_anger + emo_disgust + emo_fear + emo_joy + emo_sadness + emo_surprise

        twitter_data["emo_anger_count"] = emo_anger
        twitter_data["emo_anger_percent"] = to_percent(emo_anger, emo_total)

        twitter_data["emo_disgust_count"] = emo_disgust
        twitter_data["emo_disgust_percent"] = to_percent(emo_disgust, emo_total)

        twitter_data["emo_fear_count"] = emo_fear
        twitter_data["emo_fear_percent"] = to_percent(emo_fear, emo_total)

        twitter_data["emo_joy_count"] = emo_joy
        twitter_data["emo_joy_percent"] = to_percent(emo_joy, emo_total)

        twitter_data["emo_sadness_count"] = emo_sadness
        twitter_data["emo_sadness_percent"] = to_percent(emo_sadness, emo_total)

        twitter_data["emo_surprise_count"] = emo_surprise
        twitter_data["emo_surprise_percent"] = to_percent(emo_surprise, emo_total)

        # Gender Data
        gen_male = count(mongo().ca_db.count_profiles_with_gender_male(SOURCE_TWITTER))
        gen_female = count(mongo().ca_db.count_profiles_with_gender_female(SOURCE_TWITTER))
        gen_other = count(mongo().ca_db.count_profiles_with_gender_other(SOURCE_TWITTER))

        gen_total = gen_male + gen_female + gen_other

        twitter_data["gen_male_count"] = gen_male
        twitter_data["gen_male_percent"] = to_percent(gen_male, gen_total)

        twitter_data["gen_female_count"] = gen_female
        twitter_data["gen_female_percent"] = to_percent(gen_female, gen_total)

        twitter_data["gen_other_count"] = gen_other
        twitter_data["gen_other_percent"] = to_percent(gen_other, gen_total)

        # Age Data
        age_18_24 = count(mongo().ca_db.count_profiles_with_age_18_24(SOURCE_TWITTER))
        age_25_34 = count(mongo().ca_db.count_profiles_with_age_25_34(SOURCE_TWITTER))
        age_35_49 = count(mongo().ca_db.count_profiles_with_age_35_49(SOURCE_TWITTER))
        age_50_plus = count(mongo().ca_db.count_profiles_with_age_50_plus(SOURCE_TWITTER))

        age_total = age_18_24 + age_25_34 + age_35_49 + age_50_plus

        twitter_data["age_18_24_count"] = age_18_24
        twitter_data["age_18_24_percent"] = to_percent(age_18_24, age_total)

        twitter_data["age_25_34_count"] = age_25_34
        twitter_data["age_25_34_percent"] = to_percent(age_25_34, age_total)

        twitter_data["age_35_49_count"] = age_35_49
        twitter_data["age_35_49_percent"] = to_percent(age_35_49, age_total)

        twitter_data["age_50_plus_count"] = age_50_plus
        twitter_data["age_50_plus_percent"] = to_percent(age_50_plus, age_total)

        # Sarcasm Data
        sarc_yes = count(mongo().ca_db.count_profiles_sarcastic(SOURCE_TWITTER))
        sarc_no = count(mongo().ca_db.count_profiles_not_sarcastic(SOURCE_TWITTER))

        sarc_total = sarc_yes + sarc_no

        twitter_data["sarc_yes_count"] = sarc_yes
        twitter_data["sarc_yes_percent"] = to_percent(sarc_yes, sarc_total)

        twitter_data["sarc_no_count"] = sarc_no
        twitter_data["sarc_no_percent"] = to_percent(sarc_no, sarc_total)

        # Aggression
        agg_none = count(mongo().ca_db.count_profiles_no_aggression(SOURCE_TWITTER))
        agg_low = count(mongo().ca_db.count_profiles_low_aggression(SOURCE_TWITTER))
        agg_med = count(mongo().ca_db.count_profiles_medium_aggression(SOURCE_TWITTER))
        agg_high = count(mongo().ca_db.count_profiles_high_aggression(SOURCE_TWITTER))

        agg_total = agg_none + agg_low + agg_med + agg_high

        twitter_data["agg_none_count"] = agg_none
        twitter_data["agg_none_percent"] = to_percent(agg_none, agg_total)

        twitter_data["agg_low_count"] = agg_low
        twitter_data["agg_low_percent"] = to_percent(agg_low, agg_total)

        twitter_data["agg_med_count"] = agg_med
        twitter_data["agg_med_percent"] = to_percent(agg_med, agg_total)

        twitter_data["agg_high_count"] = agg_high
        twitter_data["agg_high_percent"] = to_percent(agg_high, agg_total)

        # Geolocations
        locations = as_locations(mongo().ca_db.top_lon_lat_and_count(SOURCE_TWITTER), limit=100)
        twitter_data["locations"] = locations

    if insta_data is None:
        print("Creating instagram view model...")
        insta_data = dict()

        # Header Data
        insta_data["posts_count"] = count(mongo().ca_db.count_source_posts(SOURCE_INSTAGRAM))
        insta_data["users_count"] = count(mongo().ca_db.count_source_users(SOURCE_INSTAGRAM))
        insta_data["avg_likes"] = int(value(mongo().ca_db.avg_like_count(SOURCE_INSTAGRAM), "avg_likes"))

        # Sentiment Data
        sent_pos_count = count(mongo().ca_db.count_positive_posts(SOURCE_INSTAGRAM))
        sent_neu_count = count(mongo().ca_db.count_neutral_posts(SOURCE_INSTAGRAM))
        sent_neg_count = count(mongo().ca_db.count_negative_posts(SOURCE_INSTAGRAM))

        sent_total = sent_pos_count + sent_neu_count + sent_neg_count

        insta_data["sent_pos_count"] = sent_pos_count
        insta_data["sent_pos_percent"] = to_percent(sent_pos_count, sent_total)

        insta_data["sent_neu_count"] = sent_neu_count
        insta_data["sent_neu_percent"] = to_percent(sent_neu_count, sent_total)

        insta_data["sent_neg_count"] = sent_neg_count
        insta_data["sent_neg_percent"] = to_percent(sent_neg_count, sent_total)

        # Emotion Data
        emo_anger = count(mongo().ca_db.count_emo_anger(SOURCE_INSTAGRAM))
        emo_disgust = count(mongo().ca_db.count_emo_disgust(SOURCE_INSTAGRAM))
        emo_fear = count(mongo().ca_db.count_emo_fear(SOURCE_INSTAGRAM))
        emo_joy = count(mongo().ca_db.count_emo_joy(SOURCE_INSTAGRAM))
        emo_sadness = count(mongo().ca_db.count_emo_sadness(SOURCE_INSTAGRAM))
        emo_surprise = count(mongo().ca_db.count_emo_surprise(SOURCE_INSTAGRAM))

        emo_total = emo_anger + emo_disgust + emo_fear + emo_joy + emo_sadness + emo_surprise

        insta_data["emo_anger_count"] = emo_anger
        insta_data["emo_anger_percent"] = to_percent(emo_anger, emo_total)

        insta_data["emo_disgust_count"] = emo_disgust
        insta_data["emo_disgust_percent"] = to_percent(emo_disgust, emo_total)

        insta_data["emo_fear_count"] = emo_fear
        insta_data["emo_fear_percent"] = to_percent(emo_fear, emo_total)

        insta_data["emo_joy_count"] = emo_joy
        insta_data["emo_joy_percent"] = to_percent(emo_joy, emo_total)

        insta_data["emo_sadness_count"] = emo_sadness
        insta_data["emo_sadness_percent"] = to_percent(emo_sadness, emo_total)

        insta_data["emo_surprise_count"] = emo_surprise
        insta_data["emo_surprise_percent"] = to_percent(emo_surprise, emo_total)

        # Gender Data
        gen_male = count(mongo().ca_db.count_profiles_with_gender_male(SOURCE_INSTAGRAM))
        gen_female = count(mongo().ca_db.count_profiles_with_gender_female(SOURCE_INSTAGRAM))
        gen_other = count(mongo().ca_db.count_profiles_with_gender_other(SOURCE_INSTAGRAM))

        gen_total = gen_male + gen_female + gen_other

        insta_data["gen_male_count"] = gen_male
        insta_data["gen_male_percent"] = to_percent(gen_male, gen_total)

        insta_data["gen_female_count"] = gen_female
        insta_data["gen_female_percent"] = to_percent(gen_female, gen_total)

        insta_data["gen_other_count"] = gen_other
        insta_data["gen_other_percent"] = to_percent(gen_other, gen_total)

        # Age Data
        age_18_24 = count(mongo().ca_db.count_profiles_with_age_18_24(SOURCE_INSTAGRAM))
        age_25_34 = count(mongo().ca_db.count_profiles_with_age_25_34(SOURCE_INSTAGRAM))
        age_35_49 = count(mongo().ca_db.count_profiles_with_age_35_49(SOURCE_INSTAGRAM))
        age_50_plus = count(mongo().ca_db.count_profiles_with_age_50_plus(SOURCE_INSTAGRAM))

        age_total = age_18_24 + age_25_34 + age_35_49 + age_50_plus

        insta_data["age_18_24_count"] = age_18_24
        insta_data["age_18_24_percent"] = to_percent(age_18_24, age_total)

        insta_data["age_25_34_count"] = age_25_34
        insta_data["age_25_34_percent"] = to_percent(age_25_34, age_total)

        insta_data["age_35_49_count"] = age_35_49
        insta_data["age_35_49_percent"] = to_percent(age_35_49, age_total)

        insta_data["age_50_plus_count"] = age_50_plus
        insta_data["age_50_plus_percent"] = to_percent(age_50_plus, age_total)

        # Sarcasm Data
        sarc_yes = count(mongo().ca_db.count_profiles_sarcastic(SOURCE_INSTAGRAM))
        sarc_no = count(mongo().ca_db.count_profiles_not_sarcastic(SOURCE_INSTAGRAM))

        sarc_total = sarc_yes + sarc_no

        insta_data["sarc_yes_count"] = sarc_yes
        insta_data["sarc_yes_percent"] = to_percent(sarc_yes, sarc_total)

        insta_data["sarc_no_count"] = sarc_no
        insta_data["sarc_no_percent"] = to_percent(sarc_no, sarc_total)

        # Aggression
        agg_none = count(mongo().ca_db.count_profiles_no_aggression(SOURCE_INSTAGRAM))
        agg_low = count(mongo().ca_db.count_profiles_low_aggression(SOURCE_INSTAGRAM))
        agg_med = count(mongo().ca_db.count_profiles_medium_aggression(SOURCE_INSTAGRAM))
        agg_high = count(mongo().ca_db.count_profiles_high_aggression(SOURCE_INSTAGRAM))

        agg_total = agg_none + agg_low + agg_med + agg_high

        insta_data["agg_none_count"] = agg_none
        insta_data["agg_none_percent"] = to_percent(agg_none, agg_total)

        insta_data["agg_low_count"] = agg_low
        insta_data["agg_low_percent"] = to_percent(agg_low, agg_total)

        insta_data["agg_med_count"] = agg_med
        insta_data["agg_med_percent"] = to_percent(agg_med, agg_total)

        insta_data["agg_high_count"] = agg_high
        insta_data["agg_high_percent"] = to_percent(agg_high, agg_total)

        # Geolocations
        locations = as_locations(mongo().ca_db.top_lon_lat_and_count(SOURCE_INSTAGRAM), limit=1000)
        insta_data["locations"] = locations

    if expl_data is None:
        print("Creating data exploration view model...")
        expl_data = dict()

        # Top Token Data
        top_tokens = as_tokens(mongo().ca_db.total_unclean_top_post_tokens(), limit=25)
        expl_data["top_tokens"] = top_tokens

        # Top Hashtag Data
        top_hashtags = as_hashtags(mongo().ca_db.total_top_hashtags(), limit=25)
        expl_data["top_hashtags"] = top_hashtags

        # Top Named Entities
        top_entities = as_named_entities(mongo().ca_db.total_top_named_entities(), limit=25)
        expl_data["top_entities"] = top_entities

    print("Loaded view models.")


@app.route('/')
@app.route('/index')
def index():
    global twitter_data
    global insta_data
    global expl_data

    init_data()
    user = as_user(mongo().ca_db.find_random_user())

    # template
    return render_template("index.html", user=user, twitter_data=twitter_data, insta_data=insta_data, expl_data=expl_data)


@app.route('/lda2.html')
def main():
    return render_template('lda2.html')


init_data()
