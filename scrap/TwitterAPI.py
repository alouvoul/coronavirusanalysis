import json
import time
from typing import Callable
import urllib.parse

try:
    import tweepy
except ImportError:
    print("ERROR: tweepy is required.")
    print("Please install TweePy using the command:")
    print("$ pip install tweepy")
    exit(1)

# api_key = ""
# api_secret = ""
#
# access_token = ""
# access_token_secret = ""


class TwitterAPI:
    """
    Class that connects to a Twitter developer accoynt and fetches data via the API
    """
    api = None
    auth = None

    def __init__(self):
        credentials = self.read_credentials()
        self.auth = tweepy.OAuthHandler(credentials["api_key"], credentials["api_secret"])
        self.auth.set_access_token(credentials["access_token"], credentials["access_token_secret"])

        self.api = tweepy.API(auth_handler=self.auth,
                              wait_on_rate_limit=True,
                              wait_on_rate_limit_notify=True)

    @staticmethod
    def read_credentials():
        """
        Method which is used to read credentials for twitter development account. Used to init the connection in the
        __init__ method of the same class.
        :return: A dictionary with credentials
        """
        file = ""
        try:
            with open("credentials.json", "r") as c:
                file = json.load(c)
        except IOError:
            print("Can't reach credentials file")
            exit(1)

        credentials = {
            "api_key": file["api_key"],
            "api_secret": file["api_secret"],
            "access_token": file["access_token"],
            "access_token_secret": file["access_token_secret"],
        }
        return credentials

    def get_tweet(self, tweet_id):
        """
        Retrieves a tweet from the API
        :param tweet_id:id tweet
        :return:
        """
        try:
            return self.api.get_status(tweet_id, include_entities=True, trim_user=False, tweet_mode="extended")
        except tweepy.RateLimitError:
            print("Rate limit reached sleeping for 15 minutes.")
            time.sleep(15.5 * 60)
            return self.get_tweet(tweet_id)
        except tweepy.TweepError:
            print("Error getting tweet.")
            return None

    def search(self, query, latest_tweet_id=None):
        """
        Searches for tweets based on a given query, containing specific hashtags related to the subject
        :param query:
        :param latest_tweet_id:
        :return:
        """
        encoded_query = urllib.parse.quote(query, encoding="UTF-8")

        if latest_tweet_id is None:
            return tweepy.Cursor(self.api.search, q=encoded_query, lang="en")
        else:
            return tweepy.Cursor(self.api.search, q=encoded_query, lang="en", since_id=latest_tweet_id)


class TWUser:
    """
    Class that retrieves Twitter Users via the API
    """
    user_id = None
    created_at = None
    name = ""
    screen_name = ""
    location = ""
    description = ""
    is_protected = False
    is_verified = False
    follower_count = 0
    follows_count = 0
    listed_count = 0
    tweet_count = 0
    favourites_count = 0

    def __init__(self, tweet):

        if tweet is None:
            return

        self.user_id = tweet.user.id_str
        self.created_at = tweet.user.created_at
        self.name = tweet.user.name
        self.screen_name = tweet.user.screen_name
        self.location = tweet.user.location
        self.description = tweet.user.description
        self.is_protected = tweet.user.protected
        self.is_verified = tweet.user.verified
        self.follower_count = tweet.user.followers_count
        self.follows_count = tweet.user.friends_count
        self.listed_count = tweet.user.listed_count
        self.tweet_count = tweet.user.statuses_count
        self.favourites_count = tweet.user.favourites_count

    def from_dict(self, dict_user: dict):
        user = TWUser(None)

        user.user_id = dict_user["user_id"]
        user.created_at = dict_user["created_at"]
        user.name = dict_user["name"]
        user.screen_name = dict_user["screen_name"]
        user.location = dict_user["location"]
        user.description = dict_user["description"]

        user.is_protected = dict_user["is_protected"]
        user.is_verified = dict_user["is_verified"]
        user.follower_count = dict_user["follower_count"]
        user.follows_count = dict_user["follows_count"]
        user.listed_count = dict_user["listed_count"]

        user.tweet_count = dict_user["tweet_count"]
        user.favourites_count = dict_user["favourites_count"]

        return user


class TWTweet:
    """
    Class that represents the content retrieved from the API
    """
    tweet_id = ""
    created_at = None
    user_id = ""
    text = ""
    has_attachment = False
    location_latlon = []
    location_place = {}
    hashtags = []
    tagged_users = []
    in_reply_to_user_id_str = ""
    retweet_count = 0
    fave_count = 0

    source = ""

    def _get_text(self, tweet):
        if hasattr(tweet, "full_text"):
            return tweet.full_text
        elif hasattr(tweet, "text"):
            return tweet.text

    def __has_attachment(self, tweet):
        entities = dict(tweet.entities)

        if "media" in entities.keys():
            return True
        else:
            return False

    def __get_hashtags(self, tweet):
        entities = dict(tweet.entities)

        if "hashtags" in entities.keys():
            return list(map(lambda it: it["text"], entities["hashtags"]))
        else:
            return []

    def __get_location(self, tweet):
        location = tweet.coordinates

        if location is None:
            return None
        elif isinstance(location, dict):
            return list(dict(location)["coordinates"])
        else:
            return list(location.coordinates)

    def __get_bounding_box(self, bounding_box):
        if bounding_box is None:
            return None
        elif isinstance(bounding_box, dict):
            box = dict()
            box["type"] = bounding_box["type"]
            box["coordinates"] = list(bounding_box["coordinates"])

            return box
        else:
            box = dict()
            box["type"] = bounding_box.type
            box["coordinates"] = list(bounding_box.coordinates)

            return box

    def __get_place(self, tweet):
        place = tweet.place

        if place is None:
            return None
        elif isinstance(place, dict):
            place_info = dict()
            place_info["bounding_box"] = self.__get_bounding_box(place["bounding_box"])
            place_info["name"] = place["name"]
            place_info["full_name"] = place["full_name"]
            place_info["country"] = place["country"]
            place_info["country_code"] = place["country_code"]

            return place_info
        else:
            place_info = dict()
            place_info["bounding_box"] = self.__get_bounding_box(place.bounding_box)
            place_info["name"] = place.name
            place_info["full_name"] = place.full_name
            place_info["country"] = place.country
            place_info["country_code"] = place.country_code

            return place_info

    def __get_mentioned_users(self, tweet):
        entities = dict(tweet.entities)

        if "user_mentions" in entities.keys():
            return list(map(lambda it: it["id_str"], entities["user_mentions"]))
        else:
            return []

    def __init__(self, tweet):

        if tweet is None:
            return

        self.tweet_id = tweet.id_str
        self.created_at = tweet.created_at
        self.user_id = tweet.user.id_str
        self.text = self._get_text(tweet)
        self.has_attachment = self.__has_attachment(tweet)
        self.location_latlon = self.__get_location(tweet)
        self.location_place = self.__get_place(tweet)
        self.hashtags = self.__get_hashtags(tweet)
        self.tagged_users = self.__get_mentioned_users(tweet)
        self.in_reply_to_user_id_str = tweet.in_reply_to_user_id_str
        self.retweet_count = tweet.retweet_count
        self.fave_count = tweet.favorite_count
        self.source = tweet.source

    def from_dict(self, dict_tweet: dict):
        tweet = TWTweet(None)

        tweet.tweet_id = dict_tweet["tweet_id"]
        tweet.created_at = dict_tweet["created_at"]
        tweet.user_id = dict_tweet["user_id"]
        tweet.text = dict_tweet["text"]
        tweet.has_attachment = dict_tweet["has_attachment"]
        tweet.location_latlon = dict_tweet["location_latlon"]
        tweet.location_place = dict_tweet["location_place"]
        tweet.hashtags = dict_tweet["hashtags"]
        tweet.tagged_users = dict_tweet["tagged_users"]
        tweet.in_reply_to_user_id_str = dict_tweet["in_reply_to_user_id_str"]
        tweet.retweet_count = dict_tweet["retweet_count"]
        tweet.fave_count = dict_tweet["fave_count"]
        tweet.source = dict_tweet["source"]

        return tweet


class TwitterScrapConsumer:
    user_consumer = None
    tweet_consumer = None

    def __init__(self, user_consumer: Callable[[TWUser], None], tweet_consumer: Callable[[TWTweet], None]):
        self.user_consumer = user_consumer
        self.tweet_consumer = tweet_consumer

    def accept_user(self, user: TWUser):
        self.user_consumer(user)

    def accept_tweet(self, tweet: TWTweet):
        self.tweet_consumer(tweet)


class TwitterScrap:
    """
    Core class that initiates the scraping of Twitter posts
    """
    api = None

    hashtags = []
    keywords = []

    ignore_retweets = True
    latest_id = None

    limit_minutes = 15

    def __init__(self, hashtags, keywords, ignore_retweets=True, latest_id=None):
        self.api = TwitterAPI()

        self.hashtags = hashtags
        self.keywords = keywords
        self.ignore_retweets = ignore_retweets
        self.latest_id = latest_id

    def __query_string(self):
        query_terms = []

        for hashtag in self.hashtags:
            if "#" not in hashtag:
                query_terms.append(str("#" + hashtag))
            else:
                query_terms.append(hashtag)

        for word in self.keywords:
            query_terms.append(str("\"" + word + "\""))

        query = " OR ".join(query_terms)

        if self.ignore_retweets:
            query = query + " -filter:retweets"

        return query

    def __fetch_next(self, cursor):
        while True:
            try:
                yield cursor.next()
            except tweepy.RateLimitError:
                time.sleep(self.limit_minutes * 60)
            except StopIteration:
                yield None
                break

    def find_results(self, consumer: TwitterScrapConsumer):
        print("Requesting tweets...")
        query_string = self.__query_string()
        print("Query:", query_string, "Starting from tweet:", self.latest_id)

        tweets_cursor = self.api.search(query=query_string, latest_tweet_id=self.latest_id)

        for fetched_tweet in self.__fetch_next(tweets_cursor.items()):

            if fetched_tweet is None:
                break

            if fetched_tweet.text.startswith("RT @"):
                continue

            print("Fetched tweet:", fetched_tweet)

            # handle tweet data
            user = TWUser(fetched_tweet)
            consumer.accept_user(user)

            tweet = TWTweet(fetched_tweet)
            consumer.accept_tweet(tweet)
            print("Fetching next...")

