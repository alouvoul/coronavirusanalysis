import scrap.TwitterAPI as tw
import scrap.InstaAPI as ig
from data.mongo import mongo


def save_tweet(tweet: tw.TWTweet):
    """
    Saves a Tweet object in mongo DB
    :param tweet: TWTweet object to save to MongoDB
    :return: Nothing
    """
    mongo().twitter.save_tweet(tweet)
    print("Saved tweet:", tweet.__dict__)


def save_twitter_user(user: tw.TWUser):
    """
    Saves a User object in mongo DB
    :param user: TWUser object to save to MongoDB
    :return: Nothing
    """
    mongo().twitter.save_twitter_user(user)
    print("Saved twitter user:", user.__dict__)


def twitter_scrap():
    """
    Method which used to scrap Twitter. Hashtags are declared in order to get tweets with relevant content. Twitter scrap
    consumer used to collect the tweets
    :return:
    """
    print("Scrapping twitter...")
    hashtags = ["coronavirus", "covid-19", "COVID-19", "covid19", "COVID19"]
    keywords = ["coronavirus", "corona virus", "covid-19", "COVID-19"]

    #  create a twitter scrap consumer and start scrapping
    consumer = tw.TwitterScrapConsumer(save_twitter_user, save_tweet)
    scrapper = tw.TwitterScrap(hashtags, keywords,
                               ignore_retweets=True,
                               latest_id=mongo().twitter.latest_tweet_id())

    scrapper.find_results(consumer)
    print("Twitter scrap job completed.")


def save_post(post: ig.IGPost):
    """
    Saves a IGPost object in mongo DB
    :param post: IGPost object to save to MongoDB
    :return: Nothing
    """
    mongo().instagram.save_insta_post(post)
    print("Saved post:", post.__dict__)


def save_instagram_user(user: ig.IGUser):
    """
    Saves a IGUser object in mongo DB.
    :param user: IGPost object to save to MongoDB
    :return: Nothing
    """
    mongo().instagram.save_insta_user(user)
    print("Saved instagram user:", user.__dict__)


def instagram_scrap():
    """
    Method which used to scrap Twitter. Hashtags are declared in order to get posts with relevant content.
    InstagramScrapConsumer  object used to collect the posts
    :return:
    """
    hashtag = "coronavirus"

    #  create an instagram scrap consumer and start scrapping
    consumer = ig.InstagramScrapConsumer(save_instagram_user, save_post)
    scrapper = ig.InstagramScrap(hashtag)

    scrapper.find_results(consumer)
    print("Instagram scrap job completed.")


def start_scrap_job(twitter=False, instagram=True):
    """
    Initializer of the scrap process using the 2 corresponding functions
    :param twitter: True to scrap twitter or false
    :param instagram: True to scrap instagram or false
    :return:
    """
    print("Started Scrapping...")

    # start scrapping from the appropriate source
    if twitter:
        twitter_scrap()

    if instagram:
        instagram_scrap()
# EOF
