from typing import Callable
import utils.text_utils as tu

try:
    import instaloader as ig
except ImportError:
    print("ERROR: instaloader is required.")
    print("Please install instaloader using the command:")
    print("$ pip install instaloader")
    exit(1)


class InstagramAPI:
    """
    Class that initiates the API connection for Instagram retrieval
    """
    api = None

    def __init__(self):
        self.api = ig.Instaloader(sleep=True,download_pictures=False,download_video_thumbnails=False,download_videos=False)

    def search(self, hashtag):
        return self.api.get_hashtag_posts(hashtag)


class IGUser:
    """
    Represents instagram users
    """
    user_id = ""
    username = ""

    full_name = ""
    biography = ""

    mediacount = 0
    follower_count = 0
    follows_count = 0

    is_private = False
    is_verified = False

    def __init__(self, user):

        if user is None:
            return

        self.user_id = str(user.owner_profile.userid)
        self.username = user.owner_profile.username

        self.full_name = user.owner_profile.full_name
        self.biography = user.owner_profile.biography

        self.mediacount = user.owner_profile.mediacount
        self.follower_count = user.owner_profile.followers
        self.follows_count = user.owner_profile.followees

        self.is_private = user.owner_profile.is_private
        self.is_verified = user.owner_profile.is_verified

    def from_dict(self, dict_user: dict):
        user = IGUser(None)

        user.user_id = dict_user["user_id"]
        user.username = dict_user["username"]

        user.full_name = dict_user["full_name"]
        user.biography = dict_user["biography"]

        user.mediacount = dict_user["mediacount"]
        user.follower_count = dict_user["follower_count"]
        user.follows_count = dict_user["follows_count"]

        user.is_private = dict_user["is_private"]
        user.is_verified = dict_user["is_verified"]

        return user


class IGPost:
    """
    Represents Instagram Posts
    """
    shortcode = ""
    created_at = None

    owner_id = ""
    owner_username = ""

    caption = ""
    caption_hashtags = []
    caption_mentions = []

    tagged_users = []

    likes = []
    location = None
    comments = []

    typename = ""

    def __get_location(self, post):
        location = post.location

        if location is not None:
            location_dict = dict()
            location_dict["lat"] = location.lat
            location_dict["lng"] = location.lng
            location_dict["name"] = location.name

            return location_dict

        return None

    def _get_likes(self, post):
        likes = post.get_likes()

        if likes is not None:
            likes_list = []

            for current in likes:
                likes_list.append(str(current.userid))

            return likes_list

        return []

    def __get_comments(self, post):
        comments = post.get_comments()

        if comments is not None:
            comments_list = []

            for current in comments:
                comment_dict = dict()

                comment_dict["owner_id"] = str(current.owner.userid)
                comment_dict["text"] = current.text
                comment_dict["likes_count"] = current.likes_count
                comment_dict["reply_count"] = 0

                if hasattr(current, "answers") and current.answers is not None:
                    count = 0

                    for reply in current.answers:
                        count += 1

                    comment_dict["reply_count"] = count

                comments_list.append(comment_dict)

            return comments_list

        return []

    def __init__(self, post):

        if post is None:
            return

        self.shortcode = post.shortcode
        self.created_at = post.date_utc

        self.owner_id = str(post.owner_id)
        self.owner_username = post.owner_username

        self.caption = post.caption
        self.caption_hashtags = post.caption_hashtags
        self.caption_mentions = post.caption_mentions

        self.tagged_users = post.tagged_users

        self.likes = self._get_likes(post)
        self.location = self.__get_location(post)
        self.comments = self.__get_comments(post)

        self.typename = post.typename

    def from_dict(self, dict_post: dict):
        post = IGPost(None)

        post.shortcode = dict_post["shortcode"]
        post.created_at = dict_post["created_at"]

        post.owner_id = dict_post["owner_id"]
        post.owner_username = dict_post["owner_username"]

        post.caption = dict_post["caption"]
        post.caption_hashtags = dict_post["caption_hashtags"]
        post.caption_mentions = dict_post["caption_mentions"]

        post.tagged_users = dict_post["tagged_users"]

        post.likes = dict_post["likes"]
        post.location = dict_post["location"]
        post.comments = dict_post["comments"]

        post.typename = dict_post["typename"]

        return post


class InstagramScrapConsumer:
    user_consumer = None
    tweet_consumer = None

    def __init__(self, user_consumer: Callable[[IGUser], None], tweet_consumer: Callable[[IGPost], None]):
        self.user_consumer = user_consumer
        self.tweet_consumer = tweet_consumer

    def accept_user(self, user: IGUser):
        self.user_consumer(user)

    def accept_post(self, tweet: IGPost):
        self.tweet_consumer(tweet)


class InstagramScrap:
    api = None
    hashtag = ""

    def __init__(self, hashtag):
        self.api = InstagramAPI()
        self.hashtag = hashtag

    def find_results(self, consumer: InstagramScrapConsumer):
        print("Requesting instagram posts...")
        print("Hashtag:", self.hashtag)

        for fetched_post in self.api.search(self.hashtag):

            if fetched_post is None:
                break

            print("Fetched post:", fetched_post)

            text = fetched_post.caption

            if not tu.is_blank(text):
                # If post is not pure english continue
                if not tu.is_fully_english(text):
                    print("Skipping post because of incompatible language.")
                    print("Text '", text, "' is not english!")
                    continue

            # handle instagram data
            user = IGUser(fetched_post)
            consumer.accept_user(user)

            post = IGPost(fetched_post)
            consumer.accept_post(post)
            print("Fetching next...")
