from textblob import TextBlob
from vaderSentiment import vaderSentiment

from data.mongo import mongo

import pandas as pd


class UnsupervisedEmotion:
    filename = "NRC-Emotion-Lexicon-Wordlevel-v0.92.txt"

    def __init__(self):
        self.data = pd.read_csv(self.filename, names=["text", "emotion", "association"], sep='\t')
        self.emotion_lexicon = self.data.pivot(index='text', columns='emotion', values='association')
        self.total_words_in_emotion_lexicon = self.data.iloc[:, 0].tolist()

    def transform(self):
        # All words in lexicon. Will be used to check each name of the post if exist.

        tweets = mongo().twitter.get_all_tweets()

        for tweet in tweets:
            anger = disgust = fear = joy = sadness = surprise = 0
            tokens = tweet["preprocessed_text"]
            emotion = None
            for token in tokens:
                if token in self.total_words_in_emotion_lexicon:
                    emotion = self.emotion_lexicon.loc[token]
                    anger += emotion.anger
                    disgust += emotion.disgust
                    fear += emotion.fear
                    joy += emotion.joy
                    sadness += emotion.sadness
                    surprise += emotion.surprise

            if emotion is None:
                continue

            num_of_words = len(tokens)
            emotions = {
                "anger": anger / num_of_words,
                "disgust": disgust / num_of_words,
                "fear": fear / num_of_words,
                "joy": joy / num_of_words,
                "sadness": sadness / num_of_words,
                "surprise": surprise / num_of_words}

            mongo().twitter.update_tweet(tweet["tweet_id"], column="emotions", value=emotions)


def start_emotion_prediction():
    model = UnsupervisedEmotion()
    model.transform()


class UnsupervisedSentiment:

    def text_blob_sentiment(self, data):
        """
        Method that predicts sentiment of a sentence using the TextBlob library.

        :param data: String sentence to predict the sentiment
        :return: Two values of polarity and subjectivity respectively
        """
        sentiment = TextBlob(' '.join(map(str, data))).sentiment
        return sentiment.polarity, sentiment.subjectivity

    def vader_sentiment(self, data):
        """
        Method that predicts sentiment of a sentence using the VADER library. The values are [-1.0, 1.0] which
        less than zero are negative sentiment and more than zero are positive sentiment.

        :param data: String sentence to predict the sentiment
        :return: Polarity score of the sentence which is [-1.0, 1.0].
        """
        vader = vaderSentiment.SentimentIntensityAnalyzer()
        return vader.polarity_scores(data)

    def polarity(self, polarity, bound=0.0):
        """
        Calculates the result of the polarity with the bounds given. For example for VADER values -0.5, 0.03 and 0.2
        are negative, neutral, and positive respectively.
        :param polarity:
        :param bound:
        :return:
        """
        if polarity == 0 or (-bound < polarity < bound):
            status = "Neutral"
        elif polarity >= bound:
            status = "Positive"
        else:
            status = "Negative"
        return status


def start_sentiment_prediction(twitter_field='text', instagram_field='caption'):
    """

    :param twitter_field:
    :param instagram_field:
    :return:
    """
    tweets = mongo().twitter.get_all_tweets()
    posts = mongo().instagram.get_all_posts()
    model = UnsupervisedSentiment()
    for t in tweets:
        polarity, subjectivity = model.text_blob_sentiment(t[twitter_field])
        sentiment_blob = model.polarity(polarity)

        # In vader compound of <0.05 and >-0.05 declares a neutral sentence
        polarity = model.vader_sentiment(t[twitter_field])
        sentiment_vader = model.polarity(polarity["compound"], bound=0.05)
        print(t[twitter_field])
        print(sentiment_blob + " - " + sentiment_vader)
        print("\n")
        # TODO add results to mongoDB
        # MongoDB.TwitterDB.update_tweet(mongo().twitter, t["tweet_id"], "textblob_polarity", sentiment_blob)
        # MongoDB.TwitterDB.update_tweet(mongo().twitter, t["tweet_id"], "vader_polarity", sentiment_vader)

    for post in posts:
        polarity, subjectivity = model.text_blob_sentiment(post[instagram_field])
        text_blob = model.polarity(polarity)
        sentiment_vader = model.vader_sentiment(post[instagram_field])
        # TODO add results to mongoDB
        # MongoDB.InstagramDB.update_tweet(mongo().instagram, t["tweet_id"], "textblob_polarity", polarity)
