from geopy.geocoders import GoogleV3

from data.mongo import mongo
from scrap.InstaAPI import IGPost
from scrap.TwitterAPI import TWTweet


def bounding_box_center(vertices):
    """
    Find the center location of the bounding box -- simple math
    :param vertices: a list of lat lon tuples or arrays
    :return:
    """
    lat_list = [geopoint[0] for geopoint in vertices]
    lon_list = [geopoint[1] for geopoint in vertices]

    center_lat = sum(lat_list) / len(vertices)
    center_lon = sum(lon_list) / len(vertices)

    return [center_lat, center_lon]


location_cache = dict()


def geodecode(name):
    """
    Geo decode a locations lat lot from it's name
    :param name: the locations name
    :return: the location's lat lon in an array [lat,lon] or None
    """
    global location_cache

    if len(location_cache.keys()) > 1000:
        location_cache = dict()

    if name in location_cache.keys():
        return location_cache[name]

    try:
        geolocator = GoogleV3(api_key="")
        location = geolocator.geocode(name)
    except Exception:
        return None

    if location is None:
        location_cache[name] = None
        return None

    lat = location.latitude
    lon = location.longitude

    if lat is None or lon is None:
        location_cache[name] = None
        return None

    location_cache[name] = [lat, lon]
    return [lat, lon]


def location_of_tweet(tweet: TWTweet):
    """
    Find the location of the tweet
    :param tweet:
    :return: array or None - location [lat,lon]
    """
    # tweet already has location
    if tweet.location_latlon is not None:
        lat = tweet.location_latlon[0]
        lon = tweet.location_latlon[1]

        return [lat, lon]

    # try to find location from place bounding box
    if tweet.location_place is not None:
        bounding_box = tweet.location_place["bounding_box"]

        if "coordinates" in bounding_box.keys():
            coords = tweet.location_place["bounding_box"]["coordinates"]

            if len(coords) == 1:
                corners = coords[0]
                latlon = bounding_box_center(corners)

                if latlon is not None:
                    return latlon

    # worst effort - find location of user from profile
    twitter = mongo().twitter
    twitter_user = twitter.get_user_of_tweet(tweet)

    if twitter_user is None:
        return None

    # try to find location from user location
    if "location" in twitter_user.keys():
        location_name = twitter_user["location"]

        if location_name is None:
            return None

        latlon = geodecode(location_name.strip())

        if latlon is not None:
            return latlon

    return None


def location_of_ig_post(post: IGPost):
    """
    Find the location of the tweet
    :param post: the instagram post
    :return: array or None - location [lat,lon]
    """
    # try to find location from post
    if post.location is not None:
        lat = None
        lon = None

        if "lat" in post.location.keys():
            lat = post.location["lat"]

        if "lng" in post.location.keys():
            lon = post.location["lng"]

        if lat is not None and lon is not None:
            return [lat, lon]

        if "name" in post.location.keys():
            place = post.location["name"]

            if place is not None:
                latlon = geodecode(place)

                if latlon is not None:
                    return latlon
    # no fallback for instagram
    return None
