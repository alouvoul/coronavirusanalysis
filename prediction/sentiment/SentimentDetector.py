from prediction.sentiment.Sentiment import Sentiment
from prediction.sentiment.SentimentClassifier import SentimentClassifier
from prediction.sentiment.UnsupervisedSentimentClassifier import UnsupervisedSentimentClassifier


class SentimentDetector:
    """
    This class handles the detection of sentiments from a posts text
    """

    features_extractor = None
    unsupervised_classifier = None
    supervised_classifier = None

    def __init__(self):
        self.supervised_classifier = SentimentClassifier()
        self.unsupervised_classifier = UnsupervisedSentimentClassifier()
        self.features_extractor = self.supervised_classifier.feature_extractor

    def detect_sentiment(self, text):
        """
        Detect text sentiment using the hybrid approach
        :param text: the textual content of a post
        :return: Sentiment instance
        """
        words = self.features_extractor.transform_string(text)

        supervised_sentiment = self.supervised_classifier.sentiment_of(words)
        unsupervised_sentiment = self.unsupervised_classifier.sentiments_of_words(words)

        sentiment = Sentiment()
        sentiment.values_by_averaging([supervised_sentiment, unsupervised_sentiment])

        return sentiment

    def detect_sentiment_supervised(self, text):
        """
        Detect text sentiment using the supervised approach
        :param text: the textual content of a post
        :return: Sentiment instance
        """
        words = self.features_extractor.transform_string(text)

        supervised_sentiment = self.supervised_classifier.sentiment_of(words)
        return supervised_sentiment

    def detect_sentiment_unsupervised(self, text):
        """
        Detect text sentiment using the unsupervised approach
        :param text: the textual content of a post
        :return: Sentiment instance
        """
        words = self.features_extractor.transform_string(text)

        unsupervised_sentiment = self.unsupervised_classifier.sentiments_of_words(words)
        return unsupervised_sentiment
