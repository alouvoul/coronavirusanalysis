class Sentiment:
    """
    A model class to hold sentiment values
    """
    negative = 0.0
    neutral = 0.0
    positive = 0.0

    def __init__(self, negative: float = 0.0, neutral: float = 0.0, positive: float = 0.0):
        self.negative = negative
        self.neutral = neutral
        self.positive = positive

    def __dict__(self):
        dict_self = dict()

        dict_self["negative"] = self.negative
        dict_self["neutral"] = self.neutral
        dict_self["positive"] = self.positive

        return dict_self

    def to_dict(self):
        return self.__dict__()

    def from_dict(self, dict_self):
        self.positive = dict_self["positive"]
        self.neutral = dict_self["neutral"]
        self.negative = dict_self["negative"]

    def __repr__(self) -> str:
        return self.to_dict().__repr__()

    def values_by_averaging(self, sentiments: list):
        sum_positive = 0
        sum_negative = 0
        sum_neutral = 0

        for sentiment in sentiments:
            sum_positive += sentiment.positive
            sum_neutral += sentiment.neutral
            sum_negative += sentiment.negative

        if sum_positive > 0.0:
            self.positive = sum_positive / len(sentiments)

        if sum_negative > 0.0:
            self.negative = sum_negative / len(sentiments)

        if sum_neutral > 0.0:
            self.neutral = sum_neutral / len(sentiments)

    def inverse(self):
        sentiment = Sentiment()

        sentiment.neutral = 1.0 - self.neutral
        sentiment.positive = 1.0 - self.negative
        sentiment.negative = 1.0 - self.positive

        return sentiment

    def max_sentiment_label(self):
        """
        Find the max sentiment label
        :return: string - the label of the sentiment with the higher value
        """
        label = None
        max = -1

        dict_self = self.__dict__()

        for key in dict_self.keys():
            value = dict_self[key]

            if value > max:
                label = key
                max = value

        return label

    def has_value(self):
        return self.positive > 0 or self.neutral > 0 or self.negative > 0
