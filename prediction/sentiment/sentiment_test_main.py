import pandas as pd

from prediction.sentiment.SentimentDetector import SentimentDetector
from utils.path_utils import resource_path

if __name__ == "__main__":
    """
    Test main for testing sentiment model
    """
    sent_detector = SentimentDetector()

    tp = 0
    tn = 0
    fp = 0
    fn = 0

    filepath = resource_path("/semeval2017_subtask_a/twitter-2016devtest-A.txt")
    raw_data = pd.read_csv(filepath, delimiter="\t")

    for row in raw_data.values:
        true_sentiment = row[1]
        text = row[2]

        print("\n\nTesting: '", text, "'.")

        sent = sent_detector.detect_sentiment(text)

        if sent.max_sentiment_label() == true_sentiment:
            tp += 1
            print("Found correct prediction:", true_sentiment)
        else:
            fp += 1
            print("Found wrong prediction:", sent.max_sentiment_label(), "Expected:", true_sentiment)

        print()
        exit(0)

    print("Correct predictions:", tp, "Incorrect:", fp)



