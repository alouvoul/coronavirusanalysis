import re

import pandas as pd
import nltk

from prediction.like_prediction.ContextDetector import ContextDetector
from prediction.sentiment.UnsupervisedSentimentClassifier import UnsupervisedSentimentClassifier
from process.tokenizing import preprocess_text

unknown = -1
negative = 0
neutral = 2
positive = 4


def to_string_label(label: int):
    """
    Convert integer label to string label
    :param label: the integer constant label of sentiment
    :return: string label
    """
    if label == neutral:
        return "neutral"
    elif label == positive:
        return "positive"
    elif label == negative:
        return "negative"
    else:
        return "unknown"


def transform_label(label: str):
    """
    Convert string label to int constant label
    :param label: string label of sentiment
    :return: integer label
    """
    if label == "neutral":
        return neutral
    elif label == "positive":
        return positive
    elif label == "negative":
        return negative
    else:
        return unknown


class SentimentFeatureExtractor:
    """
    Extracts features for sentiment detection
    """
    sentiment_extractor = None

    def __init__(self):
        self.sentiment_extractor = UnsupervisedSentimentClassifier()
        # print("Created Sentiment Feature Extractor..")

    def extract_transform_dataset(self, data: pd.DataFrame):
        """
        Transform an entire dataframe to a Dataframe with the extracted features
        :param data: the dataframe for the relevant dataset
        :return: list of rows containing the transformed dataset
        """
        dataset = []

        for row in data.values:
            # print("transforming sentiment row...")
            rid = row[0]
            label = transform_label(row[1])
            tweet = self.transform_string(row[2])

            if tweet is None or len(tweet) == 0:
                continue

            dataset.append([tweet, label])

        return dataset

    def transform_string(self, text: str):
        """
        Convert a string of text to a cleaned string containing the extracted features
        :param text: the string of text
        :return: string - cleaned string with extracted features
        """

        # count hashtags and mentions before preprocessing the string
        hashtag_matches = re.findall(r"#\w+", text)

        if hashtag_matches is None:
            hashtag_matches = []

        tag_matches = re.findall(r"@\w+", text)

        if hashtag_matches is None:
            tag_matches = []

        tokens = preprocess_text(text, discard_limited_tokens=False)

        if tokens is None or len(tokens) == 0:
            return ""

        return self.__transform_tokens(tokens, len(hashtag_matches), len(tag_matches))

    def __transform_tokens(self, tokens: list, hashtag_count: int = 0, tag_count: int = 0):
        """
        Convert a list of preprocessed tokens to a cleaned string containing the extracted features
        :param hashtag_count: the number of hastags
        :param tag_count: the number of mentions
        :param tokens: a list of preprocessed tokens
        :return: string - cleaned string with extracted features
        """
        if tokens is None:
            return None

        output_tokens = []

        period_counts = 0
        multi_period_count = 0

        exclamation_counts = 0
        multi_exclamation_count = 0

        question_counts = 0
        multi_question_count = 0

        negation_active = False
        negatable = set()

        # init negation vars
        if "SEMANTIC_NEGATION" in tokens:
            pos_tags = nltk.pos_tag(tokens)
            negatable = [word_tag_pair[0] for word_tag_pair in pos_tags if word_tag_pair[1] in ["JJ", "JJR", "JJS"]]
            negatable = set([word.replace(".", "").replace("?", "").replace("!", "").strip() for word in negatable])

        for token in tokens:

            is_sentence_end = re.search(r"(\.+)|(!+)|(\?+)", token) is not None

            # detect punctuation
            if re.search(r"\.", token) is not None:
                period_counts += 1

            if re.search(r"\.{2,}", token) is not None:
                multi_period_count += 1

            if re.search(r"!", token) is not None:
                exclamation_counts += 1

            if re.search(r"!{2,}", token) is not None:
                multi_exclamation_count += 1

            if re.search(r"\?", token) is not None:
                question_counts += 1

            if re.search(r"\?{2,}", token) is not None:
                multi_question_count += 1

            # clean punctuation
            token = token.replace(".", "").replace("?", "").replace("!", "")

            # detect flooding
            if re.search(r"(?=\w)(\w)(\1{2,})", token):
                output_tokens.append("FEATURE_WORD_FLOODING")

                if not self.sentiment_extractor.sentiments_of(token).has_value():

                    token_no_flooding = re.sub(r"(?=\w)(\w)(\1{2,})", "\\1", token)

                    if self.sentiment_extractor.sentiments_of(token_no_flooding).has_value():
                        token = token_no_flooding

            # handle detection
            if token is None or len(token) == 0:

                if is_sentence_end:
                    negation_active = False

                continue

            if token == "SEMANTIC_NEGATION":
                negation_active = not negation_active

            negate_token = negation_active and token in negatable and token != "SEMANTIC_NEGATION"
            unsupervised_sent = self.sentiment_extractor.sentiments_of(token)


            # add strong sentiment features
            if unsupervised_sent.negative >= 0.3:
                if not negate_token:
                    output_tokens.append("FEATURE_NEGATIVE_WORD")
                else:
                    output_tokens.append("FEATURE_POSITIVE_WORD")

            if unsupervised_sent.positive >= 0.3:
                if negate_token:
                    output_tokens.append("FEATURE_NEGATIVE_WORD")
                else:
                    output_tokens.append("FEATURE_POSITIVE_WORD")

            # handle negation continued
            if negate_token:
                token = "NOT_" + token
                negation_active = False

            if is_sentence_end:
                negation_active = False

            if token == "SEMANTIC_NEGATION":
                continue

            output_tokens.append(token)

        # add extra features
        hashtag_ratio = 0
        tag_ratio = 0

        if len(tokens) > 0:
            hashtag_ratio = hashtag_count / len(tokens)
            tag_ratio = tag_count / len(tokens)

        if hashtag_count > 0:
            output_tokens.append("FEATURE_HAS_HASHTAGS")

        if tag_count > 0:
            output_tokens.append("FEATURE_HAS_TAGS")

        if 0.0 < hashtag_ratio <= 0.33:
            output_tokens.append("FEATURE_HASHTAG_RATIO_LOW")
        elif 0.33 < hashtag_ratio <= 0.66:
            output_tokens.append("FEATURE_HASHTAG_RATIO_MED")
        elif 0.66 < hashtag_ratio <= 1.0:
            output_tokens.append("FEATURE_HASHTAG_RATIO_HIGH")

        if 0.0 < tag_ratio <= 0.33:
            output_tokens.append("FEATURE_TAG_RATIO_LOW")
        elif 0.33 < tag_ratio <= 0.66:
            output_tokens.append("FEATURE_TAG_RATIO_MED")
        elif 0.66 < tag_ratio <= 1.0:
            output_tokens.append("FEATURE_TAG_RATIO_HIGH")

        if period_counts > 0:
            output_tokens.append("FEATURE_HAS_PERIOD")

        if question_counts > 0:
            output_tokens.append("FEATURE_HAS_QUESTION_MARK")

        if exclamation_counts > 0:
            output_tokens.append("FEATURE_HAS_EXCLAMATION")

        if multi_period_count > 0:
            output_tokens.append("FEATURE_HAS_MULTI_PERIOD")

        if multi_question_count > 0:
            output_tokens.append("FEATURE_HAS_MULTI_QUESTION_MARK")

        if multi_exclamation_count > 0:
            output_tokens.append("FEATURE_HAS_MULTI_EXCLAMATION")

        transformed_str = " ".join(output_tokens)

        return transformed_str
