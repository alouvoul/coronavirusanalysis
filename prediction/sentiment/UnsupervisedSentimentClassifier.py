import re

import pandas as pd
from textblob import TextBlob
from vaderSentiment import vaderSentiment

from prediction.sentiment.Sentiment import Sentiment
from utils.path_utils import resource_path


class UnsupervisedSentimentClassifier:
    """
    This class is responsible for detecting the sentiment of a post using unsupervised methods
    and specifically by using a lexicon, Vader and TextBlob
    """
    data = None

    def __init__(self):
        filename = resource_path("SentiWordNet_custom.csv")
        self.data = pd.read_csv(filename, delimiter=",")

    def sentiments_of(self, word: str):
        """
        Detect the sentiments of a token
        :param word: a token
        :return: Sentiment instance
        """
        # handle negation
        is_negation = re.search(r"NOT_", word) is not None

        if is_negation:
            word = word.replace("NOT_", "")

        # use vader, textblob and lexicon to find sentiment
        vader = vaderSentiment.SentimentIntensityAnalyzer()
        vader_polarity = vader.polarity_scores(word)

        tb_polarity, tb_subjectivity = TextBlob(word).sentiment

        result_df = self.data.loc[self.data["word"] == word]

        sentiment = Sentiment()

        # average detected sentiments
        if vader_polarity is not None:
            sentiment.positive = vader_polarity["pos"] / 2.0
            sentiment.neutral = vader_polarity["neu"] / 2.0
            sentiment.negative = vader_polarity["neg"] / 2.0

        if tb_polarity < 0.0:
            sentiment.negative += tb_polarity / 2.0
        elif tb_polarity > 0.0:
            sentiment.positive += tb_polarity / 2.0

        if result_df is None or result_df.empty:
            return sentiment

        matching_row = result_df.values[0]
        sentiment_values = [matching_row[1], matching_row[2], matching_row[3]]

        positive = sentiment_values[0]
        negative = sentiment_values[1]
        objective = sentiment_values[2]

        sentiment.values_by_averaging([Sentiment(positive=positive, negative=negative, neutral=objective), sentiment])

        # invert sentiment if negation
        if is_negation:
            sentiment = sentiment.inverse()

        return sentiment

    def sentiments_of_words(self, words: list):
        """
        Detect the sentiment by the posts preprocessed tokens
        :param words: a list of preprocessed tokens
        :return: Sentiment instance
        """

        if len(words) == 0:
            return Sentiment()

        # find sentiment for each tokens
        sentiments = [self.sentiments_of(word) for word in words if re.search(r"(FEATURE_)|(SEMANTIC_)", word) is None]

        # find average by token
        sentiment = Sentiment()
        sentiment.values_by_averaging(sentiments)

        # reconstruct sentence from tokens
        reconstructed_words = []

        for word in words:

            if re.search(r"(FEATURE_)|(SEMANTIC_)", word) is not None:
                continue

            if re.search("NOT_", word) is not None:
                word = word.replace("NOT_", "")
                reconstructed_words.append("not")

            reconstructed_words.append(word)

        # reconstruct sentence string and detect sentiment by sentence level
        text = " ".join(reconstructed_words)
        vader = vaderSentiment.SentimentIntensityAnalyzer()
        vader_polarity = vader.polarity_scores(text)

        tb_polarity, tb_subjectivity = TextBlob(text).sentiment

        sentiment_2 = Sentiment()

        # average vader and textblob
        if vader_polarity is not None:
            sentiment_2.positive = vader_polarity["pos"] / 2.0
            sentiment_2.neutral = vader_polarity["neu"] / 2.0
            sentiment_2.negative = vader_polarity["neg"] / 2.0

        if tb_polarity < 0.0:
            sentiment_2.negative += tb_polarity / 2.0
        elif tb_polarity > 0.0:
            sentiment_2.positive += tb_polarity / 2.0

        # average sentiment by word level with sentiment by sentence level
        sentiment.values_by_averaging([sentiment, sentiment_2])

        return sentiment
