import os
import pickle

import pandas as pd
import sklearn.metrics as metrics
from imblearn.under_sampling import NearMiss, ClusterCentroids
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import BernoulliNB
from sklearn.svm import SVC

from prediction.sentiment.Sentiment import Sentiment
from prediction.sentiment.SentimentFeatureExtractor import SentimentFeatureExtractor, positive, neutral, negative
from utils.path_utils import resource_path


class SentimentClassifier:
    """
    This class handles the load/create of the ML model for sentiment analysis
    """
    feature_extractor = None

    vectorizer_filename = resource_path("models/sentiment_count_vec.sav")
    vectorizer = None

    model_filename = resource_path("models/sentiment_ml_model.sav")
    model = None

    def __init__(self):
        #print("Loading sentiment model..")
        self.feature_extractor = SentimentFeatureExtractor()

        if os.path.exists(self.vectorizer_filename) and os.path.exists(self.model_filename):
            try:
                self.vectorizer = pickle.load(open(self.vectorizer_filename, "rb"))
                self.model = pickle.load(open(self.model_filename, "rb"))

                #print("Loaded persisted sentiment model.")

                return
            except Exception:
                print("Failed to load model. Resuming model creation...")

        if os.path.exists(self.vectorizer_filename):
            os.remove(self.vectorizer_filename)

        if os.path.exists(self.model_filename):
            os.remove(self.model_filename)

        filepath = resource_path("/semeval2017_subtask_a/twitter-2016train-A.txt")
        raw_data = pd.read_csv(filepath, delimiter="\t")
        data = self.feature_extractor.extract_transform_dataset(raw_data)

        X_train = [data[i][0] for i in range(0, len(data))]
        y_train = [data[i][1] for i in range(0, len(data))]

        filepath = resource_path("/semeval2017_subtask_a/twitter-2016test-A.txt")
        raw_data = pd.read_csv(filepath, delimiter="\t")
        data = self.feature_extractor.extract_transform_dataset(raw_data)

        X_test = [data[i][0] for i in range(0, len(data))]
        y_test = [data[i][1] for i in range(0, len(data))]

        print("Vectorizing dataset text...")

        # create count vectorizer
        cv_stop_words = "english"
        cv_max_df = 0.85
        cv_lowercase_transform = True

        if self.vectorizer is None:
            self.vectorizer = CountVectorizer(strip_accents='unicode',
                                              analyzer="word",
                                              token_pattern=r"[\w\-:]+",
                                              stop_words=cv_stop_words,
                                              max_df=cv_max_df,
                                              ngram_range=(1, 2),
                                              lowercase=cv_lowercase_transform)
            X_train = self.vectorizer.fit_transform(X_train)
        else:
            X_train = self.vectorizer.transform(X_train)

        print("Persisting vectorizer...")

        pickle.dump(self.vectorizer, open(self.vectorizer_filename, "wb"))

        undersampler = NearMiss()
        X_train, y_train = undersampler.fit_resample(X_train, y_train)

        print("Training sentiment analysis models...")

        svm = SVC(kernel="rbf", probability=True)
        # nb = BernoulliNB()
        # rf = RandomForestClassifier()

        # create estimators list
        # estimators = [("svm", svm), ("nb", nb), ("rf", rf)]

        # create voting classifier
        self.model = svm # VotingClassifier(estimators, voting="soft")
        self.model.fit(X_train, y_train)

        print("Prediction test metrics:")
        X_test = self.vectorizer.transform(X_test)
        y_pred = self.model.predict(X_test)

        accuracy = metrics.accuracy_score(y_test, y_pred)
        recall = metrics.recall_score(y_test, y_pred, average="macro")
        precision = metrics.precision_score(y_test, y_pred, average="macro")
        f1 = metrics.f1_score(y_test, y_pred, average="macro")

        # Best results so far
        # Accuracy: 0.647059
        # Recall: 0.647703
        # Precision: 0.688056
        # F1: 0.650378

        print("\tAccuracy: %f" % accuracy)
        print("\tRecall: %f" % recall)
        print("\tPrecision: %f" % precision)
        print("\tF1: %f" % f1)

        print("Persisting Models...")
        pickle.dump(self.model, open(self.model_filename, "wb"))

    def sentiment_of(self, words: list):
        """
        Detect the sentiment of the tokens
        :param words: a list of preprocessed tokens
        :return: Sentiment instance
        """
        x = self.vectorizer.transform([words])

        prediction = self.model.predict(x)
        sentiment = Sentiment()

        if prediction == positive:
            sentiment.positive = 1.0
        elif prediction == neutral:
            sentiment.neutral = 1.0
        elif prediction == negative:
            sentiment.negative = 1.0

        return sentiment
