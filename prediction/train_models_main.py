import gc

from prediction.emotion.EmotionDetector import EmotionDetector
from prediction.like_prediction.BaseModelDetector import BaseModelDetector
from prediction.like_prediction.ContextDetector import ContextDetector
from prediction.profiling.age.AgeDetector import AgeDetector
from prediction.profiling.gender.GenderDetector import GenderDetector
from prediction.profiling.aggression.AggressionDetector import AggressionDetector
from prediction.profiling.sarcasm.SarcasmDetector import SarcasmDetector
from prediction.sentiment.SentimentDetector import SentimentDetector

if __name__ == '__main__':
    """
    Train all models.....
    Make sure you have a lot of free RAM!!!!
    """
    # d = SentimentDetector()
    # del d
    # gc.collect()

    # d = EmotionDetector()
    # del d
    # gc.collect()
    #
    # d = AgeDetector()
    # del d
    # gc.collect()
    #
    # d = GenderDetector()
    # del d
    # gc.collect()

    d = AggressionDetector()
    del d
    gc.collect()

    # d = SarcasmDetector()
    # del d
    # gc.collect()

    # d = BaseModelDetector()
    # del d
    # gc.collect()
    #
    # d = ContextDetector()
    # del d
    # gc.collect()
