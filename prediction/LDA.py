import gensim
import pyLDAvis
import pyLDAvis.gensim  # don't skip this
import matplotlib.pyplot as plt
from gensim import corpora, models

from data.mongo import mongo
from process.tokenizing import preprocess_text, stop_words
from scrap.InstaAPI import IGPost
from scrap.TwitterAPI import TWTweet

visualize_topics = True


def __tokens_of(text):
    discard = ["url", "amp"]

    for stopword in stop_words():
        discard.append(stopword)

    tokens = preprocess_text(text, discard_limited_tokens=False)
    tokens = [token for token in tokens if len(token) > 2 and "_" not in token and token not in discard and token is not None]

    return tokens


def lda_topics():
    """
    This function performs LDA analysis on the combination of Twitter and Instagram posts and extracts topics
    Simultaneously, it also visualises the topics based on the PyLDAVis library and saves the result as an HTML file
    :return:
    """
    global visualize_topics
    texts = []

    for tweet_dict in mongo().twitter.get_all_tweets():
        tweet = TWTweet(None).from_dict(tweet_dict)

        tokens = __tokens_of(tweet.text)

        if len(tokens) < 1:
            continue

        texts.append(tokens)

    for post_dict in mongo().instagram.get_all_posts():
        post = IGPost(None).from_dict(post_dict)

        tokens = __tokens_of(post.caption)

        if len(tokens) < 1:
            continue

        texts.append(tokens)

    num_topics = 10
    dictionary = gensim.corpora.Dictionary(texts)

    dictionary.filter_extremes(no_below=15, no_above=0.5, keep_n=100000)
    bow_corpus = [dictionary.doc2bow(doc) for doc in texts]
    tfidf = models.TfidfModel(bow_corpus)
    corpus_tfidf = tfidf[bow_corpus]
    lda_model = gensim.models.LdaMulticore(bow_corpus, num_topics=num_topics, id2word=dictionary, passes=2, workers=2)

    topics = []

    for idx, topic in lda_model.print_topics(-1):

        topic_dict = dict()
        topic_dict["topic_id"] = idx
        topic_dict["topic_name"] = "topic_"+str(idx)
        topic_dict["significance"] = num_topics - idx

        words = topic.split(" + ")
        topic_terms = []

        for word in words:
            parts = word.replace("\"", "").split("*")
            weight = float(parts[0])
            term = parts[1]

            item = {"term": term, "weight": weight}
            topic_terms.append(item)

        topic_dict["terms"] = topic_terms
        topics.append(topic_dict)

    if visualize_topics:
        # pyLDAvis.enable_notebook()
        vis = pyLDAvis.gensim.prepare(lda_model, bow_corpus, dictionary)
        pyLDAvis.save_html(vis, 'lda2.html')

    return topics


if __name__ == '__main__':
    print("Starting LDA analysis")
    lda_topics()


