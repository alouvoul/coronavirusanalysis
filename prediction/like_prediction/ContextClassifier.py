import os
import pickle

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import r2_score, mean_absolute_error, max_error
from sklearn.model_selection import train_test_split

from FeatureExtraction.LikePredictionFeatures import LikePredictionFeatures
from data.mongo import mongo
from prediction.like_prediction.utils import get_models
from process.tokenizing import preprocess_like
from utils.path_utils import resource_path

PLATFORM_INSTAGRAM = 0
PLATFORM_TWITTER = 1


class ContextClassifier:
    twitter_cv_path = resource_path("models/likes_context_count_vec_twitter.sav")
    insta_cv_path = resource_path("models/likes_context_count_vec_instagram.sav")

    twitter_model_path = resource_path("models/likes_context_ml_model_twitter.sav")
    insta_model_path = resource_path("models/likes_context_ml_model_instagram.sav")

    count_vec = None
    model = None

    def __init__(self, platform):
        """
        Constructor of the class. If there a model already exists just imports it using pickle. Otherwise get some ML
        models to evaluate and saves the one with the best performance. An OverSampler used to get the dataset balanced.
        After the training, the best model saved in the resource path.
        :param platform: 0 for Instagram, 1 for twitter
        """
        vectorizer_filename = self.insta_cv_path if platform == PLATFORM_INSTAGRAM else self.twitter_cv_path
        model_filename = self.insta_model_path if platform == PLATFORM_INSTAGRAM else self.twitter_model_path

        if os.path.exists(model_filename):
            try:
                self.count_vec = pickle.load(open(vectorizer_filename, "rb"))
                self.model = pickle.load(open(model_filename, "rb"))
                #print("Loaded persisted like prediction by context model.")
                return
            except Exception:
                print("Failed to load model. Resuming model creation...")

        data = None

        if platform == PLATFORM_TWITTER:
            print('Twitter like prediction model training...')

            df = []
            counter = 100
            z = 0  # Number of Tweets to process
            for doc in mongo().twitter.get_all_tweets():
                if z > 5000:
                    break
                z += 1
                if int(doc['fave_count']) > 60:
                    continue
                if int(doc['fave_count']) == 0:
                    counter -= 1
                    if not counter > 0:
                        continue
                temp = [
                    preprocess_like(doc['text']),
                    doc['fave_count']
                ]
                df.append(temp)
            data = pd.DataFrame(df, columns=['text', 'target'])
        elif platform == PLATFORM_INSTAGRAM:
            print('Instagram like prediction model training...')

            df = []
            for doc in mongo().instagram.get_all_posts():
                temp = [
                    preprocess_like(doc['caption']),
                    len(doc['likes'])
                ]
                df.append(temp)
            data = pd.DataFrame(df, columns=['text', 'target'])

        x_train, x_test, y_train, y_test = train_test_split(data.iloc[:, :-1], data.iloc[:, -1:], train_size=0.7)
        from imblearn.over_sampling import RandomOverSampler
        sm = RandomOverSampler(random_state=2)  # sampling_strategy=1
        x_train, y_train = sm.fit_resample(x_train, y_train)
        print('After Undersampling, the shape of train_X: {}'.format(x_train.shape))
        print('After Undersampling, the shape of train_y: {} \n'.format(y_train.shape))

        self.count_vec = CountVectorizer(lowercase=True, tokenizer=dummy_func, preprocessor=dummy_func,
                                         ngram_range=(1, 2), min_df=20)  # min_df=20, max_df=100)

        X = self.count_vec.fit_transform(x_train['text']).toarray()

        print("Persisting vectorizer...")
        pickle.dump(self.count_vec, open(vectorizer_filename, "wb"))

        X = pd.DataFrame(X)

        X1 = LikePredictionFeatures().transform(x_train['text'])

        frames = [X, X1]
        features = pd.concat(frames, axis=1)

        X_test = self.count_vec.transform(x_test['text']).toarray()
        X_test = pd.DataFrame(X_test)
        X1_test = LikePredictionFeatures().transform(x_test['text'])

        frames = [X_test, X1_test]
        test_features = pd.concat(frames, axis=1)

        best_mae = 200000
        best_model = None

        # ML models for training
        models = get_models()
        for name, model in models:
            model.fit(features, y_train)
            prediction = model.predict(test_features)

            mae = mean_absolute_error(y_test, y_pred=prediction)
            r2 = r2_score(y_test, y_pred=prediction)
            me = max_error(y_test, y_pred=prediction)
            print("%s Mean Absolute Error: %f" % (name, mae))
            print("%s R2 score: %f" % (name, r2))
            print("%s Max error: %f" % (name, me))

            if mae < best_mae:
                best_model = model
                best_mae = mae

        self.model = best_model
        pickle.dump(self.model, open(model_filename, "wb"))

    def like_predict(self, doc):
        return self.model.predict(doc)

    def like_predict_from_text(self, text):
        """
        Predict a from a text doc
        :param text: A string sentence
        :return: The prediction based on the model
        """
        text_x = self.count_vec.transform([preprocess_like(text)]).toarray()
        text_x = pd.DataFrame(text_x)
        features_x = LikePredictionFeatures().transform([text])

        frames = [text_x, features_x]
        features = pd.concat(frames, axis=1)

        prediction = self.model.predict(features)[0]

        if prediction is None:
            return 0

        if isinstance(prediction, float):

            if prediction < 0:
                return 0

            return prediction

        try:
            likes = prediction[0]

            if likes < 0:
                return 0

            return likes
        except Exception as err:
            nothing = 0

        return prediction


def dummy_func(doc):
    return doc
