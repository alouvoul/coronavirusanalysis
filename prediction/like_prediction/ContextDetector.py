from prediction.like_prediction.ContextClassifier import ContextClassifier

PLATFORM_INSTAGRAM = 0
PLATFORM_TWITTER = 1


class ContextDetector:

    def __init__(self):
        """
        Constructor that initialize the model for Instagram and Twitter that need in order to make predictions.
        """
        self.twitter_model = ContextClassifier(PLATFORM_TWITTER)
        self.instagram_model = ContextClassifier(PLATFORM_INSTAGRAM)

    def predict_twitter(self, tweet):
        return self.twitter_model.like_predict_from_text(tweet)

    def predict_instagram(self, post):
        return self.instagram_model.like_predict_from_text(post)
