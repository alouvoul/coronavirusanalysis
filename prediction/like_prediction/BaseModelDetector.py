import datetime
import pandas as pd
from prediction.like_prediction.BaseModelClassifier import BaseModelClassifier
from data.mongo import mongo

from prediction.like_prediction.utils import discretization, time_zone


class BaseModelDetector:

    def __init__(self):
        """
        Initialize the models for the predictions.
        """
        self.model_twitter = BaseModelClassifier(self.twitter_features(), 1)
        self.model_instagram = BaseModelClassifier(self.instagram_features(), 0)

    def predict_tweet(self, tweet):
        """
        Predict the number of likes that a tweet will receive.
        :param tweet: A doc of string
        :return: The prediction of the model about the number of likes
        """
        return self.model_twitter.model.predict(tweet)

    def predict_post(self, post):
        """
        Predict the number of likes that a post will receive.
        :param tweet: A doc of string
        :return: The prediction of the model about the number of likes
        """
        return self.model_instagram.model.predict(post)

    def twitter_features(self):
        """
        Method that extracts some baseline tweets. The tweets that used is a subset of the total number. Likes that
        doesn't have likes are dismissed except a total number of 60. Tweets with more than one hundred likes considered
        as outlier and excluded from the dataset.

        :return: DataFrame with features extracted
        """
        twitter_data = mongo().twitter.get_all_tweets()
        features = ['retweet_count', 'tagged_users', 'number_of_hashtags', 'time_zone', 'years_active',
                    'follower_count', 'follows_count', 'tweet_count', 'favorites_count', 'is_verified',
                    'post_frequency', 'average_likes', 'target']
        data = []
        z = 0
        counter = 60
        print("|    Twitter Features")
        for tweet in twitter_data:
            user = mongo().twitter.get_user_of_tweet(tweet=tweet)
            if z > 10000:
                break
            z += 1
            if not user:
                continue
            if int(tweet['fave_count']) > 100:
                continue
            if int(tweet['fave_count']) == 0:
                counter -= 1
                if not counter > 0:
                    continue
            ratio = (datetime.datetime.today().year - user['created_at'].year)  # Prevent division by zero
            temp = [
                tweet['retweet_count'],
                len(tweet["tagged_users"]),
                len(tweet['hashtags']),
                time_zone(tweet['created_at']),
                datetime.datetime.today().year - user['created_at'].year,
                user['follower_count'],
                user['follows_count'],
                user['tweet_count'],
                user['favourites_count'],
                user['is_verified'],
                user['tweet_count'] / 1 if not ratio else ratio,  # Post frequency
                user['favourites_count'] / user['tweet_count'],  # Average likes per user
                tweet['fave_count']
            ]
            data.append(temp)
        print("|    End Twitter features")
        data = pd.DataFrame(data, columns=features)
        return data

    def instagram_features(self):
        """
        Method that extracts some baseline instagram posts. All the dataset retrieved from the database and after the
        processing returned as a DtaFrame

        :return: DataFrame with features extracted
        """
        instagram_data = mongo().instagram.get_all_posts()
        features = ['time_zone', 'caption_hashtags', 'caption_mentions', 'tagged_users',
                    'follower_count', 'follows_count', 'mediacount', 'is_private', 'is_verified',
                    'likes']
        data = []
        index = 0
        for post in instagram_data:
            user = mongo().instagram.get_user_of_post(post=post)
            if not user:
                continue

            temp = [
                time_zone(post['created_at']),
                len(post['caption_hashtags']),
                len(post["caption_mentions"]),
                len(post['tagged_users']),
                user['follower_count'],
                user['follows_count'],
                user['mediacount'],
                user['is_private'],
                user['is_verified'],
                len(post['likes'])
            ]
            data.append(temp)
            index += 1
        data = pd.DataFrame(data, columns=features)
        return data