from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor


def discretization(target):
    """
    Used to convert the problem in classification. Intervals were set based on the distribution of likes in posts.
    :param target:
    :return:
    """
    if target == 0:
        discretized = 0
    elif target < 5:
        discretized = 1
    elif target < 10:
        discretized = 2
    elif target < 50:
        discretized = 3
    else:
        discretized = 4
    return discretized


def time_zone(time):
    """
    Process the time of a post and mark at 4 different numbers. 0 corresponds to morning, 1 to afternoon, 2 to
    evening and 3 to after midnight. The idea is that the time that a post published can be change the impact. For
    example a post after midnight probably some of the followers will miss it.
    :param time:
    :return:
    """
    hour = time.hour
    if hour < 8:
        return 3
    elif hour < 12:
        return 0
    elif hour < 17:
        return 1
    else:
        return 2


def get_models():
    """
    Used to add model in evaluate and selection of the best one based on their performance.
    :return: Array of ML model regressors
    """
    models = [
        ('DT', DecisionTreeRegressor()),
        ('RF', RandomForestRegressor()),
        ('GDB', GradientBoostingRegressor()),
        ('SVM', SVR()),
        # ('NN', MLPRegressor()),
        ('LR', LinearRegression())
    ]
    return models
