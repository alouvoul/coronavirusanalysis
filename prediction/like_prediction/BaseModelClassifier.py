import os
import pickle
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, mean_absolute_error, max_error
from utils.path_utils import resource_path
from prediction.like_prediction.utils import get_models
from imblearn.over_sampling import SMOTE, RandomOverSampler


class BaseModelClassifier:
    twitter_model_filename = resource_path("models/likes_base_ml_model_twitter.sav")
    instagram_model_filename = resource_path("models/likes_base_ml_model_instagram.sav")

    def __init__(self, data, platform):
        """
        Constructor of the class. If there a model already exists just imports it using pickle. Otherwise get some ML
        models to evaluate and saves the one with the best performance. An OverSampler used to get the dataset balanced.
        After the training, the best model saved in the resource path.

        :param data: Data to train and test the model. DataFrame in which the last column contains the target
        :param platform: Number 0 to train an Instagram model or 1 to train a Twitter model
        """
        if platform == 1:
            self.model_filename = self.twitter_model_filename
        else:
            self.model_filename = self.instagram_model_filename

        if os.path.exists(self.model_filename):
            try:
                self.model = pickle.load(open(self.model_filename, "rb"))
                print("Loaded persisted like prediction by baseline model.")
                return
            except Exception:
                print("Failed to load model. Resuming model creation...")
        # ML models for training
        models = get_models()

        x_train, x_test, y_train, y_test = train_test_split(data.iloc[:, :-1], data.iloc[:, -1:], train_size=0.7)

        # Imbalance
        sm = RandomOverSampler(random_state=2)
        x_train, y_train = sm.fit_resample(x_train, y_train)
        print('After Undersampling, the shape of train_X: {}'.format(x_train.shape))
        print('After Undersampling, the shape of train_y: {} \n'.format(y_train.shape))

        best_mae = 200000
        best_model = None
        for name, model in models:

            model.fit(x_train, y_train)
            prediction = model.predict(x_test)

            mae = mean_absolute_error(y_test, y_pred=prediction)
            r2 = r2_score(y_test, y_pred=prediction)
            me = max_error(y_test, y_pred=prediction)
            print("%s Mean Absolute Error: %f" % (name, mae))
            print("%s R2 score: %f" % (name, r2))
            print("%s Max error: %f" % (name, me))
            print(prediction)
            if mae < best_mae:
                best_model = model
                best_mae = mae

        self.model = best_model
        pickle.dump(self.model, open(self.model_filename, "wb"))

