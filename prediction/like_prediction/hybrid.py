
from prediction.like_prediction.ContextDetector import ContextDetector
from prediction.like_prediction.BaseModelDetector import BaseModelDetector


class LikePredictionHybridModel:

    def __init__(self):
        self.context_model = ContextDetector()
        self.base_model = BaseModelDetector()

    def predict(self):
        pass