import random


class Emotion:
    """
    A model class holding the relevant emotion values
    """
    anger = 0.0
    disgust = 0.0
    fear = 0.0
    joy = 0.0
    sadness = 0.0
    surprise = 0.0

    def __init__(self):
        # [anger, disgust, fear, joy, sadness, surprise]
        self.anger = 0.0
        self.disgust = 0.0
        self.fear = 0.0
        self.joy = 0.0
        self.sadness = 0.0
        self.surprise = 0.0

    def set(self, anger=0.0, disgust=0.0, fear=0.0, joy=0.0, sadness=0.0, surprise=0.0):
        self.anger = anger
        self.disgust = disgust
        self.fear = fear
        self.joy = joy
        self.sadness = sadness
        self.surprise = surprise

    def set_all(self, values: list):
        self.anger = values[0]
        self.disgust = values[1]
        self.fear = values[2]
        self.joy = values[3]
        self.sadness = values[4]
        self.surprise = values[5]

    def __dict__(self):
        dict_self = dict()

        dict_self["anger"] = self.anger
        dict_self["disgust"] = self.disgust
        dict_self["fear"] = self.fear
        dict_self["joy"] = self.joy
        dict_self["sadness"] = self.sadness
        dict_self["surprise"] = self.surprise

        return dict_self

    def to_dict(self):
        return self.__dict__()

    def from_dict(self, dict_self):
        self.anger = dict_self["anger"]
        self.disgust = dict_self["disgust"]
        self.fear = dict_self["fear"]
        self.joy = dict_self["joy"]
        self.sadness = dict_self["sadness"]
        self.surprise = dict_self["surprise"]

    def __repr__(self) -> str:
        return self.to_dict().__repr__()

    def values_by_averaging(self, emotions: list):
        sum_anger = 0.0
        sum_disgust = 0.0
        sum_fear = 0.0
        sum_joy = 0.0
        sum_sadness = 0.0
        sum_surprise = 0.0

        for emotion in emotions:
            sum_anger += emotion.anger
            sum_disgust += emotion.disgust
            sum_fear += emotion.fear
            sum_joy += emotion.joy
            sum_sadness += emotion.sadness
            sum_surprise += emotion.surprise

        if sum_anger > 0.0:
            self.anger = sum_anger / len(emotions)

        if sum_disgust > 0.0:
            self.disgust = sum_disgust / len(emotions)

        if sum_fear > 0.0:
            self.fear = sum_fear / len(emotions)

        if sum_joy > 0.0:
            self.joy = sum_joy / len(emotions)

        if sum_sadness > 0.0:
            self.sadness = sum_sadness / len(emotions)

        if sum_surprise > 0.0:
            self.surprise = sum_surprise / len(emotions)

    def min_max_normalize(self):
        """
        Convert values to binary 0 & 1 values
        :return: Nothing
        """
        if self.anger >= 0.5:
            self.anger = 1
        else:
            self.anger = 0

        if self.disgust >= 0.5:
            self.disgust = 1
        else:
            self.disgust = 0

        if self.fear >= 0.5:
            self.fear = 1
        else:
            self.fear = 0

        if self.joy >= 0.5:
            self.joy = 1
        else:
            self.joy = 0

        if self.sadness >= 0.5:
            self.sadness = 1
        else:
            self.sadness = 0

        if self.surprise >= 0.5:
            self.surprise = 1
        else:
            self.surprise = 0

    def inverse(self):
        """
        Convert emotion values to their inverse, used for negation.
        :return:
        """
        emotion = Emotion()
        # [anger, disgust, fear, joy, sadness, surprise]
        emotion.anger = self.fear
        emotion.disgust = 1.0 - self.disgust
        emotion.fear = self.anger
        emotion.joy = self.sadness
        emotion.sadness = self.joy
        emotion.surprise = 1.0 - self.surprise

        return emotion
