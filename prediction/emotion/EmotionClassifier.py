import os
import pickle

import pandas as pd
from sklearn import metrics
from sklearn.ensemble import VotingClassifier, RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import SVC

from prediction.emotion.Emotion import Emotion
from prediction.emotion.EmotionTextFeatureExtractor import EmotionTextFeatureExtractor
from utils.path_utils import resource_path


class EmotionClassifier:
    """
    This class manages the loading or creation of the supervised emotion ML model
    """
    feature_extractor = None

    vectorizer_filename = resource_path("models/emotion_count_vec.sav")
    vectorizer = None

    model_filename = resource_path("models/emotion_ml_model.sav")
    model = None

    def __init__(self):
        self.feature_extractor = EmotionTextFeatureExtractor()

        if os.path.exists(self.vectorizer_filename) and os.path.exists(self.model_filename):
            try:
                self.vectorizer = pickle.load(open(self.vectorizer_filename, "rb"))
                self.model = pickle.load(open(self.model_filename, "rb"))

                # print("Loaded persisted emotion model.")

                return
            except Exception:
                print("Failed to load model. Resuming model creation...")

        if os.path.exists(self.vectorizer_filename):
            os.remove(self.vectorizer_filename)

        if os.path.exists(self.model_filename):
            os.remove(self.model_filename)

        filepath = resource_path("/2018-E-c-En-train.txt")
        raw_data = pd.read_csv(filepath, delimiter="\t")
        data = self.feature_extractor.extract_transform_dataset(raw_data)
        # anger	anticipation	disgust	fear	joy	love	optimism	pessimism	sadness	surprise	trust
        data.drop([3, 7, 8, 9, 12], axis=1, inplace=True)

        X = data.iloc[:, 1]
        y = data.iloc[:, 2:]

        print("Vectorizing dataset text...")

        # create count vectorizer
        cv_stop_words = "english"
        cv_max_df = 0.85
        cv_lowercase_transform = True

        if self.vectorizer is None:
            self.vectorizer = CountVectorizer(strip_accents='unicode',
                                              analyzer="word",
                                              token_pattern=r"[\w\-:]+",
                                              stop_words=cv_stop_words,
                                              max_df=cv_max_df,
                                              ngram_range=(1, 2),
                                              lowercase=cv_lowercase_transform)

            X = self.vectorizer.fit_transform(X)
        else:
            X = self.vectorizer.transform(X)

        print("Persisting vectorizer...")

        pickle.dump(self.vectorizer, open(self.vectorizer_filename, "wb"))

        # split dataset to train and test
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=2)

        print("Training emotion analysis models...")

        svm = SVC(kernel="linear", probability=True)
        rf = RandomForestClassifier()

        # create estimators list
        estimators = [("svm", svm), ("rf", rf)]

        # create voting classifier
        # OneVsRestClassifier(VotingClassifier(estimators, voting="soft"), n_jobs=-1)
        self.model = OneVsRestClassifier(VotingClassifier(estimators, voting="soft"), n_jobs=-1)#OneVsRestClassifier(svm, n_jobs=-1)
        self.model.fit(X_train, y_train)

        print("Prediction test metrics:")

        y_pred = self.model.predict(X_test)

        accuracy = metrics.accuracy_score(y_test, y_pred)
        recall = metrics.recall_score(y_test, y_pred, average="macro")
        precision = metrics.precision_score(y_test, y_pred, average="macro")
        f1 = metrics.f1_score(y_test, y_pred, average="macro")

        # Best results so far

        print("\tAccuracy: %f" % accuracy)
        print("\tRecall: %f" % recall)
        print("\tPrecision: %f" % precision)
        print("\tF1: %f" % f1)

        print("Persisting Models...")
        pickle.dump(self.model, open(self.model_filename, "wb"))

    def emotions_of(self, words: list):
        """
        Uses the ML model to predict the emotions of the tokens and converts them to an Emotion object
        :param words: a list of preprocessed tokens
        :return: Emotion instance
        """
        x = self.vectorizer.transform([words])

        prediction = self.model.predict(x)[0]
        emotion = Emotion()
        emotion.set_all(prediction)

        return emotion
