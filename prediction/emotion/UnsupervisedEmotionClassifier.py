import random
import re

import pandas as pd

from prediction.emotion.Emotion import Emotion
from utils.path_utils import resource_path


class UnsupervisedEmotionClassifier:
    """
    This class detects the emotions of a string of text using a lexicon approach
    """
    lexicon_path = resource_path("NRC-Emotion-Lexicon-Wordlevel-v0.92.txt")

    def __init__(self):
        self.data = pd.read_csv(self.lexicon_path, names=["text", "emotion", "association"], sep='\t')
        self.emotion_lexicon = self.data.pivot(index='text', columns='emotion', values='association')
        self.total_words_in_emotion_lexicon = self.data.iloc[:, 0].tolist()

    def emotions_of(self, word):
        """
        Detect the emotions evoked by the specific word or token
        :param word: the token
        :return: Emotion instance
        """
        is_negation = re.search(r"NOT_", word) is not None

        if is_negation:
            word = word.replace("NOT_", "")

        emotion = Emotion()

        if word in self.total_words_in_emotion_lexicon:
            nrc_emotion = self.emotion_lexicon.loc[word]

            emotion.anger = nrc_emotion.anger
            emotion.disgust = nrc_emotion.disgust
            emotion.fear = nrc_emotion.fear
            emotion.joy = nrc_emotion.joy
            emotion.sadness = nrc_emotion.sadness
            emotion.surprise = nrc_emotion.surprise

        if is_negation:
            emotion = emotion.inverse()

        return emotion

    def emotions_of_words(self, words: list):
        """
        Detect the emotions evoked by the list of tokens
        :param words: a list of preprocessed tokens
        :return: Emotion instance
        """
        if len(words) == 0:
            return Emotion()

        # find emotion for each token
        emotions = [self.emotions_of(word) for word in words if re.search(r"(FEATURE_)|(SEMANTIC_)", word) is None]

        emotion = Emotion()

        # average emotions of all tokens
        emotion.values_by_averaging(emotions)
        emotion.min_max_normalize()

        return emotion
