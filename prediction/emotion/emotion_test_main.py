import pandas as pd

from prediction.emotion.Emotion import Emotion
from prediction.emotion.EmotionDetector import EmotionDetector
from utils.path_utils import resource_path

if __name__ == "__main__":
    """
    This is used as a way to test emotion detection performance.
    """
    emo_detector = EmotionDetector()

    tp = 0
    tn = 0
    fp = 0
    fn = 0

    filepath = resource_path("/2018-E-c-En-train.txt")
    raw_data = pd.read_csv(filepath, delimiter="\t")
    raw_data.drop(["anticipation", "love",	"optimism",	"pessimism", "trust"], axis=1, inplace=True)

    for row in raw_data.values:
        text = row[1]
        emotions = row[2:]

        true_emotions = Emotion()
        true_emotions.set_all(emotions)
        true_emo_vector_str = ""

        for key in true_emotions.to_dict().keys():
            true_emo_vector_str += str(true_emotions.to_dict()[key])

        print("\n\nTesting: '", text, "'.")

        emos = emo_detector.detect_emotions(text)
        emo_vector_str = ""

        for key in emos.to_dict().keys():
            emo_vector_str += str(emos.to_dict()[key])

        if emo_vector_str == true_emo_vector_str:
            tp += 1
            print("Found correct prediction:", emo_vector_str)
        else:
            fp += 1
            print("Found wrong prediction:", emo_vector_str, "Expected:", true_emo_vector_str)

        print()

    print("Correct predictions:", tp, "Incorrect:", fp)