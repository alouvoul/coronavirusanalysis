import re

import nltk
import pandas as pd

from prediction.emotion.EmojiEmotionExtractor import EmojiEmotionExtractor
from process.tokenizing import preprocess_text


class EmotionTextFeatureExtractor:
    """
    This class converts text or tokens to string containing the extracted features
    """
    imported_ntlk = None
    emoji_emotion_extractor = None

    def __init__(self):
        self.__init_nltk()
        self.emoji_emotion_extractor = EmojiEmotionExtractor()

    def __init_nltk(self):
        # Initialize nltk packages. If exist the file then
        try:
            file = open("../nltk_packages.txt")
        except IOError:
            nltk.download("punkt")
            nltk.download('wordnet')
            nltk.download('stopwords')
            nltk.download('averaged_perceptron_tagger')
            nltk.download("names")
            file = open("../nltk_packages.txt", "w")
            file.close()

        self.imported_ntlk = True

    def extract_transform_dataset(self, data: pd.DataFrame):
        """
        Transform a dataframe's tweets to the feature extracted strings
        :param data: dataset DataFrame
        :return: transoformed dataframe
        """
        dataset = []

        for row in data.values:
            rid = row[0]
            tweet = self.extract_from_string(row[1])

            transformed_row = [rid, tweet]

            for label_idx in range(2, len(row)):
                label = row[label_idx]
                transformed_row.append(label)

            dataset.append(transformed_row)

        return pd.DataFrame(dataset)

    def extract_from_string(self, text: str):
        """
        Convert a string of text to a cleaned string containing the extracted features
        :param text: the string of text
        :return: string - cleaned string with extracted features
        """

        # detect hashtags and mentions before preprocessing
        hashtag_matches = re.findall(r"#\w+", text)

        if hashtag_matches is None:
            hashtag_matches = []

        tag_matches = re.findall(r"@\w+", text)

        if hashtag_matches is None:
            tag_matches = []

        tokens = preprocess_text(text, discard_limited_tokens=False)
        return self.extract_from_tokens(tokens, len(hashtag_matches), len(tag_matches))

    def extract_from_tokens(self, tokens: list, hashtag_count: int = 0, tag_count: int = 0):
        """
        Convert a list of preprocessed tokens to a cleaned string containing the extracted features
        :param hashtag_count: the number of hastags
        :param tag_count: the number of mentions
        :param tokens: a list of preprocessed tokens
        :return: string - cleaned string with extracted features
        """
        if tokens is None:
            return None

        emoji_count = 0
        emoji_emotion_values = []

        period_counts = 0
        multi_period_count = 0

        exclamation_counts = 0
        multi_exclamation_count = 0

        question_counts = 0
        multi_question_count = 0

        # init negation relevant vars
        negation_active = False
        pos_tags = nltk.pos_tag(tokens)
        negatable = [word_tag_pair[0] for word_tag_pair in pos_tags if word_tag_pair[1] in ["NN", "JJ"]]
        negatable = set([word.replace(".", "").replace("?", "").replace("!", "").strip() for word in negatable])

        output_tokens = []

        for token in tokens:
            # find emoji related features
            if self.emoji_emotion_extractor.is_emoji_name_like(token):
                emoji_count += 1

                if self.emoji_emotion_extractor.exists_emoji_name(token):
                    emotions = self.emoji_emotion_extractor.emotions_of_emoji_named(token)
                    emoji_emotion_values.append(emotions)

            is_sentence_end = re.search(r"(\.+)|(!+)|(\?+)", token) is not None

            # detect punctuation
            if re.search(r"\.", token) is not None:
                period_counts += 1

            if re.search(r"\.{2,}", token) is not None:
                multi_period_count += 1

            if re.search(r"!", token) is not None:
                exclamation_counts += 1

            if re.search(r"!{2,}", token) is not None:
                multi_exclamation_count += 1

            if re.search(r"\?", token) is not None:
                question_counts += 1

            if re.search(r"\?{2,}", token) is not None:
                multi_question_count += 1

            # remove punctuation
            token = token.replace(".", "").replace("?", "").replace("!", "")

            # handle negation
            if token is None or len(token) == 0:

                if is_sentence_end:
                    negation_active = False

                continue

            use_negated_word = False

            if token == "SEMANTIC_NEGATION":
                negation_active = not negation_active

            if negation_active and token in negatable and token != "SEMANTIC_NEGATION":
                use_negated_word = True

            if is_sentence_end:
                negation_active = False

            if token == "SEMANTIC_NEGATION":
                continue

            if not use_negated_word:
                output_tokens.append(token)
            else:
                output_tokens.append("NOT_" + token)

        # add extracted features as synthetic tokens
        emoji_word_ratio = 0

        if emoji_count > 0:
            output_tokens.append("FEATURE_HAS_EMOJI")
            emoji_word_ratio = round(emoji_count / len(tokens), 2)

        if 0 < emoji_word_ratio <= 0.33:
            output_tokens.append("FEATURE_EMOJI_RATIO_LOW")
        elif 0.33 < emoji_word_ratio <= 0.66:
            output_tokens.append("FEATURE_EMOJI_RATIO_MEDIUM")
        elif 0.66 < emoji_word_ratio <= 1.0:
            output_tokens.append("FEATURE_EMOJI_RATIO_HIGH")

        emoji_emotions = [0, 0, 0, 0]

        if len(emoji_emotion_values) > 0:
            angerTotal = 0
            fearTotal = 0
            joyTotal = 0
            sadnessTotal = 0

            for emotions in emoji_emotion_values:
                angerTotal += emotions[0]
                fearTotal += emotions[1]
                joyTotal += emotions[2]
                sadnessTotal += emotions[3]

            anger = angerTotal / len(emoji_emotion_values)
            fear = fearTotal / len(emoji_emotion_values)
            joy = joyTotal / len(emoji_emotion_values)
            sadness = sadnessTotal / len(emoji_emotion_values)

            emoji_emotions = [anger, fear, joy, sadness]

        anger = emoji_emotions[0]
        fear = emoji_emotions[1]
        joy = emoji_emotions[2]
        sadness = emoji_emotions[3]

        if 0 < anger <= 0.33:
            output_tokens.append("FEATURE_EMOJI_ANGER_LOW")
        elif 0.33 < anger <= 0.66:
            output_tokens.append("FEATURE_EMOJI_ANGER_MEDIUM")
        elif 0.66 < anger <= 1.0:
            output_tokens.append("FEATURE_EMOJI_ANGER_HIGH")

        if 0 < fear <= 0.33:
            output_tokens.append("FEATURE_EMOJI_FEAR_LOW")
        elif 0.33 < fear <= 0.66:
            output_tokens.append("FEATURE_EMOJI_FEAR_MEDIUM")
        elif 0.66 < fear <= 1.0:
            output_tokens.append("FEATURE_EMOJI_FEAR_HIGH")

        if 0 < joy <= 0.33:
            output_tokens.append("FEATURE_EMOJI_JOY_LOW")
        elif 0.33 < joy <= 0.66:
            output_tokens.append("FEATURE_EMOJI_JOY_MEDIUM")
        elif 0.66 < joy <= 1.0:
            output_tokens.append("FEATURE_EMOJI_JOY_HIGH")

        if 0 < sadness <= 0.33:
            output_tokens.append("FEATURE_EMOJI_SADNESS_LOW")
        elif 0.33 < sadness <= 0.66:
            output_tokens.append("FEATURE_EMOJI_SADNESS_MEDIUM")
        elif 0.66 < sadness <= 1.0:
            output_tokens.append("FEATURE_EMOJI_SADNESS_HIGH")

        hashtag_ratio = 0
        tag_ratio = 0

        if len(tokens) > 0:
            hashtag_ratio = hashtag_count / len(tokens)
            tag_ratio = tag_count / len(tokens)

        if hashtag_count > 0:
            output_tokens.append("FEATURE_HAS_HASHTAGS")

        if tag_count > 0:
            output_tokens.append("FEATURE_HAS_TAGS")

        if 0.0 < hashtag_ratio <= 0.33:
            output_tokens.append("FEATURE_HASHTAG_RATIO_LOW")
        elif 0.33 < hashtag_ratio <= 0.66:
            output_tokens.append("FEATURE_HASHTAG_RATIO_MED")
        elif 0.66 < hashtag_ratio <= 1.0:
            output_tokens.append("FEATURE_HASHTAG_RATIO_HIGH")

        if 0.0 < tag_ratio <= 0.33:
            output_tokens.append("FEATURE_TAG_RATIO_LOW")
        elif 0.33 < tag_ratio <= 0.66:
            output_tokens.append("FEATURE_TAG_RATIO_MED")
        elif 0.66 < tag_ratio <= 1.0:
            output_tokens.append("FEATURE_TAG_RATIO_HIGH")

        if period_counts > 0:
            output_tokens.append("FEATURE_HAS_PERIOD")

        if question_counts > 0:
            output_tokens.append("FEATURE_HAS_QUESTION_MARK")

        if exclamation_counts > 0:
            output_tokens.append("FEATURE_HAS_EXCLAMATION")

        if multi_period_count > 0:
            output_tokens.append("FEATURE_HAS_MULTI_PERIOD")

        if multi_question_count > 0:
            output_tokens.append("FEATURE_HAS_MULTI_QUESTION_MARK")

        if multi_exclamation_count > 0:
            output_tokens.append("FEATURE_HAS_MULTI_EXCLAMATION")

        transformed_str = " ".join(output_tokens)
        return transformed_str
