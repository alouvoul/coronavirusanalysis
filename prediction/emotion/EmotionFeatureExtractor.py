import re

import nltk
import pandas as pd

from process.embeddings import EmbeddingModel
from process.tokenizing import preprocess_text
from prediction.emotion.EmojiEmotionExtractor import EmojiEmotionExtractor


class EmotionFeatureExtractor:
    """
    This class converts text or tokens to glove_vector and feature array
    """
    imported_ntlk = None
    embedding_model = None

    def __init__(self):
        self.__init_nltk()
        self.embedding_model = EmbeddingModel()

    def __init_nltk(self):
        # Initialize nltk packages. If exist the file then
        try:
            file = open("../nltk_packages.txt")
        except IOError:
            nltk.download("punkt")
            nltk.download('wordnet')
            nltk.download('stopwords')
            nltk.download('averaged_perceptron_tagger')
            nltk.download("names")
            file = open("../nltk_packages.txt", "w")
            file.close()

        self.imported_ntlk = True

    def extract_transform_dataset(self, data: pd.DataFrame):
        """

        :param data: the dataframe for the relevant dataset
        :return: list of rows containing the transformed dataset
        """
        columns = ["ID",
                   "vectors",
                   "features",
                   "anger",
                   "anticipation",
                   "disgust",
                   "fear",
                   "joy",
                   "love",
                   "optimism",
                   "pessimism",
                   "sadness",
                   "surprise",
                   "trust"]

        dataset = []

        for row in data.values:
            new_row = [row[0]]

            vectors, features = self.extract_from_string(row[1])

            new_row.append(vectors)
            new_row.append(features)

            new_row.append(row[2])
            new_row.append(row[3])
            new_row.append(row[4])
            new_row.append(row[5])
            new_row.append(row[6])
            new_row.append(row[7])
            new_row.append(row[8])
            new_row.append(row[9])
            new_row.append(row[10])
            new_row.append(row[11])
            new_row.append(row[12])

            dataset.append(new_row)

        return dataset# pd.DataFrame(dataset, columns)

    def extract_from_string(self, text: str):
        """
        Extract features from the given string of text
        :param text: the string to extract featues from
        :return: (vectors, features) the text converted to a list of vectors and a list of the extracted features
        """
        tokens = preprocess_text(text, discard_limited_tokens=False)
        return self.extract_from_tokens(tokens)

    def extract_from_tokens(self, tokens: list):
        """
        Convert a list of preprocessed tokens to glove vectors and features
        :param tokens: a list of tokens
        :return: (vectors, features) the text converted to a list of vectors and a list of the extracted features
        """

        if tokens is None:
            return None

        # load embedding model and emoji emotion extractor
        emoji_emo = EmojiEmotionExtractor()
        embedding_model = self.embedding_model

        vectors = []
        word_count = len(tokens)

        emoji_count = 0
        emoji_emotion_values = []

        period_counts = 0
        multi_period_count = 0

        exclamation_counts = 0
        multi_exclamation_count = 0

        question_counts = 0
        multi_question_count = 0

        # init negation recognition variables
        negation_active = False
        pos_tags = nltk.pos_tag(tokens)
        negatable = [word_tag_pair[0] for word_tag_pair in pos_tags if word_tag_pair[1] in ["NN", "JJ"]]
        negatable = set([word.replace(".", "").replace("?", "").replace("!", "").strip() for word in negatable])

        for token in tokens:
            # find emoji related features
            if emoji_emo.is_emoji_name_like(token):
                emoji_count += 1

                if emoji_emo.exists_emoji_name(token):
                    emotions = emoji_emo.emotions_of_emoji_named(token)
                    emoji_emotion_values.append(emotions)

                continue

            is_sentence_end = re.search(r"(\.+)|(!+)|(\?+)", token) is not None

            # detect punctuation

            if re.search(r"\.", token) is not None:
                period_counts += 1

            if re.search(r"\.{2,}", token) is not None:
                multi_period_count += 1

            if re.search(r"!", token) is not None:
                exclamation_counts += 1

            if re.search(r"!{2,}", token) is not None:
                multi_exclamation_count += 1

            if re.search(r"\?", token) is not None:
                question_counts += 1

            if re.search(r"\?{2,}", token) is not None:
                multi_question_count += 1

            # remove punctuation from token
            token = token.replace(".", "").replace("?", "").replace("!", "")

            # do token negation checks
            if token is None or len(token) == 0:

                if is_sentence_end:
                    negation_active = False

                continue

            use_negated_word = False

            if token == "SEMANTIC_NEGATION":
                negation_active = not negation_active

            if negation_active and token in negatable and token != "SEMANTIC_NEGATION":
                use_negated_word = True

            if is_sentence_end:
                negation_active = False

            if token == "SEMANTIC_NEGATION":
                continue

            if not use_negated_word:
                token_vector = embedding_model.vector_of(token)
            else:
                not_vector = embedding_model.vector_of("not")

                if not_vector is not None:
                    vectors.append(not_vector)

                token_vector = embedding_model.vector_of(token)

            if token_vector is not None:
                vectors.append(token_vector)
            else:
                print("!!!!! Not able to create word vector for '" + token + "'")

        emoji_word_ratio = 0

        if emoji_count > 0:
            emoji_word_ratio = round(emoji_count / len(tokens), 2)

        # average detected emoji emotions
        emoji_emotions = [0, 0, 0, 0]

        if len(emoji_emotion_values) > 0:
            angerTotal = 0
            fearTotal = 0
            joyTotal = 0
            sadnessTotal = 0

            for emotions in emoji_emotion_values:
                angerTotal += emotions[0]
                fearTotal += emotions[1]
                joyTotal += emotions[2]
                sadnessTotal += emotions[3]

            anger = angerTotal / len(emoji_emotion_values)
            fear = fearTotal / len(emoji_emotion_values)
            joy = joyTotal / len(emoji_emotion_values)
            sadness = sadnessTotal / len(emoji_emotion_values)

            emoji_emotions = [anger, fear, joy, sadness]

        features = [word_count,
                    emoji_word_ratio, emoji_emotions,
                    period_counts, multi_period_count,
                    exclamation_counts, multi_exclamation_count,
                    question_counts, multi_question_count]

        return vectors, features
