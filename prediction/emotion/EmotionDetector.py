from prediction.emotion.Emotion import Emotion
from prediction.emotion.EmotionClassifier import EmotionClassifier
from prediction.emotion.UnsupervisedEmotionClassifier import UnsupervisedEmotionClassifier


class EmotionDetector:
    """
    This class handles the loading of relevant ML models prediction of emotions for a text.
    """
    features_extractor = None
    unsupervised_classifier = None
    supervised_classifier = None

    def __init__(self):
        self.supervised_classifier = EmotionClassifier()
        self.unsupervised_classifier = UnsupervisedEmotionClassifier()
        self.features_extractor = self.supervised_classifier.feature_extractor

    def detect_emotions(self, text):
        """
        Detect the emotions of the text using the hybrid approach
        :param text: a string of the text to predict emotions for
        :return: Emotion instance
        """
        words = self.features_extractor.extract_from_string(text)

        supervised_sentiment = self.supervised_classifier.emotions_of(words)
        unsupervised_sentiment = self.unsupervised_classifier.emotions_of(words)

        emotion = Emotion()
        emotion.values_by_averaging([supervised_sentiment, unsupervised_sentiment])
        emotion.min_max_normalize()

        return emotion

    def detect_sentiment_supervised(self, text):
        """
        Detect the emotions of the text using the supervised approach
        :param text: a string of the text to predict emotions for
        :return: Emotion instance
        """
        words = self.features_extractor.transform_string(text)

        supervised_emotions = self.supervised_classifier.emotions_of(words)
        supervised_emotions.min_max_normalize()

        return supervised_emotions

    def detect_sentiment_unsupervised(self, text):
        """
        Detect the emotions of the text using the unsupervised approach
        :param text: a string of the text to predict emotions for
        :return: Emotion instance
        """
        words = self.features_extractor.transform_string(text)

        unsupervised_emotions = self.unsupervised_classifier.emotions_of(words)
        unsupervised_emotions.min_max_normalize()

        return unsupervised_emotions
