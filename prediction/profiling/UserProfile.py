# gender constants
GENDER_MALE = "M"
GENDER_FEMALE = "F"
GENDER_OTHER = "O"

# age constants
AGE_18_24 = "18-24"
AGE_25_34 = "25-34"
AGE_35_49 = "35-49"
AGE_50_XX = "50-XX"

# aggression bucket constants
AGGRESSION_NONE = 0
AGGRESSION_LOW = 1
AGGRESSION_MED = 2
AGGRESSION_HIGH = 3


class UserProfile:
    """
    This class is a model that holds a users profile
    """
    age = AGE_18_24
    gender = GENDER_OTHER
    sarcastic = 0.0
    aggressive = AGGRESSION_NONE
    interests = []

    def __init__(self, age: str = AGE_18_24, gender: str = GENDER_OTHER, sarcastic: float = 0.0,
                 aggressive: int = AGGRESSION_NONE, interests=None):

        if interests is None:
            interests = []

        self.age = age
        self.gender = gender
        self.sarcastic = sarcastic
        self.aggressive = aggressive
        self.interests = interests

    def __dict__(self):
        dict_self = dict()

        dict_self["age"] = self.age
        dict_self["gender"] = self.gender
        dict_self["sarcastic"] = self.sarcastic
        dict_self["aggressive"] = self.aggressive
        dict_self["interests"] = self.interests

        return dict_self

    def to_dict(self):
        return self.__dict__()

    def from_dict(self, dict_self):
        self.age = dict_self["age"]
        self.gender = dict_self["gender"]
        self.sarcastic = dict_self["sarcastic"]
        self.aggressive = dict_self["aggressive"]
        self.interests = dict_self["interests"]

        return self

    def __repr__(self) -> str:
        return self.to_dict().__repr__()
