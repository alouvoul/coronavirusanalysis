
from prediction.profiling.sarcasm.SarcasmClassifier import SarcasmClassifier


class SarcasmDetector:

    def __init__(self):
        self.classifier = SarcasmClassifier()

    def detect_sarcasm(self, docs):
        sarcastic = self.classifier.predict_sarcasm_str(doc=docs)
        return sarcastic

    def detect_sarcasm_tokens(self, tokens):
        sarcastic = self.classifier.predict_sarcasm(tokens)
        return sarcastic

    def detect_sarcastic_user(self, docs: list):
        """
        Method to detect sarcasm in a total docs of a user. Each doc with sarcasm add +1 to the variable and each
        non-sarcastic -1. If a user count more than 0 is a sarcastic user else is a non sarcastic
        :param docs: Array of strings which represent the posts of the user
        :return: 0 if is non-sarcastic, 1 if is sarcastic
        """
        total = 0
        for doc in docs:
            prediction = self.classifier.predict_sarcasm(doc)
            if not prediction:
                total -= 1
            else:
                total += 1
        return 1 if total > 0 else 0


def run():
    print('start')
    sd = SarcasmDetector()

    prediction = sd.detect_sarcasm("she more more told us XD.....")
    print(prediction)
    prediction = sd.detect_sarcasm("Coronavirus is a serious disease")
    print(prediction)


if __name__ == "__main__":
    run()
