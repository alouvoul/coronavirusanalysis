import os
import pickle

import pandas as pd
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import accuracy_score, f1_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline, FeatureUnion

from FeatureExtraction.SarcasmFeatures import SarcasmFeatures
from process.tokenizing import tokenize_text
from utils.path_utils import resource_path


class SarcasmClassifier:
    sarcasm_file = resource_path('/sarcastic_tweets.csv')
    model_filename = resource_path("models/usr_sarcasm_ml_model.sav")
    model = None

    def __init__(self):
        """
        Class that trains a model to make prediction about sarcasm. If there is a model already trained load if from the
        corresponding file. Else, a dataset used in order to train the model using a Random Forest. The model is saved as
        a pickle file for future use.
        """
        if os.path.exists(self.model_filename):
            try:
                self.model = pickle.load(open(self.model_filename, "rb"))

                #print("Loaded persisted sarcasm model.")

                return
            except Exception:
                print("Failed to load model. Resuming model creation...")
        x_train, x_test, y_train, y_test = self.__train_test_data_sarcasm(self.sarcasm_file)

        self.model = Pipeline([
            ('u1', FeatureUnion([
                ('vectorizer', Pipeline([
                    ('cv', CountVectorizer(lowercase=True, tokenizer=tokenize_text, preprocessor=dummy_func,
                                           ngram_range=(1, 2))),
                ])),
                ('text_features', Pipeline([
                    ('text', SarcasmFeatures()),
                ])),
            ])),
            ('clf', RandomForestClassifier()),
        ])

        self.model.fit(x_train, y_train)
        y_pred = self.model.predict(x_test)
        accuracy = metrics.accuracy_score(y_test, y_pred)
        recall = metrics.recall_score(y_test, y_pred, average="macro")
        precision = metrics.precision_score(y_test, y_pred, average="macro")
        f1 = metrics.f1_score(y_test, y_pred, average="macro")
        print("\tAccuracy: %f" % accuracy)
        print("\tRecall: %f" % recall)
        print("\tPrecision: %f" % precision)
        print("\tF1: %f" % f1)

        pickle.dump(self.model, open(self.model_filename, "wb"))

    def predict_sarcasm(self, doc):
        return self.model.predict([doc])[0]

    def predict_sarcasm_str(self, doc):
        return self.model.predict([doc])[0]

    def __train_test_data_sarcasm(self, filename):
        data = pd.read_csv(filename, error_bad_lines=False)

        data = data.dropna(how='any', axis=0)
        feature_data = list(data['Tweet'])
        labels = list(data['Label'])

        x_train, x_test, y_train, y_test = train_test_split(feature_data, labels, test_size=0.3, random_state=123,
                                                            shuffle=True)
        return x_train, x_test, y_train, y_test


def dummy_func(doc):
    return doc



