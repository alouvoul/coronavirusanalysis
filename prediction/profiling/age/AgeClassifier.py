import pickle
import os
import pandas as pd
from sklearn import metrics

from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.svm import SVC

from FeatureExtraction.GenderFeatures import GenderFeatures
from prediction.profiling.UserProfile import AGE_18_24, AGE_25_34, AGE_35_49, AGE_50_XX
from process.tokenizing import preprocess_text
from utils.path_utils import resource_path


class AgeClassifier:
    """
    This class detects the age of a user based on users posts
    """
    feature_extractor = None

    model_filename = resource_path("models/usr_age_ml_model.sav")
    model = None

    def __init__(self):

        if os.path.exists(self.model_filename):
            try:
                self.model = pickle.load(open(self.model_filename, "rb"))

                #print("Loaded persisted age model.")

                return
            except Exception:
                print("Failed to load model. Resuming model creation...")
        train_filename = resource_path("pan15-author-csvs/tweet_age_train.csv")
        test_filename = resource_path("pan15-author-csvs/tweet_age_test.csv")

        train_df = pd.read_csv(train_filename, delimiter=",")
        test_df = pd.read_csv(test_filename, delimiter=",")

        x_train = self.__transform_x(train_df.iloc[:, 1])
        y_train = self.__transform_y(train_df.iloc[:, 2])
        x_test = self.__transform_x(test_df.iloc[:, 1])
        y_test = self.__transform_y(test_df.iloc[:, 2])

        print("Training user age models...")

        svm = SVC(kernel="rbf", probability=True)
        rf = RandomForestClassifier()
        knn = KNeighborsClassifier(n_neighbors=16)

        # create estimators list
        estimators = [("svm", svm), ("rf", rf)]

        self.model = Pipeline([
            ('u1', FeatureUnion([
                ('vectorizer', Pipeline([
                    ('cv', CountVectorizer(strip_accents='unicode',
                                           analyzer="word",
                                           token_pattern=r"[\w\-:]+",
                                           stop_words="english",
                                           max_df=0.85,
                                           ngram_range=(1, 2),
                                           lowercase=True)
                     )
                ])),
                ('text_features', Pipeline([
                    ('text', GenderFeatures()),
                ])),
            ])),
            ('clf', VotingClassifier(estimators, voting="soft")),
        ])

        self.model.fit(x_train, y_train)

        print("Prediction test metrics:")

        y_pred = self.model.predict(x_test)

        accuracy = metrics.accuracy_score(y_test, y_pred)
        recall = metrics.recall_score(y_test, y_pred, average="macro")
        precision = metrics.precision_score(y_test, y_pred, average="macro")
        f1 = metrics.f1_score(y_test, y_pred, average="macro")

        # Best results so far

        print("\tAccuracy: %f" % accuracy)
        print("\tRecall: %f" % recall)
        print("\tPrecision: %f" % precision)
        print("\tF1: %f" % f1)

        print("Persisting Models...")
        pickle.dump(self.model, open(self.model_filename, "wb"))

    def __transform_x(self, x_list: list):
        """
        Convert input vars to features
        :param x_list: the raw x's
        :return: the dataset transformed texts to preprocessed tokens
        """
        transformed_x = []

        for x in x_list:
            tokens = preprocess_text(x, discard_limited_tokens=False)
            new_x = " ".join(tokens)
            transformed_x.append(new_x)

        return transformed_x

    def __transform_y(self, y_list: list):
        """
        Convert string age labels to numbers
        :param y_list: the y's as strings
        :return: the labels as numbers
        """
        transformed_y = []
        # 18-24, 25-34, 35-49, 50-XX
        for y in y_list:
            if y == AGE_18_24:
                transformed_y.append(0)
            elif y == AGE_25_34:
                transformed_y.append(1)
            elif y == AGE_35_49:
                transformed_y.append(2)
            elif y == AGE_50_XX:
                transformed_y.append(3)

        return transformed_y

    def age_of_post(self, post: str):
        """
        Detect the age of a user based on a post's text
        :param post: a string of text contained in the post
        :return: the detected age as a string constant
        """
        tokens = preprocess_text(post, discard_limited_tokens=False)
        cleaned_post = " ".join(tokens)
        prediction = self.model.predict([cleaned_post])[0]

        if prediction == 0:
            return AGE_18_24
        elif prediction == 1:
            return AGE_25_34
        elif prediction == 2:
            return AGE_35_49
        elif prediction == 3:
            return AGE_50_XX

        return None

    def age_of(self, tokens: list):
        """
        Detect the age of a user based on a post's preprocessed tokens
        :param tokens: a list of preprocessed tokens
        :return: the detected age as a string constant
        """
        cleaned_post = " ".join(tokens)
        prediction = self.model.predict([cleaned_post])[0]

        if prediction == 0:
            return AGE_18_24
        elif prediction == 1:
            return AGE_25_34
        elif prediction == 2:
            return AGE_35_49
        elif prediction == 3:
            return AGE_50_XX

        return None

