import random

from prediction.profiling.UserProfile import AGE_18_24, AGE_25_34, AGE_35_49, AGE_50_XX
from prediction.profiling.age.AgeClassifier import AgeClassifier


class AgeDetector:
    """
    This detector class is used to detect a users age based on their posts content
    """
    classifier = None

    def __init__(self):
        self.classifier = AgeClassifier()

    def detect_age(self, posts: list):
        """
        Detect the age of a user based a list of post texts
        :param posts: a list of str - posts content
        :return: detected age string constant
        """
        count_18_24 = 0
        count_25_34 = 0
        count_35_49 = 0
        count_50_xx = 0

        # detect the age for each post
        for post in posts:
            pred = self.classifier.age_of_post(post)

            # increase respective count
            if pred == AGE_18_24:
                count_18_24 += 1
            elif pred == AGE_25_34:
                count_25_34 += 1
            elif pred == AGE_35_49:
                count_35_49 += 1
            elif pred == AGE_50_XX:
                count_50_xx += 1

        # find the max count
        max_count = max(count_18_24, count_25_34, count_35_49, count_50_xx)

        # find the age with max count - best effort if all equal start from lowest to highest
        # since lower ages are more likely to use social media
        if max_count == count_18_24:
            return AGE_18_24
        elif max_count == count_25_34:
            return AGE_25_34
        elif max_count == count_35_49:
            return AGE_35_49
        elif max_count == count_50_xx:
            return AGE_50_XX

        # worst effort (fallback) return random age if no age was found
        # will never probably go here, but still return something
        all = [AGE_18_24, AGE_25_34, AGE_35_49, AGE_50_XX]
        return all[random.randint(0, 3)]