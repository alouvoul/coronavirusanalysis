import gc

from prediction.profiling.UserProfile import UserProfile
from prediction.profiling.age.AgeDetector import AgeDetector
from prediction.profiling.gender.GenderDetector import GenderDetector
from prediction.profiling.aggression.AggressionDetector import AggressionDetector
from prediction.profiling.sarcasm.SarcasmDetector import SarcasmDetector


class UserProfileBuilder:
    """
    This utility class is a single entry point that builds a users complete profile
    using the required detectors
    """

    def __init__(self):
        self.none = 0

    def build_profile(self, username: str, post_texts: list):
        """
        Build a users complete profile with their username and posts
        :param username: the users username
        :param post_texts: a list of the users post contents
        :return:
        """
        profile = UserProfile()

        profile.gender = self.__gender_of(username, post_texts)
        profile.age = self.__age_of(username, post_texts)
        profile.aggressive = self.__aggression_of(username, post_texts)
        profile.sarcastic = self.__sarcasm_of(username, post_texts)
        profile.interests = self.__interests_of(username, post_texts)

        return profile

    def __gender_of(self, username: str, post_texts: list):
        detector = GenderDetector()
        gender = detector.detect_gender(username, post_texts)

        # memory cleanup
        del detector
        gc.collect()

        return gender

    def __age_of(self, username: str, post_texts: list):
        detector = AgeDetector()
        age = detector.detect_age(post_texts)

        # memory cleanup
        del detector
        gc.collect()

        return age

    def __aggression_of(self, username: str, post_texts: list):
        detector = AggressionDetector()
        aggression = detector.detect_aggression(post_texts)

        # memory cleanup
        del detector
        gc.collect()

        return aggression

    def __sarcasm_of(self, username: str, post_texts: list):
        detector = SarcasmDetector()
        sarcasm = detector.detect_sarcastic_user(post_texts)

        # memory cleanup
        del detector
        gc.collect()

        return sarcasm

    def __interests_of(self, username: str, post_texts: list):
        """
        Not implemented !!!!
        :param username:
        :param post_texts:
        :return:
        """
        return []
