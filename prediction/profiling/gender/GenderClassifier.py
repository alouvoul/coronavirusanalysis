import pickle
import os
import pandas as pd
from sklearn import metrics

from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.svm import SVC

from FeatureExtraction.GenderFeatures import GenderFeatures
from prediction.profiling.UserProfile import GENDER_OTHER, GENDER_FEMALE, GENDER_MALE
from process.tokenizing import preprocess_text
from utils.path_utils import resource_path


class GenderClassifier:
    feature_extractor = None

    model_filename = resource_path("models/usr_gender_ml_model.sav")
    model = None

    def __init__(self):
        """
        Constructor of the class. If there a model already exists just imports it using pickle. Otherwise get some ML
        models to evaluate and saves the one with the best performance. A voting classifier is used to make a collective
        decision using a SVM and RF.
        """
        if os.path.exists(self.model_filename):
            try:
                self.model = pickle.load(open(self.model_filename, "rb"))

                #print("Loaded persisted gender model.")

                return
            except Exception:
                print("Failed to load model. Resuming model creation...")
        train_filename = resource_path("pan15-author-csvs/tweet_gender_train.csv")
        test_filename = resource_path("pan15-author-csvs/tweet_gender_test.csv")

        train_df = pd.read_csv(train_filename, delimiter=",")
        test_df = pd.read_csv(test_filename, delimiter=",")

        x_train = self.__transform_x(train_df.iloc[:, 1])
        y_train = self.__transform_y(train_df.iloc[:, 2])
        x_test = self.__transform_x(test_df.iloc[:, 1])
        y_test = self.__transform_y(test_df.iloc[:, 2])

        print("Training user gender models...")

        svm = SVC(kernel="linear", probability=True)
        rf = RandomForestClassifier()

        # create estimators list
        estimators = [("svm", svm), ("rf", rf)]

        self.model = Pipeline([
            ('u1', FeatureUnion([
                ('vectorizer', Pipeline([
                    ('cv', CountVectorizer(strip_accents='unicode',
                                           analyzer="word",
                                           token_pattern=r"[\w\-:]+",
                                           stop_words="english",
                                           max_df=0.85,
                                           ngram_range=(1, 2),
                                           lowercase=True)
                     )
                ])),
                ('text_features', Pipeline([
                    ('text', GenderFeatures()),
                ])),
            ])),
            ('clf', VotingClassifier(estimators, voting="soft")),
        ])

        self.model.fit(x_train, y_train)

        print("Prediction test metrics:")

        y_pred = self.model.predict(x_test)

        accuracy = metrics.accuracy_score(y_test, y_pred)
        recall = metrics.recall_score(y_test, y_pred, average="macro")
        precision = metrics.precision_score(y_test, y_pred, average="macro")
        f1 = metrics.f1_score(y_test, y_pred, average="macro")

        # Best results so far

        print("\tAccuracy: %f" % accuracy)
        print("\tRecall: %f" % recall)
        print("\tPrecision: %f" % precision)
        print("\tF1: %f" % f1)

        print("Persisting Models...")
        pickle.dump(self.model, open(self.model_filename, "wb"))

    def __transform_x(self, x_list: list):
        transformed_x = []

        for x in x_list:
            tokens = preprocess_text(x, discard_limited_tokens=False)
            new_x = " ".join(tokens)
            transformed_x.append(new_x)

        return transformed_x

    def __transform_y(self, y_list: list):
        transformed_y = []

        for y in y_list:
            if y == "M":
                transformed_y.append(0)
            else:
                transformed_y.append(1)

        return transformed_y

    def gender_of_post(self, post: str):
        """
        The given string is used to predict the gender of the individual. The model that have trained used in order to
        make the predictions.
        :param post: A string sentence
        :return: "M" if is a male, "F" if is a female, "O" if is other
        """
        tokens = preprocess_text(post, discard_limited_tokens=False)
        cleaned_post = " ".join(tokens)
        prediction = self.model.predict([cleaned_post])[0]

        if prediction == 0:
            return GENDER_MALE
        elif prediction == 1:
            return GENDER_FEMALE

        return GENDER_OTHER

    def gender_of(self, tokens: list):
        """
        An array of tokens used to make the prediction of gender
        :param tokens: Words
        :return: "M" if is a male, "F" if is a female, "O" if is other
        """
        cleaned_post = " ".join(tokens)
        prediction = self.model.predict([cleaned_post])[0]

        if prediction == 0:
            return GENDER_MALE
        elif prediction == 1:
            return GENDER_FEMALE

        return GENDER_OTHER
