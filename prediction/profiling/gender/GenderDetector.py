import pandas as pd
from nltk.corpus import names
from nltk.stem.porter import PorterStemmer

from prediction.profiling.UserProfile import GENDER_FEMALE, GENDER_MALE, GENDER_OTHER
from prediction.profiling.gender.GenderClassifier import GenderClassifier
from utils.path_utils import resource_path


class GenderDetector:
    males = []
    females = []
    stemmer = PorterStemmer()

    classifier = None

    def __init__(self):
        """
        In this method the model which used for gender prediction initialized for supervised learning. Also, there is an
        unsupervised learning using dictionary. If a user's name exists in a lexicon the respective value returned as a
        results. In the case that there is not in a lexicon the trained ML model used to make the prediction.
        """
        self.classifier = GenderClassifier()

        self.males = [name.lower() for name in names.words('male.txt')]

        all_names = []
        for name in self.males:
            all_names.append(name)
            all_names.append(self.stemmer.stem(name))

        self.males = set(all_names)

        self.females = [name.lower() for name in names.words('female.txt')]

        all_names = []
        for name in self.females:
            all_names.append(name)
            all_names.append(self.stemmer.stem(name))

        self.females = set(all_names)

    def detect_gender(self, user_name: str, posts: list):
        """

        :param user_name: A user's name in string format
        :param posts: List of posts
        :return: "M" if is a male, "F" if is a female, "O" if is other
        """
        name_gender = self.detect_gender_by_user_name(user_name)
        posts_gender = self.detect_gender_by_posts_texts(posts)

        if name_gender == posts_gender:
            return name_gender
        elif name_gender == GENDER_OTHER:
            return posts_gender
        elif name_gender != GENDER_OTHER:
            return name_gender

        prediction_sum = 0

        if name_gender == GENDER_MALE:
            prediction_sum += 0.0
        elif name_gender == GENDER_FEMALE:
            prediction_sum += 1.0
        else:
            prediction_sum += 0.5

        if posts_gender == GENDER_MALE:
            prediction_sum += 0.0
        elif posts_gender == GENDER_FEMALE:
            prediction_sum += 1.0
        else:
            prediction_sum += 0.5

        prediction = prediction_sum / 2.0

        if 0 <= prediction <= 0.45:
            return GENDER_MALE
        elif 0.45 < prediction < 0.55:
            return GENDER_OTHER
        elif 0.55 <= prediction <= 1.0:
            return GENDER_FEMALE

        return GENDER_OTHER

    def __gender_of_token(self, name_token):
        if name_token in self.males and name_token not in self.females:
            # if male add 0
            return 0.0
        elif name_token in self.females and name_token not in self.males:
            # if female add 1
            return 1.0
        elif name_token in self.males and name_token in self.females:
            # if undetermined add 0.5
            return 0.5
        else:
            stemmed_name_token = self.stemmer.stem(name_token)

            if stemmed_name_token != name_token:
                return self.__gender_of_token(stemmed_name_token)

            # if undetermined add 0.5
            #   to avoid skewing prediction
            return 0.5

    def detect_gender_by_user_name(self, user_name):
        tokens = [t.strip().lower() for t in user_name.split()]
        prediction_sum = 0.0

        if tokens is None or len(tokens) == 0:
            return GENDER_OTHER

        # find gender for each token in user's name
        for name_token in tokens:
            prediction_sum += self.__gender_of_token(name_token)

        prediction = prediction_sum / len(tokens)

        if 0 <= prediction <= 0.45:
            return GENDER_MALE
        elif 0.45 < prediction < 0.55:
            return GENDER_OTHER
        elif 0.55 <= prediction <= 1.0:
            return GENDER_FEMALE

        return GENDER_OTHER

    def detect_gender_by_posts_texts(self, posts: list):
        prediction_sum = 0.0

        for post in posts:
            pred = self.classifier.gender_of_post(post)

            if pred == GENDER_MALE:
                prediction_sum += 0.0
            elif pred == GENDER_FEMALE:
                prediction_sum += 1.0
            else:
                prediction_sum += 0.5

        if posts is None or len(posts) == 0:
            return GENDER_OTHER

        prediction = prediction_sum / len(posts)

        if 0 <= prediction <= 0.45:
            return GENDER_MALE
        elif 0.45 < prediction < 0.55:
            return GENDER_OTHER
        elif 0.55 <= prediction <= 1.0:
            return GENDER_FEMALE

        return GENDER_OTHER

    def detect_gender_by_posts_tokens(self, tokens: list):
        prediction_sum = 0.0

        for postTokens in tokens:
            pred = self.classifier.gender_of(postTokens)

            if pred == GENDER_MALE:
                prediction_sum += 0.0
            elif pred == GENDER_FEMALE:
                prediction_sum += 1.0
            else:
                prediction_sum += 0.5

        prediction = prediction_sum / len(tokens)

        if 0 <= prediction <= 0.45:
            return GENDER_MALE
        elif 0.45 < prediction < 0.55:
            return GENDER_OTHER
        elif 0.55 <= prediction <= 1.0:
            return GENDER_FEMALE

        return GENDER_OTHER


def ds_make(target):
    """
    Read the dataset which is in a specific format in XML. The dataset after returned.
    :param target:
    :return:
    """
    comments_df = pd.read_csv(resource_path("train-balanced-sarcasm.csv"),
                              delimiter="\t")
    truth_df = pd.read_csv(resource_path("/aggression_wiki_dataset/aggression_annotations.tsv"), delimiter="\t")

    x_ids = []
    x_comments = []
    y_aggression_probs = []

    for row in comments_df[comments_df['split'].notnull() & (comments_df['split'] == target)].values:
        # rev_id	comment	year	logged_in	ns	sample	split
        rev_id = row[0]
        comment = row[1].replace("NEWLINE_TOKEN", " ").replace("TAB_TOKEN", " ")

        count = 0
        aggression_sum = 0

        for truth in truth_df[truth_df['rev_id'].notnull() & (truth_df['rev_id'] == rev_id)].values:
            # rev_id	worker_id	aggression
            aggression_sum += truth[2]
            count += 1

        if count == 0:
            print("Truth for comment", rev_id, "not found. Skipping")
            continue

        aggression_prob = 0

        if aggression_sum > 0:
            aggression_prob = round(aggression_sum / count, 2)

        x_ids.append(rev_id)
        x_comments.append(comment)
        y_aggression_probs.append(aggression_prob)

    df = pd.DataFrame({"ID": x_ids, "Comment": x_comments, "AggressionProbability": y_aggression_probs},
                      columns=["ID", "Comment", "AggressionProbability"])
    return df
