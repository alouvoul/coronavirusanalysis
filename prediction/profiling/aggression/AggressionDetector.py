from prediction.profiling.UserProfile import AGGRESSION_NONE, AGGRESSION_LOW, AGGRESSION_MED, AGGRESSION_HIGH
from prediction.profiling.aggression.AggressionClassifier import AggressionClassifier


class AggressionDetector:
    """
    This class is used to detect a users aggression based on their posts
    """
    classifier = None

    def __init__(self):
        self.classifier = AggressionClassifier()

    def detect_aggression(self, posts: list):
        """
        Detect aggression level based on a list of posts
        :param posts: a list of strings - the users posts
        :return: an integer constant representing the predicted aggression bucket
        """
        count_none = 0
        count_low = 0
        count_med = 0
        count_high = 0

        for post in posts:
            pred = self.classifier.aggression_of_post(post)

            if pred == AGGRESSION_NONE:
                count_none += 1
            elif pred == AGGRESSION_LOW:
                count_low += 1
            elif pred == AGGRESSION_MED:
                count_med += 1
            elif pred == AGGRESSION_HIGH:
                count_high += 1

        max_count = max(count_none, count_low, count_med, count_high)

        if max_count == count_none:
            return AGGRESSION_NONE
        elif max_count == count_low:
            return AGGRESSION_LOW
        elif max_count == count_med:
            return AGGRESSION_MED
        elif max_count == count_high:
            return AGGRESSION_HIGH

        return AGGRESSION_NONE
