import os
import pickle

import pandas as pd
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.svm import SVC

from FeatureExtraction.SarcasmFeatures import SarcasmFeatures
from prediction.profiling.UserProfile import AGGRESSION_NONE, AGGRESSION_LOW, AGGRESSION_MED, AGGRESSION_HIGH
from process.tokenizing import preprocess_text
from utils.path_utils import resource_path


class AggressionClassifier:
    """
    This class is used to load/create the ML models used for detecting user aggression.
    """
    feature_extractor = None

    model_filename = resource_path("models/usr_aggr_ml_model.sav")
    model = None

    def __init__(self):

        if os.path.exists(self.model_filename):
            try:
                self.model = pickle.load(open(self.model_filename, "rb"))

                # print("Loaded persisted aggression model.")

                return
            except Exception as err:
                print("Failed to load model. Resuming model creation...")
        train_filename = resource_path("aggression_wiki_dataset/wiki_aggression_train.csv")
        test_filename = resource_path("aggression_wiki_dataset/wiki_aggression_test.csv")

        train_df = pd.read_csv(train_filename, delimiter=",")
        test_df = pd.read_csv(test_filename, delimiter=",")

        x_train = self.__transform_x(train_df.iloc[:, 1])
        y_train = self.__transform_y(train_df.iloc[:, 2])
        x_test = self.__transform_x(test_df.iloc[:, 1])
        y_test = self.__transform_y(test_df.iloc[:, 2])

        print("Training aggression models...")

        svm = SVC(kernel="rbf", probability=True)
        rf = RandomForestClassifier()

        # create estimators list
        estimators = [("svm", svm), ("rf", rf)]

        self.model = Pipeline([
            ('u1', FeatureUnion([
                ('vectorizer', Pipeline([
                    ('cv', CountVectorizer(strip_accents='unicode',
                                           analyzer="word",
                                           token_pattern=r"[\w\-:]+",
                                           stop_words="english",
                                           max_df=0.85,
                                           ngram_range=(1, 2),
                                           lowercase=True)
                     )
                ])),
                ('text_features', Pipeline([
                    ('text', SarcasmFeatures()),
                ])),
            ])),
            ('clf', VotingClassifier(estimators, voting="soft")),
        ])

        self.model.fit(x_train, y_train)

        print("Prediction test metrics:")

        y_pred = self.model.predict(x_test)

        accuracy = metrics.accuracy_score(y_test, y_pred)
        recall = metrics.recall_score(y_test, y_pred, average="macro")
        precision = metrics.precision_score(y_test, y_pred, average="macro")
        f1 = metrics.f1_score(y_test, y_pred, average="macro")

        # Best results so far

        print("\tAccuracy: %f" % accuracy)
        print("\tRecall: %f" % recall)
        print("\tPrecision: %f" % precision)
        print("\tF1: %f" % f1)

        print("Persisting Models...")
        pickle.dump(self.model, open(self.model_filename, "wb"))

    def __transform_x(self, x_list: list):
        """
        Transforms the input text into preprocessed text
        :param x_list: a list of posts or tweets
        :return: list[String] - transformed dataset
        """
        transformed_x = []

        for x in x_list:
            tokens = preprocess_text(x, discard_limited_tokens=False)
            new_x = " ".join(tokens)
            transformed_x.append(new_x)

        return transformed_x

    def __transform_y(self, y_list: list):
        """
        Convert aggression value to buckets i.e. to classification task
        :param y_list:
        :return:
        """
        transformed_y = []

        for y in y_list:
            if y == 0:
                transformed_y.append(AGGRESSION_NONE)
            elif 0 < y <= 0.33:
                transformed_y.append(AGGRESSION_LOW)
            elif 0.33 < y <= 0.66:
                transformed_y.append(AGGRESSION_MED)
            elif 0.66 < y <= 1.0:
                transformed_y.append(AGGRESSION_HIGH)

        return transformed_y

    def aggression_of_post(self, post: str):
        """
        Detect aggression of a post based on it's text
        :param post: a string of text with the posts content
        :return: an integer constant representing the predicted bucket
        """
        tokens = preprocess_text(post, discard_limited_tokens=False)
        cleaned_post = " ".join(tokens)
        prediction = self.model.predict([cleaned_post])[0]

        if prediction == 0:
            return AGGRESSION_NONE
        elif prediction == 1:
            return AGGRESSION_LOW
        elif prediction == 2:
            return AGGRESSION_MED
        elif prediction == 3:
            return AGGRESSION_HIGH

        return None

    def aggression_of(self, tokens: list):
        """
        Detect aggression of a post based on it's preprocessed tokens
        :param tokens: a list of preprocessed tokens with the posts content
        :return: an integer constant representing the predicted bucket
        """
        cleaned_post = " ".join(tokens)
        prediction = self.model.predict([cleaned_post])[0]

        if prediction == 0:
            return AGGRESSION_NONE
        elif prediction == 1:
            return AGGRESSION_LOW
        elif prediction == 2:
            return AGGRESSION_MED
        elif prediction == 3:
            return AGGRESSION_HIGH

        return None
