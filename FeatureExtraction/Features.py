import re

from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

from prediction.emotion.EmojiEmotionExtractor import EmojiEmotionExtractor
from prediction.unsupervised import UnsupervisedSentiment
from process.tokenizing import clear_emoji, demoji


class Features:

    def __init__(self):
        self.words_analyzer = SentimentIntensityAnalyzer()
        self.unsupervised_sentiment = UnsupervisedSentiment()
        self.sentiments = {
            "Negative": 0,
            "Neutral": 1,
            "Positive": 2,
        }

    def number_of_mentions(self, text):
        """
        Method which summarize the total number of mentions exist in the text. Mentions start with @ symbol.
        :param text: Tokenized text
        :return: Total number of mentions
        """
        mentions = 0
        for token in text:
            if token == "MENTION":
                mentions += 1
        return mentions

    def number_of_hashtags(self, text):
        """
        Method which summarize the total number of hash tags exist in the text. Mentions start with # symbol.
        :param text: Tokenized text
        :return: Total number of hash tags
        """
        hashtags = 0
        for token in text:
            if token == "HASHTAG":
                hashtags += 1
        return hashtags

    def character_flooding(self, doc):
        """
        Calculated the number of times that a doc contains token that has character flooding. For example, words like
        goooood and niceeee both have character flooding
        :param doc: Tokenized text
        :return: number of words which contains character flooding
        """
        flooding = 0
        for token in doc:
            if re.match(r'[A-Za-z]+([a-zA-Z])\1+', token):
                flooding += 1
        return flooding

    def number_of_punctuations(self, doc):
        """
        Total number of 3 punctuations are calculated. '?', '!' and '.' supposed that are meaningful as a feature.
        :param doc:Tokenized text of tokens
        :return: number of punctuations that are in the document
        """
        number_of_punctuations = 0
        for token in doc:
            number_of_punctuations += sum([1 for x in token if x in ['?', '!', '.']])
        return number_of_punctuations

    def capitals(self, doc):
        """
        Calculates the number of capital words in a document.
        :param doc: Array of string tokens
        :return: Total number of capital letter in a document
        """
        upper = 0
        for tokens in doc:
            if tokens.isupper():
                upper += 1
        return upper

    def vowel(self, doc):
        """
        Count the total number of vowels in the given array of token strings.
        :param doc: Array of string tokens
        :return: Number of vowels in the doc
        """
        number_of_words_without_vowels = []
        for token in doc:
            number_of_words_without_vowels.append(re.findall(r'\b[^AEIOU_0-9\W]+\b', token, flags=re.I))
        return len(number_of_words_without_vowels)

    def effect_of_words(self, doc):
        """
        This method used to count the sentiment of a document. Negative and positive scores added to the variable in
        order to get the average value of them.
        :param doc: Array of string tokens
        :return: Average values for positive and negative words, If the doc is empty return 0, 0
        """
        positive = 0
        negative = 0
        for token in doc:
            results = self.words_analyzer.polarity_scores(token)
            positive += results['pos']
            negative += results['neg']

        if len(doc) < 1:
            return 0, 0

        return positive / len(doc), negative / len(doc)

    def sentiment(self, doc):
        """
        Used to calculated the sentiment of a document.
        :param doc: Array of string tokens
        :return: Return 0 for negative, 1 for neutral and 2 for positive sentiment in the doc
        """
        polarity = self.unsupervised_sentiment.vader_sentiment(doc)
        sentiment = self.unsupervised_sentiment.polarity(polarity["compound"], bound=0.05)
        return self.sentiments[sentiment]

    def number_of_emoji(self, doc):
        """
        Method that finds the total number of emojies of a given array of tokens. Uses a simple regex to find the emoji.
        :param doc: Array of string tokens
        :return: Total number of emojies
        """
        emojies = clear_emoji(doc)
        total = 0
        for e in emojies:
            if re.search(r":[A-Za-z\-_]+:", e):
                total += 1
        return total

    def emojies(self, doc):
        """
        This method used to detect emoji. All the emoji are separating in 4 categories anger, fear, joy, sadness and
        all of the detected emoji add a value to their category.
        :param doc: Array of string tokens
        :return: anger, fear, joy, sadness average value
        """
        emoji_emo = EmojiEmotionExtractor()
        emojies = demoji(doc)
        emoji_emotion_values = []
        for e in emojies:
            if emoji_emo.is_emoji_name_like(e):

                if emoji_emo.exists_emoji_name(e):
                    emotions = emoji_emo.emotions_of_emoji_named(e)
                    emoji_emotion_values.append(emotions)

        if len(emoji_emotion_values) > 0:
            angerTotal = 0
            fearTotal = 0
            joyTotal = 0
            sadnessTotal = 0

            for emotions in emoji_emotion_values:
                angerTotal += emotions[0]
                fearTotal += emotions[1]
                joyTotal += emotions[2]
                sadnessTotal += emotions[3]

            anger = angerTotal / len(emoji_emotion_values)
            fear = fearTotal / len(emoji_emotion_values)
            joy = joyTotal / len(emoji_emotion_values)
            sadness = sadnessTotal / len(emoji_emotion_values)

            return anger, fear, joy, sadness
        return 0, 0, 0, 0

    def intensifiers(self):
        pass

# EOF
