import pandas as pd

from FeatureExtraction.Features import Features


class SarcasmFeatures:

    # fit() doesn't do anything, this is a transformer class
    def fit(self, x, y=None):
        return self

    def transform(self, data):
        """
        Transform the documents to feature vector. Used by Feature union to concat with vectorizers.
        :param docs: Array of docs
        :return: Dataframe of features
        """
        feature_extractor = Features()
        features = []
        for doc in data:
            flooding = feature_extractor.character_flooding(doc)
            punctuations = feature_extractor.number_of_punctuations(doc)
            sentiment_prediction = feature_extractor.sentiment(doc)
            number_of_vowels = feature_extractor.vowel(doc)
            positive_words, negative_words = feature_extractor.effect_of_words(doc)
            number_emoji = feature_extractor.number_of_emoji(doc)

            temp = [flooding, punctuations, sentiment_prediction, number_of_vowels, positive_words, negative_words, number_emoji]
            features.append(temp)
        return pd.DataFrame(features)
