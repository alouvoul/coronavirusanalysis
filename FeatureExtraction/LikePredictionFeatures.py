import pandas as pd

from FeatureExtraction.Features import Features


class LikePredictionFeatures:

    def __init__(self):
        self.feature_extractor = Features()

    # fit() doesn't do anything, this is a transformer class
    def fit(self, x, y=None):
        return self

    def transform(self, docs):
        """
        Transform the documents to feature vector. Used by Feature union to concat with vectorizers.
        :param docs: Array of docs
        :return: Dataframe of features
        """

        features = []
        for doc in docs:
            total_number_mentions = self.feature_extractor.number_of_mentions(doc)
            total_number_hashtags = self.feature_extractor.number_of_hashtags(doc)
            flooding = self.feature_extractor.character_flooding(doc)
            total_number_punctuations = self.feature_extractor.number_of_punctuations(doc)
            number_of_emojis = self.feature_extractor.number_of_emoji(doc)
            sentiment = self.feature_extractor.sentiment(doc)
            # emojis = feature_extractor.emoji(doc)
            temp = [total_number_mentions, total_number_hashtags, flooding, total_number_punctuations, number_of_emojis,
                    sentiment]
            features.append(temp)
        return pd.DataFrame(features)
