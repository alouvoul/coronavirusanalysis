import pandas as pd

from FeatureExtraction.Features import Features


class GenderFeatures:

    # fit() doesn't do anything, this is a transformer class
    def fit(self, x, y=None):
        return self

    def transform(self, docs):
        """
        Transform the documents to feature vector. Used by Feature union to concat with vectorizers.
        :param docs: Array of docs
        :return: Dataframe of features
        """
        feature_extractor = Features()
        features = []
        for doc in docs:

            total_number_mentions = feature_extractor.number_of_mentions(doc)
            total_number_hashtags = feature_extractor.number_of_hashtags(doc)
            flooding = feature_extractor.character_flooding(doc)
            total_number_punctuations = feature_extractor.number_of_punctuations(doc)
            temp = [total_number_mentions, total_number_hashtags, flooding, total_number_punctuations]
            features.append(temp)
        return pd.DataFrame(features)

