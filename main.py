import sys

from prediction.like_prediction.BaseModelDetector import BaseModelDetector
from process.jobs import start_data_processing
from scrap.jobs import start_scrap_job
from server.init import start_server


def args_error():
    """
    Prints an argument error message and exits the application.
    :return: Nothing
    """
    print("Illegal argument error. Use --scrap SOURCE, --process or --server")
    exit(1)


if __name__ == "__main__":
    """
    The main entry point of the application.
    Based on the arguments the correct component is invoked or an argument error is thrown.
    """

    arg_count = len(sys.argv)

    if arg_count < 2:
        args_error()

    # detect the application component to start
    option_arg = sys.argv[1]
    scrap_mode = option_arg == "--scrap"
    process_mode = option_arg == "--process"
    server_mode = option_arg == "--server"

    if scrap_mode:
        if arg_count != 3:
            args_error()

        source_option = sys.argv[2]

        twitter = False
        instagram = False

        #  detect the source to scrap from
        if source_option.lower() == "twitter":
            twitter = True
        elif source_option.lower() == "instagram":
            instagram = True
        else:
            print("Unrecognized SOURCE argument. Available sources are 'twitter' and 'instagram'.")
            args_error()

        start_scrap_job(twitter=twitter, instagram=instagram)
    elif process_mode:
        start_data_processing()
    elif server_mode:
        start_server(8080)
    else:
        args_error()

    print("Shutting down...")

# EOF
