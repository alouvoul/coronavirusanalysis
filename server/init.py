from server.app_lifecycle import will_start_app
from server.app_lifecycle import will_stop_app
from app import app
import atexit

with app.app_context():
    will_start_app()


def start_server(port: int):
    print("Starting Server at port " + str(port) + "...")

    atexit.register(will_stop_app)

    # run the flask app
    app.run(debug=True, port=port)
# EOF
