import gc
from typing import Callable, List

import spacy

from data.entities.CAPost import CAPost
from data.entities.CAUser import CAUser
from data.entities.ca_constants import SOURCE_TWITTER, SOURCE_INSTAGRAM
from data.mongo import mongo
from prediction.emotion.EmotionDetector import EmotionDetector
from prediction.geocoding import location_of_tweet, location_of_ig_post
from prediction.like_prediction.ContextDetector import ContextDetector
from prediction.profiling.UserProfileBuilder import UserProfileBuilder
from prediction.sentiment.SentimentDetector import SentimentDetector
from process.tokenizing import preprocess_text
from scrap.InstaAPI import IGPost, IGUser
from scrap.TwitterAPI import TWTweet, TWUser


##############################################################################
#
#   Section: CAPost converters
#
##############################################################################


def twitter_post_make(tweet: TWTweet, twitter_user: TWUser, topic_resolver: Callable[[list], list]):
    """
    :param tweet: The stored TWTweet.
    :param twitter_user: The stored TWUser that published the tweet.
    :param topic_resolver: A function that resolves the tweets topic.
    :return: CAPost - A processed representation of the tweet in the common namespace.
    """
    post = CAPost(source=SOURCE_TWITTER, ext_id=tweet.tweet_id, ext_user_id=tweet.user_id, created_at=tweet.created_at)

    tags = []
    users = mongo().twitter.get_users_by_id(tweet.tagged_users)

    for user_id in tweet.tagged_users:
        tag = {"user_id": user_id, "username": ""}
        matches = [u for u in users if u["user_id"] == user_id]

        if matches is not None and len(matches) > 0:
            user = matches[0]
            tag["username"] = user["screen_name"]

        tags.append(tag)

    post.tags = tags
    post.hashtags = tweet.hashtags

    post.text = tweet.text
    post.tokens = preprocess_text(tweet.text, discard_limited_tokens=False)  # TODO: maybe true ???

    if post.tokens is None or len(post.tokens) == 0:
        return None

    post.sentiment = __detect_sentiment(tweet.text)
    post.emotion = __detect_emotion(tweet.text)

    location = location_of_tweet(tweet)

    if location is not None:
        post.set_location(location[0], location[1])

    post.topics = topic_resolver(post.tokens)
    post.named_entities = __named_entities(post.tokens)

    post.like_count = tweet.fave_count
    post.predicted_like_count = __predict_tweet_likes(tweet, twitter_user, post.tokens)

    return post


def instagram_post_make(ig_post: IGPost, ig_user: IGUser, topic_resolver: Callable[[list], list]):
    """
    :param ig_post: The stored IGPost.
    :param ig_user: The stored IGUser that published the post.
    :param topic_resolver: A function that resolves the posts topic.
    :return: CAPost - A processed representation of the post in the common namespace.
    """
    post = CAPost(source=SOURCE_INSTAGRAM, ext_id=ig_post.shortcode, ext_user_id=ig_post.owner_id,
                  created_at=ig_post.created_at)

    tagged_usernames = []

    for mention in ig_post.caption_mentions:
        tagged_usernames.append(mention)

    for user in ig_post.tagged_users:
        tagged_usernames.append(user)

    tags = []
    users = mongo().instagram.get_users_by_username(tagged_usernames)

    for user_name in tagged_usernames:
        tag = {"user_id": "", "username": user_name}
        matches = [u for u in users if u["username"] == user_name]

        if matches is not None and len(matches) > 0:
            user = matches[0]
            tag["user_id"] = user["user_id"]

        tags.append(tag)

    post.tags = tags
    post.hashtags = ig_post.caption_hashtags

    post.text = ig_post.caption
    post.tokens = preprocess_text(ig_post.caption, discard_limited_tokens=False)  # TODO: maybe true ???

    if post.tokens is None or len(post.tokens) == 0:
        return None

    post.sentiment = __detect_sentiment(ig_post.caption)
    post.emotion = __detect_emotion(ig_post.caption)

    location = location_of_ig_post(ig_post)

    if location is not None:
        post.set_location(location[0], location[1])

    post.topics = topic_resolver(post.tokens)
    post.named_entities = __named_entities(post.tokens)

    post.like_count = len(ig_post.likes)
    post.predicted_like_count = __predict_post_likes(ig_post, ig_user, post.tokens)

    return post


def __detect_sentiment(text: str):
    """
    :param text: The text to examine.
    :return: Sentiment - the detected sentiments.
    """
    sentiment_detector = SentimentDetector()
    sentiment = sentiment_detector.detect_sentiment(text)

    # memory cleanup
    del sentiment_detector
    gc.collect()

    return sentiment


def __detect_emotion(text: str):
    """
    :param text: The text to examine.
    :return: Emotion - the detected emotions.
    """
    emotion_detector = EmotionDetector()
    emotion = emotion_detector.detect_emotions(text)

    # memory cleanup
    del emotion_detector
    gc.collect()

    return emotion


def __named_entities(tokens: list):
    """
    :param tokens: The list of tokens extracted from the text of a post or tweet
    :return: list The detected named entities.
    """
    nlp = spacy.load("en_core_web_sm")

    # Named Entity Types: https://spacy.io/api/annotation#named-entities

    joined_tokens = " ".join(tokens)
    doc = nlp(joined_tokens)

    named_entities = []

    for detected_entity in doc.ents:
        named_entity = dict()
        named_entity["text"] = detected_entity.text
        named_entity["type"] = detected_entity.label_

        named_entities.append(named_entity)

    # memory cleanup
    del nlp
    gc.collect()

    return named_entities


def __predict_tweet_likes(tweet: TWTweet, user: TWUser, tokens: list):
    """
    :param tweet: The item to predict likes for.
    :param user: The user.
    :param tokens: A list of the processed tokens.
    :return: int - the number of likes predicted
    """
    likes_detector = ContextDetector()
    likes = likes_detector.predict_twitter(tweet.text)

    # memory cleanup
    del likes_detector
    gc.collect()

    return int(likes)


def __predict_post_likes(post: IGPost, user: IGUser, tokens: list):
    """
        :param post: The item to predict likes for.
        :param user: The user.
        :param tokens: A list of the processed tokens.
        :return: int - the number of likes predicted
        """
    likes_detector = ContextDetector()
    likes = likes_detector.predict_twitter(post.caption)

    # memory cleanup
    del likes_detector
    gc.collect()

    return int(likes)


##############################################################################
#
#   Section: CAUser converters
#
##############################################################################


def twitter_user_make(twitter_user: TWUser, posts: List[CAPost]):
    """
    :param twitter_user: The TWUser object representing a twitter user.
    :param posts: A list of posts in the common namespace
    :return: CAUser - A representation of the user in the common namespace.
    """
    user = CAUser(source=SOURCE_TWITTER, ext_id=twitter_user.user_id)

    user.username = twitter_user.screen_name
    user.name = twitter_user.name

    user.description = twitter_user.description

    user.is_verified = twitter_user.is_verified

    user.follower_count = twitter_user.follower_count
    user.follows_count = twitter_user.follows_count

    user.lifetime_post_count = twitter_user.tweet_count

    user.posts = posts
    user.profile = __user_profile_of(user.name, posts)

    return user


def instagram_user_make(ig_user: IGUser, posts: List[CAPost]):
    """
    :param ig_user: The IGUser object representing an instagram user.
    :param posts: A list of posts in the common namespace
    :return: CAUser - A representation of the user in the common namespace.
    """
    user = CAUser(source=SOURCE_INSTAGRAM, ext_id=ig_user.user_id)

    user.username = ig_user.username
    user.name = ig_user.full_name

    user.description = ig_user.biography

    user.is_verified = ig_user.is_verified

    user.follower_count = ig_user.follower_count
    user.follows_count = ig_user.follows_count

    user.lifetime_post_count = ig_user.mediacount

    user.posts = posts
    user.profile = __user_profile_of(user.name, posts)

    return user


def __user_profile_of(user_display_name: str, posts: List[CAPost]):
    """
    :param user_display_name: The users visible name or profile name.
    :param posts: A list of the user's posts in the common namespace representation.
    :return: UserProfile - The user's inferred profile.
    """
    profile_builder = UserProfileBuilder()
    profile = profile_builder.build_profile(user_display_name, [post.text for post in posts])

    # memory cleanup
    del profile_builder
    gc.collect()

    return profile
