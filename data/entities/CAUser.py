from data.entities.CAPost import CAPost
from data.entities.ca_constants import SOURCE_UNKNOWN, SOURCE_TWITTER, SOURCE_INSTAGRAM
from prediction.profiling.UserProfile import UserProfile


class CAUser:
    """
    A common namespace user model.
    """
    source = SOURCE_UNKNOWN

    ext_id = None

    username = ""
    name = ""

    description = ""

    is_verified = False

    follower_count = 0
    follows_count = 0

    lifetime_post_count = 0

    profile = UserProfile()
    posts = []

    def __init__(self, source: str = SOURCE_UNKNOWN, ext_id: str = ""):
        self.source = source

        self.ext_id = ext_id

        self.username = ""
        self.name = ""

        self.description = ""

        self.is_verified = False

        self.follower_count = 0
        self.follows_count = 0

        self.lifetime_post_count = 0

        self.profile = UserProfile()
        self.posts = []

    def __dict__(self):
        dict_self = dict()

        dict_self["source"] = self.source

        dict_self["ext_id"] = self.ext_id

        dict_self["username"] = self.username
        dict_self["name"] = self.name

        dict_self["description"] = self.description

        dict_self["is_verified"] = self.is_verified

        dict_self["follower_count"] = self.follower_count
        dict_self["follows_count"] = self.follows_count

        dict_self["lifetime_post_count"] = self.lifetime_post_count

        dict_self["profile"] = self.profile.to_dict()
        dict_self["posts"] = [post.to_dict() for post in self.posts]

        return dict_self

    def to_dict(self):
        return self.__dict__()

    def from_dict(self, dict_self):
        self.source = dict_self["source"]

        self.ext_id = dict_self["ext_id"]

        self.username = dict_self["username"]
        self.name = dict_self["name"]

        self.description = dict_self["description"]

        self.is_verified = dict_self["is_verified"]

        self.follower_count = dict_self["follower_count"]
        self.follows_count = dict_self["follows_count"]

        self.lifetime_post_count = dict_self["lifetime_post_count"]

        self.profile = UserProfile()
        self.profile.from_dict(dict_self["profile"])

        self.posts = [CAPost().from_dict(dict_post) for dict_post in dict_self["posts"]]

        return self
    