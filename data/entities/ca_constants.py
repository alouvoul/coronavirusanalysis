"""
This file holds the source constants used during processing and by queries.
"""

SOURCE_TWITTER = "TW"
SOURCE_INSTAGRAM = "IG"
SOURCE_UNKNOWN = "UNK"