from data.entities.ca_constants import SOURCE_TWITTER, SOURCE_INSTAGRAM, SOURCE_UNKNOWN
from prediction.emotion.Emotion import Emotion
from prediction.sentiment.Sentiment import Sentiment


class CAPost:
    """
    The common namespace model of a post.
    """
    source = SOURCE_UNKNOWN

    ext_id = None
    ext_user_id = None

    created_at = None

    tags = []
    hashtags = []

    tokens = []
    text = ""

    sentiment = Sentiment()
    emotion = Emotion()

    location = []

    topics = []
    named_entities = []

    like_count = 0
    predicted_like_count = 0

    def __init__(self, source: str = SOURCE_UNKNOWN, ext_id: str = "", ext_user_id: str = "", created_at=None):
        self.source = source

        self.ext_id = ext_id
        self.ext_user_id = ext_user_id

        self.created_at = created_at

        self.tags = []
        self.hashtags = []

        self.tokens = []
        self.text = ""

        self.sentiment = Sentiment()
        self.emotion = Emotion()

        self.location = []

        self.topics = []
        self.named_entities = []

        self.like_count = 0
        self.predicted_like_count = 0

    def set_location(self, latitude: float, longitude: float):
        # mongo requires longitude first
        self.location = [longitude, latitude]

    def __dict__(self):
        dict_self = dict()

        dict_self["source"] = self.source

        dict_self["ext_id"] = self.ext_id
        dict_self["ext_user_id"] = self.ext_user_id

        dict_self["created_at"] = self.created_at

        dict_self["tags"] = self.tags
        dict_self["hashtags"] = self.hashtags

        dict_self["tokens"] = self.tokens
        dict_self["text"] = self.text

        dict_self["sentiment"] = self.sentiment.to_dict()
        dict_self["emotion"] = self.emotion.to_dict()

        dict_self["location"] = self.location

        dict_self["topics"] = self.topics
        dict_self["named_entities"] = self.named_entities

        dict_self["like_count"] = self.like_count
        dict_self["predicted_like_count"] = self.predicted_like_count

        return dict_self

    def to_dict(self):
        return self.__dict__()

    def from_dict(self, dict_self: dict):
        self.source = dict_self["source"]

        self.ext_id = dict_self["ext_id"]
        self.ext_user_id = dict_self["ext_user_id"]

        self.created_at = dict_self["created_at"]

        self.tags = dict_self["tags"]
        self.hashtags = dict_self["hashtags"]

        self.tokens = dict_self["tokens"]
        self.text = dict_self["text"]

        self.sentiment = Sentiment()
        self.sentiment.from_dict(dict_self["sentiment"])

        self.emotion = Emotion()
        self.emotion.from_dict(dict_self["emotion"])

        self.location = dict_self["location"]

        self.topics = dict_self["topics"]
        self.named_entities = dict_self["named_entities"]

        self.like_count = dict_self["like_count"]
        self.predicted_like_count = dict_self["predicted_like_count"]

        return self
