from data.MongoDB import MongoDBConnection


connection = None


def mongo():
    """
    A helper function to invoke mongo DB commands in a less cumbersome way.
    :return: MongoDBConnection - The connection to MongoDB.
    """
    global connection

    if connection is None:
        connection = MongoDBConnection()

    return connection

