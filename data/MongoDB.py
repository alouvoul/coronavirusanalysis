import random
from math import ceil

from data.entities.CAUser import CAUser, SOURCE_TWITTER, SOURCE_INSTAGRAM
from scrap.TwitterAPI import TWTweet
from scrap.TwitterAPI import TWUser
from scrap.InstaAPI import IGPost
from scrap.InstaAPI import IGUser

try:
    import pymongo
except ImportError:
    print("ERROR: pymongo is required.")
    print("Please install PyMongo using the command:")
    print("$ pip install pymongo")
    exit(1)


class MongoDBConnection:
    """
    A class that manages the connection to MongoDB and various databases.
    The API resembles a builder type object for easy access.
    """
    host = ""
    port = 0

    client = None

    twitter = None
    instagram = None
    ca_db = None

    def __init__(self, host="localhost", port=27017):
        self.host = host
        self.port = port

        #  create a connection to MongoDB
        self.client = pymongo.MongoClient("localhost", 27017)

        #  create helper objects for twitter, instagram and internal DBs
        self.twitter = TwitterDB(self.client, self.database("twitter"))
        self.instagram = InstagramDB(self.client, self.database("instagram"))
        self.ca_db = CoronavirusAnalysisDB(self.client, self.database("coronavirus_analysis"))

    def database(self, dbname):
        return self.client[dbname]


class CoronavirusAnalysisDB:
    """
    A helper object to manage queries for the CoronavirusAnalysisDB
    """
    client = None
    db = None

    ca_users = None
    ca_topics = None
    ca_data = None

    def __init__(self, client, db):
        self.client = client
        self.db = db

        self.ca_users = self.collection("ca_users")
        self.ca_topics = self.collection("ca_topics")
        self.ca_data = self.collection("ca_data")

    def collection(self, name):
        return self.db[name]

    def save_topic(self, topic: dict):
        topic_id = topic["topic_id"]

        match = self.ca_topics.find_one({"topic_id": topic_id})
        exists = match is not None

        if not exists:
            self.ca_topics.insert_one(topic)
        else:
            self.ca_topics.update({"_id": match["_id"]}, topic)

    def find_all_topics(self):
        return self.ca_topics.find({})

    def save_processed_user(self, user: CAUser):
        ext_id = user.ext_id
        source = user.source

        match = self.ca_users.find_one({"$and": [{"source": source}, {"ext_id": ext_id}]})
        exists = match is not None

        if not exists:
            self.ca_users.insert_one(user.to_dict())
        else:
            self.ca_users.update({"_id": match["_id"]}, user.to_dict())

    def find_random_user(self):
        use_twitter = bool(random.getrandbits(1))

        if use_twitter:
            return self.find_random_user_by_source(SOURCE_TWITTER)
        else:
            return self.find_random_user_by_source(SOURCE_INSTAGRAM)

    def find_random_user_by_source(self, source):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source,
                    'posts': {
                        '$exists': True,
                        '$not': {
                            '$size': 0
                        }
                    }
                }
            }, {
                '$sample': {
                    'size': 1
                }
            }
        ])

    # ---- COUNTS ----

    def count_source_users(self, source: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$count': 'count'
            }
        ])

    def count_source_posts(self, source: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$count': 'count'
            }
        ])

    # ---- SENTIMENTS ----

    def count_negative_posts(self, source):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'sent_negative': '$posts.sentiment.negative',
                    'sent_neutral': '$posts.sentiment.neutral',
                    'sent_positive': '$posts.sentiment.positive'
                }
            }, {
                '$project': {
                    'sent_negative': 1,
                    'sent_neutral': 1,
                    'sent_positive': 1
                }
            }, {
                '$match': {
                    'sent_negative': {
                        '$gte': 0.5
                    }
                }
            }, {
                '$count': 'count'
            }
        ])

    def count_neutral_posts(self, source):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'sent_negative': '$posts.sentiment.negative',
                    'sent_neutral': '$posts.sentiment.neutral',
                    'sent_positive': '$posts.sentiment.positive'
                }
            }, {
                '$project': {
                    'sent_negative': 1,
                    'sent_neutral': 1,
                    'sent_positive': 1
                }
            }, {
                '$match': {
                    'sent_neutral': {
                        '$gte': 0.5
                    }
                }
            }, {
                '$count': 'count'
            }
        ])

    def count_positive_posts(self, source):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'sent_negative': '$posts.sentiment.negative',
                    'sent_neutral': '$posts.sentiment.neutral',
                    'sent_positive': '$posts.sentiment.positive'
                }
            }, {
                '$project': {
                    'sent_negative': 1,
                    'sent_neutral': 1,
                    'sent_positive': 1
                }
            }, {
                '$match': {
                    'sent_positive': {
                        '$gte': 0.5
                    }
                }
            }, {
                '$count': 'count'
            }
        ])

    # ---- EMOTIONS ----

    def count_emo_anger(self, source: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'anger': '$posts.emotion.anger',
                    'disgust': '$posts.emotion.disgust',
                    'fear': '$posts.emotion.fear',
                    'joy': '$posts.emotion.joy',
                    'sadness': '$posts.emotion.sadness',
                    'surprise': '$posts.emotion.surprise'
                }
            }, {
                '$project': {
                    'anger': 1,
                    'disgust': 1,
                    'fear': 1,
                    'joy': 1,
                    'sadness': 1,
                    'surprise': 1
                }
            }, {
                '$match': {
                    'anger': 1
                }
            }, {
                '$count': 'count'
            }
        ])

    def count_emo_disgust(self, source: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'anger': '$posts.emotion.anger',
                    'disgust': '$posts.emotion.disgust',
                    'fear': '$posts.emotion.fear',
                    'joy': '$posts.emotion.joy',
                    'sadness': '$posts.emotion.sadness',
                    'surprise': '$posts.emotion.surprise'
                }
            }, {
                '$project': {
                    'anger': 1,
                    'disgust': 1,
                    'fear': 1,
                    'joy': 1,
                    'sadness': 1,
                    'surprise': 1
                }
            }, {
                '$match': {
                    'disgust': 1
                }
            }, {
                '$count': 'count'
            }
        ])

    def count_emo_fear(self, source: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'anger': '$posts.emotion.anger',
                    'disgust': '$posts.emotion.disgust',
                    'fear': '$posts.emotion.fear',
                    'joy': '$posts.emotion.joy',
                    'sadness': '$posts.emotion.sadness',
                    'surprise': '$posts.emotion.surprise'
                }
            }, {
                '$project': {
                    'anger': 1,
                    'disgust': 1,
                    'fear': 1,
                    'joy': 1,
                    'sadness': 1,
                    'surprise': 1
                }
            }, {
                '$match': {
                    'fear': 1
                }
            }, {
                '$count': 'count'
            }
        ])

    def count_emo_joy(self, source: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'anger': '$posts.emotion.anger',
                    'disgust': '$posts.emotion.disgust',
                    'fear': '$posts.emotion.fear',
                    'joy': '$posts.emotion.joy',
                    'sadness': '$posts.emotion.sadness',
                    'surprise': '$posts.emotion.surprise'
                }
            }, {
                '$project': {
                    'anger': 1,
                    'disgust': 1,
                    'fear': 1,
                    'joy': 1,
                    'sadness': 1,
                    'surprise': 1
                }
            }, {
                '$match': {
                    'joy': 1
                }
            }, {
                '$count': 'count'
            }
        ])

    def count_emo_sadness(self, source: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'anger': '$posts.emotion.anger',
                    'disgust': '$posts.emotion.disgust',
                    'fear': '$posts.emotion.fear',
                    'joy': '$posts.emotion.joy',
                    'sadness': '$posts.emotion.sadness',
                    'surprise': '$posts.emotion.surprise'
                }
            }, {
                '$project': {
                    'anger': 1,
                    'disgust': 1,
                    'fear': 1,
                    'joy': 1,
                    'sadness': 1,
                    'surprise': 1
                }
            }, {
                '$match': {
                    'sadness': 1
                }
            }, {
                '$count': 'count'
            }
        ])

    def count_emo_surprise(self, source: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'anger': '$posts.emotion.anger',
                    'disgust': '$posts.emotion.disgust',
                    'fear': '$posts.emotion.fear',
                    'joy': '$posts.emotion.joy',
                    'sadness': '$posts.emotion.sadness',
                    'surprise': '$posts.emotion.surprise'
                }
            }, {
                '$project': {
                    'anger': 1,
                    'disgust': 1,
                    'fear': 1,
                    'joy': 1,
                    'sadness': 1,
                    'surprise': 1
                }
            }, {
                '$match': {
                    'surprise': 1
                }
            }, {
                '$count': 'count'
            }
        ])

    # ---- LIKES ----

    def avg_like_count(self, source):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'likes': '$posts.like_count'
                }
            }, {
                '$project': {
                    'likes': 1
                }
            }, {
                '$group': {
                    '_id': '_id',
                    'avg_likes': {
                        '$avg': '$likes'
                    }
                }
            }
        ])

    def count_posts_with_more_likes_than(self, source, avg_likes):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'likes': '$posts.like_count'
                }
            }, {
                '$project': {
                    'likes': 1
                }
            }, {
                '$match': {
                    'likes': {
                        '$gte': avg_likes
                    }
                }
            }, {
                '$count': 'count'
            }
        ])

    # ---- EXPLORATORY (by source) ----

    def unclean_top_post_tokens(self, source: str):
        discarded_tokens = ['url', 'amp', 'SEMANTIC_NEGATION']
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'tokens': '$posts.tokens'
                }
            }, {
                '$project': {
                    'tokens': 1
                }
            }, {
                '$unwind': {
                    'path': '$tokens',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$match': {
                    'tokens': {
                        '$nin': discarded_tokens
                    }
                }
            }, {
                '$group': {
                    '_id': '$tokens',
                    'count': {
                        '$sum': 1
                    }
                }
            }, {
                '$match': {
                    'count': {
                        '$gt': 1
                    }
                }
            }, {
                '$sort': {
                    'count': -1
                }
            }
        ])

    def top_hashtags(self, source: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'hashtags': '$posts.hashtags'
                }
            }, {
                '$project': {
                    'hashtags': 1
                }
            }, {
                '$unwind': {
                    'path': '$hashtags',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$group': {
                    '_id': '$hashtags',
                    'count': {
                        '$sum': 1
                    }
                }
            }, {
                '$match': {
                    'count': {
                        '$gt': 1
                    }
                }
            }, {
                '$sort': {
                    'count': -1
                }
            }
        ])

    def top_mentions(self, source: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'tags': '$posts.tags'
                }
            }, {
                '$project': {
                    'tags': 1
                }
            }, {
                '$unwind': {
                    'path': '$tags',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$group': {
                    '_id': '$tags',
                    'count': {
                        '$sum': 1
                    }
                }
            }, {
                '$match': {
                    'count': {
                        '$gt': 1
                    }
                }
            }, {
                '$sort': {
                    'count': -1
                }
            }
        ])

    def top_named_entities(self, source: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'named_entities': '$posts.named_entities'
                }
            }, {
                '$project': {
                    'named_entities': 1
                }
            }, {
                '$unwind': {
                    'path': '$named_entities',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$group': {
                    '_id': '$named_entities',
                    'count': {
                        '$sum': 1
                    }
                }
            }, {
                '$match': {
                    'count': {
                        '$gt': 1
                    }
                }
            }, {
                '$sort': {
                    'count': -1
                }
            }
        ])

    def top_lon_lat_and_count(self, source: str):
        # CAUTION locations are in an array with [lon,lat]
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'lon_lat': '$posts.location'
                }
            }, {
                '$project': {
                    'lon_lat': 1
                }
            }, {
                '$group': {
                    '_id': '$lon_lat',
                    'count': {
                        '$sum': 1
                    }
                }
            }, {
                '$sort': {
                    'count': -1
                }
            }
        ])

    def find_lon_lat(self, source: str):
        # CAUTION locations are in an array with [lon,lat]
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'lon_lat': '$posts.location'
                }
            }, {
                '$project': {
                    'lon_lat': 1
                }
            }
        ])

    # ---- EXPLORATORY (total) ----

    def total_unclean_top_post_tokens(self):
        discarded_tokens = ['url', 'amp', 'SEMANTIC_NEGATION']
        return self.ca_users.aggregate([
            {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'tokens': '$posts.tokens'
                }
            }, {
                '$project': {
                    'tokens': 1
                }
            }, {
                '$unwind': {
                    'path': '$tokens',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$match': {
                    'tokens': {
                        '$nin': discarded_tokens
                    }
                }
            }, {
                '$group': {
                    '_id': '$tokens',
                    'count': {
                        '$sum': 1
                    }
                }
            }, {
                '$match': {
                    'count': {
                        '$gt': 1
                    }
                }
            }, {
                '$sort': {
                    'count': -1
                }
            }
        ])

    def total_top_hashtags(self):
        return self.ca_users.aggregate([
            {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'hashtags': '$posts.hashtags'
                }
            }, {
                '$project': {
                    'hashtags': 1
                }
            }, {
                '$unwind': {
                    'path': '$hashtags',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$group': {
                    '_id': '$hashtags',
                    'count': {
                        '$sum': 1
                    }
                }
            }, {
                '$match': {
                    'count': {
                        '$gt': 1
                    }
                }
            }, {
                '$sort': {
                    'count': -1
                }
            }
        ])

    def total_top_mentions(self):
        return self.ca_users.aggregate([
            {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'tags': '$posts.tags'
                }
            }, {
                '$project': {
                    'tags': 1
                }
            }, {
                '$unwind': {
                    'path': '$tags',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$group': {
                    '_id': '$tags',
                    'count': {
                        '$sum': 1
                    }
                }
            }, {
                '$match': {
                    'count': {
                        '$gt': 1
                    }
                }
            }, {
                '$sort': {
                    'count': -1
                }
            }
        ])

    def total_top_named_entities(self):
        return self.ca_users.aggregate([
            {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'named_entities': '$posts.named_entities'
                }
            }, {
                '$project': {
                    'named_entities': 1
                }
            }, {
                '$unwind': {
                    'path': '$named_entities',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$group': {
                    '_id': '$named_entities',
                    'count': {
                        '$sum': 1
                    }
                }
            }, {
                '$match': {
                    'count': {
                        '$gt': 1
                    }
                }
            }, {
                '$sort': {
                    'count': -1
                }
            }
        ])

    def total_top_lon_lat_and_count(self):
        # CAUTION locations are in an array with [lon,lat]
        return self.ca_users.aggregate([
            {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'lon_lat': '$posts.location'
                }
            }, {
                '$project': {
                    'lon_lat': 1
                }
            }, {
                '$group': {
                    '_id': '$lon_lat',
                    'count': {
                        '$sum': 1
                    }
                }
            }, {
                '$sort': {
                    'count': -1
                }
            }
        ])

    def total_find_lon_lat(self):
        # CAUTION locations are in an array with [lon,lat]
        return self.ca_users.aggregate([
            {
                '$unwind': {
                    'path': '$posts',
                    'includeArrayIndex': 'post_idx',
                    'preserveNullAndEmptyArrays': False
                }
            }, {
                '$set': {
                    'lon_lat': '$posts.location'
                }
            }, {
                '$project': {
                    'lon_lat': 1
                }
            }
        ])

    # ---- PROFILE ----

    def count_profiles_in_age_group(self, source: str, age_group: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$set': {
                    'age_group': '$profile.age'
                }
            }, {
                '$project': {
                    'age_group': 1
                }
            }, {
                '$match': {
                    'age_group': age_group
                }
            }, {
                '$count': 'count'
            }
        ])

    def count_profiles_with_age_18_24(self, source: str):
        return self.count_profiles_in_age_group(source, "18-24")

    def count_profiles_with_age_25_34(self, source: str):
        return self.count_profiles_in_age_group(source, "25-34")

    def count_profiles_with_age_35_49(self, source: str):
        return self.count_profiles_in_age_group(source, "35-49")

    def count_profiles_with_age_50_plus(self, source: str):
        return self.count_profiles_in_age_group(source, "50-XX")

    def count_profiles_with_gender(self, source: str, gender: str):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$set': {
                    'gender': '$profile.gender'
                }
            }, {
                '$project': {
                    'gender': 1
                }
            }, {
                '$match': {
                    'gender': gender
                }
            }, {
                '$count': 'count'
            }
        ])

    def count_profiles_with_gender_male(self, source: str):
        return self.count_profiles_with_gender(source, "M")

    def count_profiles_with_gender_female(self, source: str):
        return self.count_profiles_with_gender(source, "F")

    def count_profiles_with_gender_other(self, source: str):
        return self.count_profiles_with_gender(source, "O")

    def count_profiles_with_sarcasm(self, source: str, sarcasm: int):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$set': {
                    'sarcastic': '$profile.sarcastic'
                }
            }, {
                '$project': {
                    'sarcastic': 1
                }
            }, {
                '$match': {
                    'sarcastic': sarcasm
                }
            }, {
                '$count': 'count'
            }
        ])

    def count_profiles_sarcastic(self, source: str):
        return self.count_profiles_with_sarcasm(source, 1)

    def count_profiles_not_sarcastic(self, source: str):
        return self.count_profiles_with_sarcasm(source, 0)

    def count_profiles_with_aggression(self, source: str, aggression: int):
        return self.ca_users.aggregate([
            {
                '$match': {
                    'source': source
                }
            }, {
                '$set': {
                    'aggression': '$profile.aggressive'
                }
            }, {
                '$project': {
                    'aggression': 1
                }
            }, {
                '$match': {
                    'aggression': aggression
                }
            }, {
                '$count': 'count'
            }
        ])

    def count_profiles_no_aggression(self, source: str):
        return self.count_profiles_with_aggression(source, 0)

    def count_profiles_low_aggression(self, source: str):
        return self.count_profiles_with_aggression(source, 1)

    def count_profiles_medium_aggression(self, source: str):
        return self.count_profiles_with_aggression(source, 2)

    def count_profiles_high_aggression(self, source: str):
        return self.count_profiles_with_aggression(source, 3)


class TwitterDB:
    """
    A helper object to manage queries for the TwitterDB
    """
    client = None
    db = None

    tweets = None
    users = None

    def __init__(self, client, db):
        self.client = client
        self.db = db

        # create collection refs
        self.tweets = self.collection("tweets")
        self.users = self.collection("users")

    def collection(self, name):
        return self.db[name]

    def latest_tweet_id(self):
        max_id = None
        latest_tweet = self.tweets.find_one(sort=[("tweet_id", pymongo.DESCENDING)])

        if latest_tweet is not None:
            max_id = latest_tweet["tweet_id"]

        return max_id

    def save_tweet(self, tweet: TWTweet):
        tweet_id = tweet.tweet_id

        match = self.tweets.find_one({"tweet_id": tweet_id})
        exists = match is not None

        if not exists:
            self.tweets.insert(tweet.__dict__)
        else:
            self.tweets.update({"_id": match["_id"]}, tweet.__dict__)

    def save_twitter_user(self, user: TWUser):
        user_id = user.user_id

        match = self.users.find_one({"user_id": user_id})
        exists = match is not None

        if not exists:
            self.users.insert(user.__dict__)
        else:
            self.users.update({"_id": match["_id"]}, user.__dict__)

    def get_all_tweets(self):
        return self.tweets.find({})

    def get_tweets_with_location(self):
        return self.tweets.find({"location_place": {"$ne": None}})

    def get_tweets_with_place_only(self):
        return self.tweets.find({"location_place": {"$ne": None}, "location_latlon": None})

    def get_tweets_no_location(self):
        return self.tweets.find({"location_place": None, "location_latlon": None})

    def get_user_of_tweet(self, tweet):
        user_id = None

        if isinstance(tweet, TWTweet):
            user_id = tweet.user_id
        elif isinstance(tweet, dict):
            user_id = tweet["user_id"]
        else:
            return None

        return self.users.find_one({"user_id": user_id})

    def update_tweet(self, tweet_id, column, value):
        match = self.tweets.find_one({"tweet_id": tweet_id})
        self.tweets.update_one({"_id": match["_id"]}, {"$set": {column: value}})

    def get_all_users(self, sticky=False):
        return self.users.find({}, no_cursor_timeout=sticky)

    def count_user_pages(self, size: int):
        count = float(self.users.count_documents({}))
        pages = count / float(size)
        return ceil(pages)

    def get_users_paginated(self, page: int, size: int):
        # convert page request to limit - skip query
        skipped = (page - 1) * size
        return self.users.find({}, skip=skipped, limit=size)

    def get_users_by_id(self, user_ids: list):
        return self.users.find({"user_id": {"$in": user_ids}})

    def update_user(self, user_id, column, value):
        match = self.tweets.find_one({"user_id": user_id})
        self.users.update_one({"_id": match["_id"]}, {"$set": {column: value}})

    def get_user_tweets(self, user_id):
        return self.tweets.find({"user_id": user_id})

    def delete_documents(self):
        match = self.tweets.find({"preprocessed_text": "Not useful for this study"})
        self.tweets.delete_many(match)


class InstagramDB:
    """
    A helper object to manage queries for the InstagramDB
    """
    client = None
    db = None

    posts = None
    users = None

    def __init__(self, client, db):
        self.client = client
        self.db = db

        # create collection refs
        self.posts = self.collection("posts")
        self.users = self.collection("users")

    def collection(self, name):
        return self.db[name]

    def save_insta_post(self, post: IGPost):
        post_shortcode = post.shortcode

        match = self.posts.find_one({"shortcode": post_shortcode})
        exists = match is not None

        if not exists:
            self.posts.insert(post.__dict__)
        else:
            self.posts.update({"_id": match["_id"]}, post.__dict__)

    def save_insta_user(self, user: IGUser):
        user_id = user.user_id

        match = self.users.find_one({"user_id": user_id})
        exists = match is not None

        if not exists:
            self.users.insert(user.__dict__)
        else:
            self.users.update({"_id": match["_id"]}, user.__dict__)

    def get_all_posts(self):
        return self.posts.find({})

    def get_all_users(self, sticky=False):
        return self.users.find({}, no_cursor_timeout=sticky)

    def count_user_pages(self, size: int):
        count = float(self.users.count_documents({}))
        pages = count / float(size)
        return ceil(pages)

    def get_users_paginated(self, page: int, size: int):
        # convert page request to limit - skip query
        skipped = (page - 1) * size
        return self.users.find({}, skip=skipped, limit=size)

    def get_users_by_username(self, user_names: list):
        return self.users.find({"username": {"$in": user_names}})

    def get_user_posts(self, user_id):
        return self.posts.find({"owner_id": user_id})

    def get_user_of_post(self, post):
        user_id = None

        if isinstance(post, IGPost):
            user_id = post.owner_id
        elif isinstance(post, dict):
            user_id = post["owner_id"]
        else:
            return None

        return self.users.find_one({"user_id": user_id})

    def update_post(self, post_id, column, value):
        match = self.posts.find_one({"_id": post_id})
        self.posts.update_one({"_id": match["_id"]}, {"$set": {column: value}})

# EOF
