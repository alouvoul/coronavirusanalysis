from prediction.like_prediction import BaseModelDetector, ContextDetector


def interpret():
    print("START")
    base = BaseModelDetector.BaseModelDetector()
    context = ContextDetector.ContextDetector()
    exit(0)
    # model = base.model_twitter.model
    print("START")
    data = base.twitter_features()
    from sklearn.model_selection import train_test_split
    x_train, x_test, y_train, y_test = train_test_split(data.iloc[:, :-1], data.iloc[:, -1:], train_size=0.7)
    import shap
    import sklearn
    model = sklearn.ensemble.RandomForestRegressor()
    model.fit(x_train, y_train)
    shap.initjs()
    features = x_train.columns
    se = shap.TreeExplainer(model)  # , feature_perturbation="interventional", model_output="raw"
    shap_values = se.shap_values(x_test)
    SHAP_VALUES = shap_values  # Predicting a zero state
    MAX_DISPLAY = 20  # Maximum number of important features
    shap.summary_plot(SHAP_VALUES, features=x_test, feature_names=features, max_display=MAX_DISPLAY)

    import seaborn as sns
    import pandas as pd
    from eli5.sklearn import PermutationImportance
    from matplotlib import pyplot as plt
    weights = PermutationImportance(model).fit(x_test, y_test)
    print(list(data.loc[:, data.columns != 'target'].columns))
    print(weights.feature_importances_)
    model_weights = pd.DataFrame(
        {'Features': list(data.loc[:, data.columns != 'target'].columns), 'Importance': weights.feature_importances_})
    model_weights = model_weights.reindex(model_weights['Importance'].abs().sort_values(ascending=False).index)
    model_weights = model_weights[(model_weights["Importance"] != 0)]
    plt.figure(num=None, figsize=(8, 6), dpi=100, facecolor='w', edgecolor='k')
    sns.barplot(x="Importance", y="Features", data=model_weights)
    # plt.title("Intercept (Bias): " + str(self.model.intercept_[0]), loc='right')
    plt.xticks(rotation=90)
    plt.show()

    exit(0)