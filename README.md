In the Contributors Section, the contributors were the following:

Konstantinos Georgiou (koage13.giou@gmail.com)
Athanasios Kefalas (sakiskefalas@yahoo.gr)
Anastasios Louvoulinas (alouvoul@hotmail.com)
Anastasios Louvoulinas (alouvoul@csd.auth.gr)

Due to synchronization issues, Anastasios has 2 emails connected but all the 
commits are his.

In order to run the different components of the application, we have constructed
three arguments that should be passed in the main method. 

--server: Launches the web application
--scrap SOURCE: Retrieves the data, SOURCE = [twitter,instagram]
--process: Performs necessary preprocessing and runs Machine Learning Models

By passing a single argument, a specific part of the application runs every time
and performs a different operation.
For the application to run properly, a database should be created under the name
"coronavirus_analysis" in MongoDb and inside, there should be a collection named
"ca_users" and a collection named "ca_topics". Also, two databases should be 
created named "twitter" and "instagram". Twitter should have two collections
"tweets" and "users" and Instagram should have two collections "posts" and
"users". 


